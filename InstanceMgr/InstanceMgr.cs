﻿using CrystalDecisions.Enterprise;
using CrystalDecisions.Enterprise.Desktop;
using DFT.BOE;
using DFT.BOE.CommandLine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace DFT
{
    public class InstanceMgr
    {
        private static List<FolderObject> buildStructure(BOECommon CommonInfo, InfoObjects currentLevel, List<FolderObject> DirNode, bool Recursive, bool IsRoot, bool Verbose, bool QueryOutput)
        {
            List<FolderObject> childNodes = new List<FolderObject>();
            InfoObjects childFolders;
            String sQuery = "select  SI_NAME, SI_ID, SI_CUID, SI_PARENT_CUID, SI_PARENTID from CI_INFOOBJECTS " +
                            "where SI_KIND IN ('Folder') and SI_PARENTID = ";
            foreach (InfoObject iFolder in currentLevel)
            {
                if (iFolder is Folder)
                {
                    FolderObject folder;
                    if (iFolder.Parent != null)
                    {
                        folder = new FolderObject(iFolder.ID, iFolder.CUID, iFolder.Title, iFolder.ParentID, iFolder.ParentCUID, iFolder.Parent.Title, IsRoot);
                    }
                    else
                    {
                        folder = new FolderObject(iFolder.ID, iFolder.CUID, iFolder.Title, iFolder.ParentID, iFolder.ParentCUID, "", IsRoot);
                    }

                    if (Verbose) LogIt("Loading Folder: " + iFolder.Title + " - ID: " + iFolder.ID.ToString(), true);

                    if (Recursive)
                    {
                        if (QueryOutput) LogIt("Executing Query: " + sQuery + "'" + iFolder.ID + "'", true);
                        childFolders = CommonInfo.ExecuteRawQuery(sQuery + "'" + iFolder.ID + "'");
                        if (childFolders.Count > 0)
                        {
                            folder.Children = buildStructure(CommonInfo, childFolders, DirNode, Recursive, false, Verbose, QueryOutput);
                            folder.HasChildren = true;
                        }
                        else
                        {
                            folder.HasChildren = false;
                        }
                    }
                    childNodes.Add(folder);
                }
            }
            return childNodes;
        }

        private static List<FolderObject> loadFolders(BOECommon CommonInfo, string[] CUIDs, string[] IDs, bool IsRoot, bool Verbose, bool Recursive, bool QueryOutput)
        {
            List<FolderObject> DirNode = new List<FolderObject>();
            InfoObjects iFolders;
            foreach (string CUID in CUIDs)
            {
                if (CUID != "")
                {
                    if (QueryOutput) LogIt("Executing Query: SELECT SI_NAME, SI_ID, SI_CUID, SI_HAS_CHILDREN, SI_PARENT_CUID FROM CI_INFOOBJECTS WHERE SI_KIND = 'Folder' AND SI_CUID = '" + CUID + "'", true);
                    iFolders = CommonInfo.ExecuteRawQuery("SELECT SI_NAME, SI_ID, SI_CUID, SI_HAS_CHILDREN, SI_PARENT_CUID FROM CI_INFOOBJECTS WHERE SI_KIND = 'Folder' AND SI_CUID = '" + CUID + "'");
                    //if (Verbose) LogIt("Loading Folder: " + iFolders[1].Title + " - ID: " + iFolders[1].ID.ToString(), true);

                    DirNode.AddRange(buildStructure(CommonInfo, iFolders, DirNode, Recursive, IsRoot, Verbose, QueryOutput));
                }
            }

            foreach (string ID in IDs)
            {
                if (ID != "")
                {
                    if (QueryOutput) LogIt("Executing Query: SELECT SI_NAME, SI_ID, SI_CUID, SI_HAS_CHILDREN, SI_PARENT_CUID FROM CI_INFOOBJECTS WHERE SI_KIND = 'Folder' AND SI_ID = " + ID, true);
                    iFolders = CommonInfo.ExecuteRawQuery("SELECT SI_NAME, SI_ID, SI_CUID, SI_HAS_CHILDREN, SI_PARENT_CUID FROM CI_INFOOBJECTS WHERE SI_KIND = 'Folder' AND SI_ID = " + ID);
                    //if (Verbose) LogIt("Loading Folder: " + iFolders[1].Title + " - ID: " + iFolders[1].ID.ToString(), true);

                    DirNode.AddRange(buildStructure(CommonInfo, iFolders, DirNode, Recursive, IsRoot, Verbose, QueryOutput));
                }
            }

            return DirNode;
        }

        private static void LogIt(string Message, bool ErrorLog = false)
        {
            if (ErrorLog) Console.Error.WriteLine(Message);
            else Console.WriteLine(Message);
        }

        private static int Main(string[] args)
        {
            Arguments CommandLine = new Arguments(args);
            InstanceVars Vars = new InstanceVars();
            if (CommandLine["h"] != null || CommandLine["fh"] != null || CommandLine.Count == 0)
            {
                LogIt("BusinessObjects Instance Manager");
                LogIt(" ");
                LogIt("Help Screen: -h (This screen)");
                LogIt("File Help Screen: -fh (Optional Parameter File Setup)");
                LogIt(" ");
                LogIt("Required Options: ");
                LogIt("UserName: -U UserID or --UserID UserID");
                LogIt("UserName: -U UserID@CMS or --UserID UserID@CMS");
                LogIt("CMS: -C CMS or --CMS CMS (Not required if UserID@CMS format used)");
                LogIt("Password: -P or --Password");
                LogIt("Folder ID: -ID or --ID (Not required if CUID passed)");
                LogIt("Folder CUID: -CUID or --CUID (Not required if ID passed)");
                LogIt("  Note: Multiple Folder IDs or CUIDs can be passed if comma delimited. i.e. -ID 23,100,...");
                LogIt(" ");
                LogIt("Optional Options: ");
                LogIt("Verbose: -V");
                LogIt("Query Output: -Q");
                LogIt("Output in CSV: -CSV");
                LogIt("Traverse Folders: -R or --Recursive");
                LogIt("Start Date: -SD 'MM/DD/YYYY' or --StartDate 'MM/DD/YYYY'");
                LogIt("End Date: -ED 'MM/DD/YYYY' or --EndDate 'MM/DD/YYYY'");
                LogIt("  Start and End Dates also accept or X[D]ays/[M]onths/[Y]ears where X is a number");
                LogIt("  To use the D/M/Y format, use a : instead of space, an example is:");
                LogIt("   -SD:-6M for 6 Months ago or -ED:1D for 1 day in the future.");
                LogIt(" Note: Start Date defaults to 1/1/1970 if not supplied, End Date defaults to current date");
                LogIt("");
                LogIt("Delete Instances: -D or --Delete");
                LogIt("Delete Failed Instances Only: --Failed");
                LogIt("Auto-Confirm Deletion: -Y or --Confirm");
                LogIt("Parameter File: -F File or --File File");
                LogIt(" ");
                if (CommandLine["fh"] != null)
                {
                    LogIt("Parameter File Options:");
                    LogIt("A Parameter file can be specified with -F File or --file File");
                    LogIt("The File contains a list of options delimited by = of options");
                    LogIt("All options are optional and if not specified are checked on command line");
                    LogIt("The list of options is as follows:");
                    LogIt("UserName: UserID=Value");
                    LogIt("UserName: UserID=Value@CMS");
                    LogIt("CMS: CMS=Value (Not required if UserID@CMS format used)");
                    LogIt("Password: Password=Value");
                    LogIt("Folder ID: ID=Value (Not required if CUID passed)");
                    LogIt("Folder CUID: CUID=Value (Not required if ID passed)");
                    LogIt("  Note: Multiple Folder IDs or CUIDs can be passed if comma delimited. i.e. ID=23,100,...");
                    LogIt("Verbose: Verbose=Yes");
                    LogIt("Output in CSV: CSV=Yes");
                    LogIt("Traverse Folders: Recursive=Yes");
                    LogIt("Start Date: StartDate='MM/DD/YYYY' or StartDate=+/-XD/M/Y");
                    LogIt("End Date: EndDate='MM/DD/YYYY' or EndDate=+/-XD/M/Y");
                    LogIt("  Note the +/-X is + or - number, such as -30D or +5D or -6M");
                    LogIt("  Note: Start Date defaults to 1/1/1970 if not supplied, End Date defaults to current date");
                    LogIt("Delete Instances: Delete=Yes");
                    LogIt("Delete Failed Instances Only: FailedOnly=Yes");
                    LogIt("Auto-Confirm Deletion: Confirm=Yes");
                    LogIt(" ");
                }
                return -1;
            }
            else
            {
                foreach (DictionaryEntry entry in CommandLine)
                    switch (entry.Key.ToString().ToLower())
                    {
                        case "f": ProcessParamFile(entry.Value.ToString(), Vars); break;
                        case "file": ProcessParamFile(entry.Value.ToString(), Vars); break;
                        case "u": Vars.UserID = entry.Value.ToString(); break;
                        case "userid": Vars.UserID = entry.Value.ToString(); break;
                        case "cuid": Vars.CUID = entry.Value.ToString(); break;
                        case "p": Vars.Password = entry.Value.ToString(); break;
                        case "password": Vars.Password = entry.Value.ToString(); break;
                        case "cms": Vars.CMS = entry.Value.ToString(); break;
                        case "c": Vars.CMS = entry.Value.ToString(); break;
                        case "id": Vars.ID = entry.Value.ToString(); break;
                        case "startdate": Vars.StartDate = ParseDate(entry.Value.ToString(), true); break;
                        case "sd": Vars.StartDate = ParseDate(entry.Value.ToString(), true); break;
                        case "enddate": Vars.EndDate = ParseDate(entry.Value.ToString(), false); break;
                        case "ed": Vars.EndDate = ParseDate(entry.Value.ToString(), false); break;
                        case "csv": Vars.CSV = true; break;
                        case "v": Vars.Verbose = true; break;
                        case "verbose": Vars.Verbose = true; break;
                        case "q": Vars.QueryOutput = true; break;
                        case "queryoutput": Vars.QueryOutput = true; break;
                        case "r": Vars.Recursive = true; break;
                        case "recursive": Vars.Recursive = true; break;
                        case "d": Vars.DeleteInstances = true; break;
                        case "delete": Vars.DeleteInstances = true; break;
                        case "y": Vars.VerifyDelete = true; break;
                        case "confirm": Vars.VerifyDelete = true; break;
                        case "failed": Vars.DeleteFailedOnly = true; break;
                    }

                if (Vars.Verbose) LogIt("BusinessObjects Instance Manager", true);

                if ((Vars.Password == "") && (Vars.UserID != ""))
                {
                    LogIt("Please Enter Password for " + Vars.UserID);
                    Vars.Password = Console.ReadLine();
                }

                try
                {
                    if ((Vars.UserID == "") && (Vars.CMS == ""))
                    {
                        LogIt("UserID and Password is not specified", true);
                        Environment.Exit(10);
                    }

                    if ((Vars.ID == "") && (Vars.CUID == ""))
                    {
                        LogIt("No ID or CUID has been specified", true);
                        Environment.Exit(10);
                    }
                    BOECommon CommonInfo = new BOECommon();
                    if (Vars.Verbose) LogIt("Logging On to CMS: " + Vars.CMS + " with UserID: " + Vars.UserID, true);
                    CommonInfo.LogOn(Vars.UserID, Vars.Password, Vars.CMS);
                    if (CommonInfo.LoggedOn)
                    {
                        StartProcessing(CommonInfo, Vars);
                        Environment.ExitCode = 0;
                    }
                }
                catch (Exception ex)
                {
                    LogIt("An Error Has Occurred: " + ex.Message, true);
                    Environment.ExitCode = 11015;
                }
                return Environment.ExitCode;
            }
        }

        private static DateTime ParseDate(String Date, bool Truncate)
        {
            DateTime dTemp;
            DateTime rDate = DateTime.Today.AddDays(1).AddSeconds(-1);
            char[] charsToTrim = { '\'', '\"', ' ' };
            Date = Date.TrimStart(charsToTrim).TrimEnd(charsToTrim);

            if (DateTime.TryParse(Date, out dTemp))
            {
                rDate = DateTime.Parse(Date);
            }
            else
            {
                Regex rgx = new Regex(@"^(([-][0-9])|[0-9])+[dmy]$");
                Date = Date.ToLower();
                if (rgx.IsMatch(Date))
                {
                    Date = Date.TrimStart(new char[] { '+' });
                    if (Date.EndsWith("d"))
                    {
                        Date = Date.TrimEnd(new char[] { 'd', 'm', 'y' });
                        double dVal;
                        if (Double.TryParse(Date, out dVal)) rDate = rDate.AddDays(dVal);
                    }

                    if (Date.EndsWith("m"))
                    {
                        Date = Date.TrimEnd(new char[] { 'd', 'm', 'y' });
                        int iVal;
                        if (int.TryParse(Date, out iVal)) rDate = rDate.AddMonths(iVal);
                    }

                    if (Date.EndsWith("y"))
                    {
                        Date = Date.TrimEnd(new char[] { 'd', 'm', 'y' });
                        int iVal;
                        if (int.TryParse(Date, out iVal)) rDate = rDate.AddYears(iVal);
                    }
                }
            }
            if (Truncate) rDate = new DateTime(rDate.Year, rDate.Month, rDate.Day); //Truncate
            return rDate;
        }

        private static bool ProcessParamFile(string FileName, InstanceVars Vars)
        {
            Dictionary<String, String> paramList = new Dictionary<String, String>();
            StreamReader sr = new StreamReader(FileName);
            bool bReturn = true;
            try
            {
                string line;
                while (!string.IsNullOrEmpty((line = sr.ReadLine())))
                {
                    if (!line.StartsWith("//"))
                        if (!line.StartsWith("--"))
                        {
                            String[] tmp = line.Split(new[] { '=' },
                                             StringSplitOptions.RemoveEmptyEntries);
                            paramList.Add(tmp[0], tmp[1]);
                        }
                }
            }
            catch (Exception ex) { LogIt("An Error Has Occurred: " + ex.Message, true); bReturn = false; }
            finally
            {
                if (sr != null)
                {
                    sr.Close();
                    //sr.Dispose();
                }
            }

            try
            {
                if (bReturn)
                    foreach (KeyValuePair<string, string> entry in paramList)
                        switch (entry.Key.ToString().ToLower())
                        {
                            case "userid": Vars.UserID = entry.Value.ToString(); break;
                            case "cuid": Vars.CUID = entry.Value.ToString(); break;
                            case "password": Vars.Password = entry.Value.ToString(); break;
                            case "cms": Vars.CMS = entry.Value.ToString(); break;
                            case "id": Vars.ID = entry.Value.ToString(); break;
                            case "startdate": Vars.StartDate = ParseDate(entry.Value.ToString(), true); break;
                            case "enddate": Vars.EndDate = ParseDate(entry.Value.ToString(), false); break;
                            case "csv": if (entry.Value.ToString().ToLower() == "yes") Vars.CSV = true; break;
                            case "verbose": if (entry.Value.ToString().ToLower() == "yes") Vars.Verbose = true; break;
                            case "queryoutput": if (entry.Value.ToString().ToLower() == "yes") Vars.QueryOutput = true; break;
                            case "recursive": if (entry.Value.ToString().ToLower() == "yes") Vars.Recursive = true; break;
                            case "delete": if (entry.Value.ToString().ToLower() == "yes") Vars.DeleteInstances = true; break;
                            case "confirm": if (entry.Value.ToString().ToLower() == "yes") Vars.VerifyDelete = true; break;
                            case "failedonly": if (entry.Value.ToString().ToLower() == "yes") Vars.DeleteFailedOnly = true; break;
                        }
            }
            catch (Exception ex)
            {
                LogIt("An Error Has Occurred: " + ex.Message, true);
                bReturn = false;
            }

            return bReturn;
        }

        private static int ProcReportInstances(BOECommon CommonInfo, int ReportID, string Name, bool Verbose, bool CSV, bool Delete, bool Verify, bool FailedOnly, bool QueryOutput, DateTime StartDate, DateTime EndDate)
        {
            int iInstances = 0;

            string sStartDateParsed = StartDate.Year.ToString() + "." + StartDate.Month.ToString() + "." + StartDate.Day.ToString() + "." + StartDate.Hour + ":" + StartDate.Minute + ":" + StartDate.Second;
            string sEndDateParsed = EndDate.Year.ToString() + "." + EndDate.Month.ToString() + "." + EndDate.Day.ToString() + "." + EndDate.Hour + ":" + EndDate.Minute + ":" + EndDate.Second;
            if (Verbose) LogIt("Loading Instances for Report " + Name, true);
            String sInstQuery = "SELECT TOP 10000 SI_ID, SI_CUID, SI_NAME, SI_UPDATE_TS, SI_CREATION_TIME FROM CI_INFOOBJECTS WHERE SI_INSTANCE_OBJECT = 1 AND SI_PARENTID = " + ReportID.ToString();
            if (FailedOnly) sInstQuery = sInstQuery + " AND SI_SCHEDULE_STATUS IN (3)";
            else sInstQuery = sInstQuery + " AND SI_SCHEDULE_STATUS IN (1,3)";

            sInstQuery = sInstQuery + " AND SI_CREATION_TIME BETWEEN '" + sStartDateParsed + "' AND '" + sEndDateParsed + "'";
            if (QueryOutput) LogIt("Executing Query: " + sInstQuery, true);
            InfoObjects iObjects = CommonInfo.ExecuteRawQuery(sInstQuery);
            if (iObjects != null)
                if (iObjects.Count > 0)
                {
                    iInstances = iInstances + iObjects.Count;
                    if (Verbose) LogIt(iObjects.Count.ToString() + " Instances Found for Report " + Name, true);

                    bool bHasDeletedItems = false;
                    foreach (InfoObject iObject in iObjects)
                    {
                        if (CSV) Console.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\"", Name, iObject.Title, iObject.ID, iObject.Properties["SI_CREATION_TIME"], iObject.Properties["SI_UPDATE_TS"]);

                        if (Delete)
                        {
                            if (Verify)
                            {
                                LogIt("Deleting Instance: " + iObject.ID + " - " + iObject.Title + " - Last Updated On: " + iObject.Properties["SI_UPDATE_TS"], true);
                                iObject.DeleteNow();
                                iObjects.Delete(iObject);
                                bHasDeletedItems = true;
                            }
                            else
                            {
                                LogIt("Delete Instance: " + iObject.ID + " - " + iObject.Title + " - Last Updated On: " + iObject.Properties["SI_UPDATE_TS"] + "? (Yes/No/Cancel)", true);
                                string sResult = Console.ReadLine();
                                if (sResult.ToLower().StartsWith("y"))
                                {
                                    iObject.DeleteNow();
                                    iObjects.Delete(iObject);
                                    bHasDeletedItems = true;
                                }
                                if (sResult.ToLower().StartsWith("c"))
                                    throw new Exception("Cancel Deletion");
                            }
                        }
                        else if (Verbose) LogIt("Instance Found: " + iObject.ID + " - " + iObject.Title + " - Last Updated On: " + iObject.Properties["SI_UPDATE_TS"], true);
                    }
                    if (Delete)
                        if (bHasDeletedItems)
                            CommonInfo.CommitObjects(iObjects);
                }

            return iInstances;
        }

        private static int procReports(BOECommon CommonInfo, List<FolderObject> DirTree, bool Verbose, bool CSV, bool Delete, bool Verify, bool FailedOnly, bool QueryOutput, DateTime StartDate, DateTime EndDate, ref int Instances)
        {
            int iReports = 0;
            try
            {
                foreach (FolderObject fNode in DirTree)
                {
                    if (Verbose) LogIt("Loading Reports for folder " + fNode.Name, true);
                    String sReportQuery = "SELECT TOP 10000 SI_ID, SI_CUID, SI_NAME FROM CI_INFOOBJECTS WHERE SI_INSTANCE_OBJECT = 0 AND SI_KIND NOT IN ('Folder', 'FavoritesFolder') AND SI_PARENTID = " + fNode.Id.ToString();
                    if (QueryOutput) LogIt("Executing Query: " + sReportQuery, true);
                    InfoObjects iObjects = CommonInfo.ExecuteRawQuery(sReportQuery);
                    if (iObjects != null)
                        if (iObjects.Count > 0)
                        {
                            iReports = iReports + iObjects.Count;
                            if (Verbose) LogIt(iObjects.Count.ToString() + " Reports Found for folder " + fNode.Name, true);

                            foreach (InfoObject iObject in iObjects)
                            {
                                Instances = Instances + ProcReportInstances(CommonInfo, iObject.ID, iObject.Title, Verbose, CSV, Delete, Verify, FailedOnly, QueryOutput, StartDate, EndDate);
                            }
                        }
                    if (fNode.HasChildren && (fNode.Children != null)) { iReports = iReports + procReports(CommonInfo, fNode.Children, Verbose, CSV, Delete, Verify, FailedOnly, QueryOutput, StartDate, EndDate, ref Instances); }
                }
            }
            catch (Exception) { throw; }
            return iReports;
        }

        private static void StartProcessing(BOECommon CommonInfo, InstanceVars Vars)
        {
            DateTime dElapsed = DateTime.Now;

            if (Vars.Verbose) LogIt("Begin Processing Instances: " + dElapsed.ToLocalTime().ToString("MM/dd/yyyy hh:mm:ss tt"), true);
            if (Vars.Verbose) LogIt("Querying for instances between " + Vars.StartDate.ToShortDateString() + " and " + Vars.EndDate.ToShortDateString());
            List<FolderObject> fDirectories = loadFolders(CommonInfo, Vars.CUIDs, Vars.IDs, true, Vars.Verbose, Vars.Recursive, Vars.QueryOutput);

            if (Vars.Verbose) LogIt(fDirectories.Count.ToString() + " Folders Loaded", true);

            if (Vars.CSV)
            {
                traverseFolders(fDirectories, true);
            }

            try
            {
                if (Vars.CSV) LogIt("\"Folder Name\",\"Report Name\",\"Object ID\",\"Created On\",\"Updated On\"");
                int iInstCount = 0;
                int iReportCount = procReports(CommonInfo, fDirectories, Vars.Verbose, Vars.CSV, Vars.DeleteInstances, Vars.VerifyDelete, Vars.DeleteFailedOnly, Vars.QueryOutput, Vars.StartDateUtc, Vars.EndDateUtc, ref iInstCount);
                if (Vars.Verbose) LogIt("", true);
                if (Vars.Verbose) LogIt(iInstCount.ToString() + " Instances Processed in " + iReportCount.ToString() + " Reports", true);
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("Cancel Deletion"))
                    LogIt("Deletion Operation Cancelled", true);
                else
                    throw new Exception(ex.Message);
            }
            if (Vars.Verbose) LogIt("", true);
            if (Vars.Verbose) LogIt("User " + Vars.UserID + " Logging Off CMS: " + Vars.CMS, true);
            CommonInfo.LogOff();

            DateTime dComplete = DateTime.Now;
            LogIt("", true);
            LogIt("Completed on: " + dComplete.ToLocalTime().ToString("MM/dd/yyyy hh:mm:ss tt"), true);
            TimeSpan diff = dComplete.Subtract(dElapsed);
            String res = String.Format("{0:00}:{1:00}:{2:00}.{3:##}", diff.Hours, diff.Minutes, diff.Seconds, diff.Milliseconds);
            LogIt("Elapsed Time: " + res);
        }

        private static void traverseFolders(List<FolderObject> fDirectories, bool InitialRun)
        {
            try
            {
                if (InitialRun) LogIt("\"Folder Name\",\"Parent Folder\",\"Object ID\",\"Object CUID\", \"Parent CUID\"");
                foreach (FolderObject fNode in fDirectories)
                {
                    Console.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\"", fNode.Name, fNode.ParentName, fNode.Id, fNode.CUID, fNode.ParentCUID);
                    if (fNode.HasChildren && (fNode.Children != null)) traverseFolders(fNode.Children, false);
                }
                if (InitialRun) LogIt("");
            }
            catch (Exception e)
            {
                LogIt("An Error Has Occurred: " + e.Message);
            }
        }
    }

    public class InstanceVars
    {
        private bool bCSV;
        private bool bDeleteInstances;
        private bool bQueryOutput;
        private bool bRecursive;
        private bool bVerbose;
        private bool bVerifyDelete;
        private DateTime dEndDate;
        private DateTime dStartDate;
        private string sCMS;
        private string sCUID;
        private string sID;
        private string sPassword;
        private string sUserID;
        private bool bFailedOnly;

        public InstanceVars()
        {
            sCUID = "";
            sUserID = "";
            sPassword = "";
            sCMS = "";
            sID = "";
            dStartDate = new DateTime(1970, 1, 1);
            dEndDate = DateTime.Today.AddDays(1).AddSeconds(-1);
            bVerbose = false;
            bQueryOutput = false;
            bCSV = false;
            bRecursive = false;
            bDeleteInstances = false;
            bVerifyDelete = false;
            bFailedOnly = false;
        }

        public string CMS
        {
            get { return sCMS; }
            set { sCMS = value; }
        }

        public bool CSV
        {
            get { return bCSV; }
            set { bCSV = value; }
        }

        public string CUID
        {
            get { return sCUID; }
            set { sCUID = value; }
        }

        public string[] CUIDs
        {
            get
            {
                string[] rCUIDs = null;
                try
                {
                    if (sCUID.Contains("'")) sCUID = sCUID.Replace("'", "");
                    if (sCUID.Contains("\"")) sCUID = sCUID.Replace("\"", "");
                    if (sCUID.Contains(","))
                    {
                        rCUIDs = sCUID.Split(',');
                    }
                    else
                    {
                        rCUIDs = new string[1];
                        rCUIDs[0] = sCUID;
                    }
                }
                catch (Exception) { }
                return rCUIDs;
            }
        }

        public bool DeleteInstances
        {
            get { return bDeleteInstances; }
            set { bDeleteInstances = value; }
        }

        public bool DeleteFailedOnly
        {
            get { return bFailedOnly; }
            set { bFailedOnly = value; }
        }

        public DateTime EndDate
        {
            get { return dEndDate; }
            set { dEndDate = value; }
        }

        public DateTime EndDateUtc
        {
            get { return dEndDate.ToUniversalTime(); }
        }

        public string ID
        {
            get { return sID; }
            set { sID = value; }
        }

        public string[] IDs
        {
            get
            {
                string[] rIDs = null;
                try
                {
                    if (sID.Contains(","))
                    {
                        rIDs = sID.Split(',');
                    }
                    else
                    {
                        rIDs = new string[1];
                        rIDs[0] = sID;
                    }
                }
                catch (Exception) { }
                return rIDs;
            }
        }

        public string Password
        {
            get { return sPassword; }
            set { sPassword = value; }
        }

        public bool QueryOutput
        {
            get { return bQueryOutput; }
            set { bQueryOutput = value; }
        }

        public bool Recursive
        {
            get { return bRecursive; }
            set { bRecursive = value; }
        }

        public DateTime StartDate
        {
            get { return dStartDate; }
            set
            {
                dStartDate = value;
            }
        }

        public DateTime StartDateUtc
        {
            get { return dStartDate.ToUniversalTime(); }
        }

        public string UserID
        {
            get { return sUserID; }
            set
            {
                if (value.Contains("@"))
                {
                    sCMS = value.Substring(value.IndexOf("@") + 1);
                    sUserID = value.Substring(0, value.IndexOf("@"));
                }
                else sUserID = value;
            }
        }

        public bool Verbose
        {
            get { return bVerbose; }
            set { bVerbose = value; }
        }

        public bool VerifyDelete
        {
            get { return bVerifyDelete; }
            set { bVerifyDelete = value; }
        }
    }
}