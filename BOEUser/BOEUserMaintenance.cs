﻿using CrystalDecisions.Enterprise;
using CrystalDecisions.Enterprise.Desktop;
using System;
using System.Collections.Generic;

namespace DFT.BOE
{
    public struct AliasInfo
    {
        public string ID;
        public string Alias;
        public string UserName;
        public string UserFullName;
    }

    public class BOEUserMaintenance : IDisposable
    {
        internal const string C_GET_USER =
          "Select Top 1* From CI_SYSTEMOBJECTS Where SI_KIND='User' and SI_NAME='{0}' and SI_KIND = 'User'";

        private BOECommon _common;
        private bool canDispose = true;

        #region Class_Methods

        /// <summary>
        /// Constructor - create the BOECommon object that holds the connection
        /// to the CMS.
        /// <param name="userName">The User ID for logging in to the CMS</param>
        /// <param name="password">The Password for loggin in to the CMS</param>
        /// <param name="cms">The CMS to log in to</param>
        /// </summary>
        public BOEUserMaintenance(string userName, string password, string cms)
        {
            clearErr();
            try
            {
                _common = new BOECommon(userName, password, cms);
                ErrMsg = BOECommon.ErrMsg;
                StackTrace = BOECommon.StackTrace;
            }
            catch (Exception ex)
            {
                ErrMsg = ex.Message;
                StackTrace = ex.StackTrace;
            }
        }

        public BOEUserMaintenance(BOECommon common)
        {
            clearErr();
            _common = common;
            canDispose = false;  //we don't want to dispose the BOECommon because it was passed in to the object.
        }

        ~BOEUserMaintenance()
        {
            Dispose(true);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool Managed)
        {
            if (canDispose && (_common != null))
            {
                try
                {
                    _common.Dispose();
                    _common = null;
                    System.GC.SuppressFinalize(this);
                }
                catch
                { }  //eat it because we don't want any _errMsgs here
            }
        }

        /// <summary>
        /// Any error message that occurs when processing a user
        /// </summary>
        public string ErrMsg { get; private set; }

        public string StackTrace { get; private set; }
        public string UserRptList { get; set; }

        #endregion Class_Methods

        #region Public_Methods

        /// <summary>
        /// Create a new user.
        /// </summary>
        /// <param name="usr">A BOEUserInfo object that contains the information about the new user.</param>
        /// <returns>True if successful.</returns>
        public bool CreateUser(BOEUserInfo usr)
        {
            clearErr();
            try
            {
                if (!_common.ObjectExistsByName(usr.Name, "SI_KIND = 'User'", BOECommon.CETables.CI_SYSTEMOBJECTS))
                {
                    addUser(usr);
                    return true;
                }
                else
                {
                    ErrMsg = string.Format("User {0} already exists", usr.Name);
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "Unable to Create User.  Err=" + ex.Message;
            }
            return false;
        }

        /// <summary>
        /// Search for the specified user.  If the user doesn't exist, add it.  If it does exist, update it.
        /// </summary>
        /// <param name="usr"></param>
        /// <returns>True if successful.</returns>
        public bool AddUpdateUser(BOEUserInfo usr)
        {
            clearErr();
            string query = string.Format("Select * from CI_SYSTEMOBJECTS where SI_NAME = '{0}' and SI_KIND = 'User'", usr.Name);
            try
            {
                using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
                {
                    if (BOECommon.StackTrace != string.Empty) //there was an error running the query
                    {
                        ErrMsg = BOECommon.ErrMsg;
                        StackTrace = BOECommon.StackTrace;
                        return false;
                    }
                    else if ((iobjs != null) && (iobjs.Count > 0))
                    {
                        return updateUser(usr, iobjs);
                    }
                    else
                    {
                        return addUser(usr);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "Unable to AddUpdate User.  Err=" + ex.Message;
                StackTrace = ex.StackTrace;
            }
            return false;
        }

        /// <summary>
        /// Delete the specified user.
        /// </summary>
        /// <param name="userName">The user ID to be deleted.</param>
        /// <returns>True if successful.</returns>
        public bool DeleteUser(string userName)
        {
            clearErr();
            try
            {
                string query = string.Format("Select SI_ID, SI_NAME from CI_SYSTEMOBJECTS where SI_NAME = '{0}' ans SI_KIND = 'User'", userName);
                using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
                {
                    if (BOECommon.StackTrace != string.Empty) //there was an error running the query
                    {
                        ErrMsg = BOECommon.ErrMsg;
                        StackTrace = BOECommon.StackTrace;
                        return false;
                    }
                    else if ((iobjs != null) && (iobjs.Count > 0)) //the user was found
                    {
                        deleteUser(iobjs);
                        return true;
                    }
                    else //the user wasn't found
                    {
                        ErrMsg = string.Format("User = {0} does not exist in the CMS.", userName);
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "Unable to Delete User. Err=" + ex.Message;
                StackTrace = ex.StackTrace;
            }
            return false;
        }

        /// <summary>
        /// Delete the specified user.
        /// </summary>
        /// <param name="id">The SI_ID of the user to be deleted.</param>
        /// <returns>True if successful.</returns>
        public bool DeleteUser(int id)
        {
            clearErr();
            try
            {
                string query = "Select SI_ID, SI_NAME from CI_SYSTEMOBJECTS where SI_ID = " + id.ToString();
                using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
                {
                    if (BOECommon.StackTrace != string.Empty) //there was an error running the query
                    {
                        ErrMsg = BOECommon.ErrMsg;
                        StackTrace = BOECommon.StackTrace;
                        return false;
                    }
                    else if ((iobjs != null) && (iobjs.Count > 0)) //the user was found
                    {
                        deleteUser(iobjs);
                        return true;
                    }
                    else //the user wasn't found
                    {
                        ErrMsg = string.Format("The user with SI_ID = {0} does not exist in the CMS.", id);
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "Unable to Delete User. Err=" + ex.Message;
            }
            return false;
        }

        public bool UpdateAlias(string mainID, string fullName, string email, string aliasID)
        {
            bool result = false;
            bool canAlias = false;
            clearErr();
            string query = string.Format("Select SI_ID, SI_NAME, SI_USERFULLNAME, SI_EMAIL_ADDRESS, SI_ALIASES, SI_USERGROUPS from CI_SYSTEMOBJECTS where SI_NAME = '{0}' and SI_KIND = 'User'", mainID);
            using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
            {
                if ((iobjs == null) || (BOECommon.StackTrace != string.Empty))
                {
                    ErrMsg = BOECommon.ErrMsg;
                    StackTrace = BOECommon.StackTrace;
                }
                else if (iobjs.Count == 0)
                {
                    ErrMsg = string.Format("BOXIID: {0} FullName: {1} not found in BO.", mainID, fullName);
                }
                else
                {
                    try
                    {
                        User toUser = (User)iobjs[1];
                        bool doUpdate = false;
                        if (aliasID != string.Empty) //we need to add the alias
                        {
                            InfoObject iobj = locateFromUser(aliasID);
                            if (iobj == null)
                            {
                                ErrMsg = string.Format("ALIAS_ID: {0} for BOXIID: {1} FullName: {2} not found in BO. ", aliasID, mainID, fullName);
                            }
                            else //The user we want to add exists.
                            {
                                canAlias = true;
                                User fromUser = (User)iobj;
                                result = addAssignedGroups(fromUser, toUser);
                                result = result && moveFavorites(fromUser.Title, toUser.Title, toUser.ID);
                                result = result && changeReportOwners(fromUser.ID, toUser.ID, toUser.Title);
                                result = result && movePersonalCategories(fromUser.Title, toUser.Title);
                                result = result && moveInboxes(fromUser, toUser);
                                doUpdate = (result && addAlias(fromUser, toUser)) || doUpdate;
                            }
                        }
                        doUpdate = (result && updateUserDetails(toUser, fullName, email)) || doUpdate;
                        if (result && doUpdate && !canAlias)
                            result = verifyAlias(toUser, aliasID, fullName);
                        else
                            result = true;
                        if (doUpdate && result)
                            toUser.Save();
                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ex.Message;
                        StackTrace = ex.StackTrace;
                        result = false;
                    }
                }
            }
            return result;
        }

        public bool UpdateUserInfo(string id, string fullName, string email)
        {
            clearErr();
            bool result = false;
            InfoObject iobj = _common.GetObjectFromName(id, "User", BOECommon.CETables.CI_SYSTEMOBJECTS);
            if (iobj == null)
            {
                ErrMsg = string.Format("User ID: {0} FullName: {1} not found in BO", id, fullName);
            }
            else
            {
                User usr = (User)iobj;
                bool saveIt = false;
                try
                {
                    if (usr.FullName.ToUpper() != fullName.ToUpper())
                    {
                        usr.FullName = fullName;
                        saveIt = true;
                    }
                    if (usr.EmailAddress.ToUpper() != email.ToUpper())
                    {
                        usr.EmailAddress = email;
                        saveIt = true;
                    }
                    if (saveIt)
                    {
                        usr.Save();
                        result = true;
                    }
                    else
                    {
                        result = true;
                        ErrMsg = "No Update Required";
                    }
                }
                catch (Exception ex)
                {
                    ErrMsg = ex.Message;
                    StackTrace = ex.StackTrace;
                }
            }
            return result;
        }

        public bool MergeUsers(string toUserID, List<string> fromUsers, bool doDelete)
        {
            clearErr();
            bool result = false;

            User toUser = getUser(toUserID);
            if (toUser != null)
            {
                foreach (string fromUserID in fromUsers)
                {
                    User fromUser = getUser(fromUserID);
                    if (fromUser != null)
                    {
                        result = addAssignedGroups(fromUser, toUser);
                        result = result && moveFavorites(fromUserID, toUserID, toUser.ID);
                        result = result && changeReportOwners(fromUser.ID, toUser.ID, toUserID);
                        result = result && movePersonalCategories(fromUserID, toUserID);
                        result = result && moveInboxes(fromUser, toUser);
                        if (result)
                        {
                            try
                            {
                                toUser.Save();
                                int i = int.Parse(fromUserID);
                                //if we made it to here, the from user ID is a number - don't delete
                                //but we will update the description
                                fromUser.Description = "REVIEW FOR DELETE: User also has GVS ID " + toUserID;
                                fromUser.Save();
                            }
                            catch
                            {
                                //this bo user is not a GVS user, delete them.
                                if (doDelete)
                                {
                                    result = deleteUser(fromUser);
                                }
                                else
                                {
                                    fromUser.Description = "REVIEW FOR DELETE:  Merged to user " + toUserID;
                                    fromUser.Save();
                                }
                            }
                        }
                    }
                    if (!result)
                    {
                        break;
                    }
                }
            }
            return result;
        }

        public int GetUserFavoritesId(string userName)
        {
            int result = -1;
            using (InfoObject iobj = _common.GetObjectFromName(userName, "FavoritesFolder", BOECommon.CETables.CI_INFOOBJECTS))
            {
                if (BOECommon.StackTrace == string.Empty && iobj != null)
                {
                    result = iobj.ID;
                }
                else
                {
                    if (BOECommon.ErrMsg == string.Empty)
                    {
                        ErrMsg = "Favorites Folder not found for user " + userName;
                    }
                    else
                    {
                        ErrMsg = BOECommon.ErrMsg;
                        StackTrace = BOECommon.StackTrace;
                    }
                }
            }
            return result;
        }

        public int GetFolderFromFavorites(string userName, string fldrName, string fldrDescrip, bool createFolder)
        {
            clearErr();
            int result = 0;
            int fav = GetUserFavoritesId(userName);
            string query = string.Format("Select SI_ID from CI_INFOOBJECTS where SI_PARENTID = {0} and SI_NAME = '{1}'", fav, fldrName);
            using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
            {
                if (BOECommon.StackTrace != string.Empty)
                {
                    ErrMsg = BOECommon.ErrMsg;
                    StackTrace = BOECommon.StackTrace;
                }
                else if (iobjs.Count == 0)
                {
                    if (createFolder)
                    {
                        result = _common.CreateObject("CrystalEnterprise.Folder", fldrName, fldrDescrip, fav, BOECommon.CETables.CI_INFOOBJECTS);
                    }
                    else
                    {
                        ErrMsg = string.Format("Unable to find folder {0} for user {1}", fldrName, userName);
                    }
                }
                else
                {
                    result = iobjs[1].ID;
                }
            }
            return result;
        }

        public bool DeactivateUser(BOEUserInfo usr, bool enterpriseOnly)
        {
            bool result = true;
            clearErr();
            string qry = "Select SI_ID, SI_NAME, SI_ALIASES from CI_SYSTEMOBJECTS where SI_KIND = 'User' and {0}";
            if (usr.ID > 0)
            {
                qry = string.Format(qry, "SI_ID = " + usr.ID.ToString());
            }
            else
            {
                qry = string.Format(qry, "SI_NAME = " + usr.Name);
            }
            try
            {
                using (InfoObjects iobjs = _common.ExecuteRawQuery(qry))
                {
                    if (iobjs != null && iobjs.Count > 0)
                    {
                        User userobj = (User)iobjs[1];
                        bool updated = false;
                        foreach (UserAlias alias in userobj.Aliases)
                        {
                            if (!enterpriseOnly || (alias.Authentication.ToUpper() == "ENTERPRISE"))
                            {
                                alias.Disabled = true;
                                updated = true;
                            }
                        }
                        if (updated)
                        {
                            userobj.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                ErrMsg = ex.Message;
                StackTrace = ex.StackTrace;
            }
            return result;
        }

        #endregion Public_Methods

        #region Private_Methods

        private void clearErr()
        {
            ErrMsg = string.Empty;
            StackTrace = string.Empty;
        }

        /// <summary>
        /// Add a user to the CMS
        /// </summary>
        /// <param name="user">The user to be added</param>
        /// <returns></returns>
        private bool addUser(BOEUserInfo user)
        {
            bool result = true;
            try
            {
                using (PluginInfo pi = _common.GetPluginInfo("CrystalEnterprise.User"))
                {
                    InfoObjects userObjs = _common.NewInfoObjects();
                    userObjs.Add(pi);
                    User newUser = (User)userObjs[1];
                    newUser.Title = user.Name;
                    newUser.FullName = user.FullName;
                    newUser.Connection = user.Connection;
                    newUser.PasswordExpires = user.PasswordExpires;
                    newUser.ChangePasswordAtNextLogon = user.ChangePasswordAtNextLogon;
                    newUser.AllowChangePassword = user.AllowChangePassword;
                    newUser.NewPassword = user.Password;

                    newUser.Description = user.Description;
                    if (user.DefaultGroupId > 0)
                    {
                        newUser.Groups.Add(user.DefaultGroupId);
                    }
                    newUser.Save();
                }
                if (BOECommon.ErrMsg != string.Empty)
                {
                    result = false;
                    ErrMsg = String.Format("Unable to add user {0}. Err={1}", user.Name, BOECommon.ErrMsg);
                }
            }
            catch (Exception ex)
            {
                result = false;
                ErrMsg = String.Format("Unable to add user {0}. Err={1}", user.Name, ex.Message);
            }
            return result;
        }

        /// <summary>
        /// Verifies the properties of a User InfoObject against data in BOEUserInfo.
        /// If there are differences, the user is updated in the CMS.
        /// </summary>
        /// <param name="user">The BOEUserInfo to compare against</param>
        /// <param name="iobjs">An InfoObjects collection that contains the user to be updated.</param>
        /// <returns></returns>
        private bool updateUser(BOEUserInfo user, InfoObjects iobjs)
        {
            ErrMsg = string.Empty;
            bool updated = false;
            try
            {
                User usrObj = (User)iobjs[1];
                if (usrObj.FullName != user.FullName)
                {
                    usrObj.FullName = user.FullName;
                    updated = true;
                }
                if (user.Password != string.Empty)
                {
                    usrObj.NewPassword = user.Password;
                    updated = true;
                }
                if (usrObj.Connection != user.Connection)
                {
                    usrObj.Connection = user.Connection;
                    updated = true;
                }
                if (usrObj.PasswordExpires != user.PasswordExpires)
                {
                    usrObj.PasswordExpires = user.PasswordExpires;
                    updated = true;
                }
                if (usrObj.ChangePasswordAtNextLogon != user.ChangePasswordAtNextLogon)
                {
                    usrObj.ChangePasswordAtNextLogon = user.ChangePasswordAtNextLogon;
                    updated = true;
                }
                if (usrObj.AllowChangePassword != user.AllowChangePassword)
                {
                    usrObj.AllowChangePassword = user.AllowChangePassword;
                    updated = true;
                }
                if (user.DefaultGroupId > 0)
                {
                    bool grpFound = false;
                    foreach (int grp in usrObj.Groups)
                    {
                        if (grp == user.DefaultGroupId)
                        {
                            grpFound = true;
                            break;
                        }
                    }
                    if (!grpFound)
                    {
                        usrObj.Groups.Add(user.DefaultGroupId);
                        updated = true;
                    }
                }
                if (updated)
                {
                    usrObj.Save();
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "Unable to Update User.  Err=" + ex.Message;
                StackTrace = ex.StackTrace;
            }
            return false;
        }

        /// <summary>
        /// Delete a user from the CMS based on an InfoObjects collection containing the user info.
        /// </summary>
        /// <param name="userObjs"></param>
        /// <returns></returns>
        private bool deleteUser(InfoObjects userObjs)
        {
            bool result = true;
            if (userObjs.Count > 0) //if the user hasn't already been deleted....
            {
                try
                {
                    //first, delete the user's schedules and instances
                    using (BOEFolderRptMgmt frmgmt = new BOEFolderRptMgmt(_common))
                    {
                        if (frmgmt.DeleteUserInstances(userObjs[1].ID))
                        {
                            userObjs.Delete(userObjs[1]);
                            _common.CommitObjects(userObjs);
                        }
                        else
                        {
                            ErrMsg = frmgmt.ErrMsg;
                            StackTrace = frmgmt.StackTrace;
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (ex.Message.IndexOf("was not found in the collection") == 0)
                    {
                        throw;
                    }
                }
            }
            return result;
        }

        private bool deleteUser(User user)
        {
            try
            {
                user.DeleteNow();
                return true;
            }
            catch (Exception ex)
            {
                ErrMsg = ex.Message;
                StackTrace = ex.StackTrace;
                return false;
            }
        }

        /// <summary>
        /// Users may have an Enterprise ID that exactly matches userID and an LDAP id that has "0" on the end.
        /// Or they may have just an LDAP ID that exactly matches the userID.  In either case, we want the
        /// LDAP object only for processing.
        /// </summary>
        /// <param name="userId">The user ID to look for.</param>
        /// <returns></returns>
        private InfoObject locateFromUser(string userId)
        {
            string query = string.Format("Select SI_ID, SI_NAME, SI_USERFULLNAME, SI_EMAIL_ADDRESS, SI_ALIASES, SI_USERGROUPS " +
                           "from CI_SYSTEMOBJECTS where SI_NAME = '{0}' or SI_NAME = '{1}' and SI_KIND = 'User'", userId, userId + "0");
            using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
            {
                if (iobjs != null)
                {
                    foreach (InfoObject iobj in iobjs)
                    {
                        if (iobj.Kind == "User")
                        {
                            User usr = (User)iobj;
                            foreach (UserAlias alias in usr.Aliases)
                            {
                                if (alias.Authentication != "secEnterprise")
                                    return iobj;
                            }
                        }
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Copy one user's group assignments to another user
        /// </summary>
        /// <param name="fromUser">The user to copy from.</param>
        /// <param name="toUser">The user to copy to.</param>
        /// <returns>True if at least 1 group is copied to the new user.</returns>
        private bool addAssignedGroups(User fromUser, User toUser)
        {
            bool result = true;
            foreach (int fromGrp in fromUser.Groups)
            {
                bool found = false;
                bool entGroup = true;

                //If this isn't an Enterprise group we can't use it...
                using (UserGroup grp = (UserGroup)_common.GetObjectById(fromGrp, "SI_ID, SI_ALIASES", BOECommon.CETables.CI_SYSTEMOBJECTS))
                {
                    foreach (UserGroupAlias alias in grp.Aliases)
                    {
                        if (alias.Authentication != "secEnterprise")
                            entGroup = false;
                    }
                    if (entGroup)
                    {
                        foreach (int toGrp in toUser.Groups)
                        {
                            if (fromGrp == toGrp)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            toUser.Groups.Add(fromGrp);
                            grp.Users.Add(toUser.ID);
                        }
                    }
                }
            }
            if (result)
            {
                try
                {
                    toUser.Save();
                }
                catch (Exception ex)
                {
                    result = false;
                    ErrMsg = ex.Message;
                    StackTrace = ex.StackTrace;
                }
            }
            //save the group update
            if (!result)
            {
                ErrMsg = "Unable to copy groups from user " + fromUser.Title + " to user " + toUser.Title;
            }
            return result;
        }

        private bool addAlias(User fromUser, User toUser)
        {
            bool result = false;
            bool hasEnterprise = false;
            foreach (UserAlias alias in toUser.Aliases)
            {
                //Only one Enterprise alias is allowed per user, so we need to check for this....
                if (alias.Authentication == "secEnterprise")
                    hasEnterprise = true;
            }

            foreach (UserAlias alias in fromUser.Aliases)
            {
                if (hasEnterprise && (alias.Authentication == "secEnterprise"))
                {
                    //we can't use an Enterprise Alias if toUser already has an enterprise alias
                    ErrMsg += string.Format("Alias Update Failed: Unable to add Enterprise alias {0} to user {1}.  User {1} already has an Enterprise Alias./n", alias.ID, toUser.Title);
                }
                else
                {
                    //but we can use any other type of Alias
                    string id = (alias.ID.Contains(alias.Authentication) ? alias.ID : string.Format("{0}:{1}", alias.Authentication, alias.ID));
                    toUser.Aliases.AddExisting(alias.Name, id, false);
                    ErrMsg += string.Format("Alias Update Success: {0} alias {1} added to user {2}.", alias.Authentication, fromUser.Title, toUser.Title);
                    result = true;
                }
            }
            return result;
        }

        private bool updateUserDetails(User toUser, string fullName, string email)
        {
            bool result = false;
            if (toUser.FullName.ToUpper() != fullName.ToUpper())
            {
                toUser.FullName = fullName;
                result = true;
            }
            if (toUser.EmailAddress.ToUpper() != email.ToUpper())
            {
                toUser.EmailAddress = email;
                result = true;
            }
            if (result)
            {
                ErrMsg += string.Format("FullName and Email Address updated for user {0} ", toUser.Title);
            }
            return result;
        }

        private bool verifyAlias(User toUser, string userID, string fullName)
        {
            bool result = false;
            //NOTE:  We cannot do a direct compare on the aliases that come from LDAP, WinAD, etc because
            //       the ID of the Alias doesn't contain just the userID value - it may contain other data
            //       as well, so we'll assume that if we find the userID value in the ID of the alias that
            //       everything is good.  This DOES NOT verify the authentication type though!
            foreach (UserAlias alias in toUser.Aliases)
            {
                result = result || alias.Name.ToUpper().Contains(userID.ToUpper()) || alias.Name.ToUpper().Contains(fullName.ToUpper());
            }
            if (result)
            {
                ErrMsg += string.Format("ALIAS_ID: {0} is already assigned to BOXIID: {1} ", userID, toUser.Title);
            }
            return result;
        }

        private bool moveFavorites(string fromUser, string toUser, int toUserID)
        {
            bool result = false;
            int toFolderId = _common.GetIDFromName(toUser, "FavoritesFolder", BOECommon.CETables.CI_INFOOBJECTS);
            int fromFolderId = _common.GetIDFromName(fromUser, "FavoritesFolder", BOECommon.CETables.CI_INFOOBJECTS);

            if ((toFolderId > 0) && (fromFolderId > 0))
            {
                using (BOEFolderRptMgmt frmgmt = new BOEFolderRptMgmt(_common))
                {
                    result = frmgmt.MergeFolders(fromFolderId, toFolderId, toUser, toUserID);
                    if (frmgmt.ErrMsg != string.Empty)
                    {
                        ErrMsg = frmgmt.ErrMsg;
                        StackTrace = frmgmt.StackTrace;
                    }
                }
            }
            if ((ErrMsg == string.Empty) && !result)
            {
                ErrMsg = "Unable to copy folders from user " + fromUser + " to user " + toUser;
            }
            return result;
        }

        private bool changeReportOwners(int fromUserID, int toUserID, string toUserName)
        {
            bool result = false;
            using (BOEFolderRptMgmt frmgmt = new BOEFolderRptMgmt(_common))
            {
                result = frmgmt.ChangeReportOwners(fromUserID, toUserID, toUserName);
                ErrMsg = frmgmt.ErrMsg;
            }
            if (!result && (ErrMsg == string.Empty))
            {
                ErrMsg = String.Format("Unable to change reports owners from user SI_ID {0} to user SI_ID {1}", fromUserID, toUserID);
            }
            return result;
        }

        private bool movePersonalCategories(string fromUser, string toUser)
        {
            bool result = false;
            InfoObject fromCat = _common.GetObjectFromName(fromUser, "PersonalCategory", BOECommon.CETables.CI_INFOOBJECTS);
            if (fromCat == null)
            {
                ErrMsg = BOECommon.ErrMsg;
                StackTrace = BOECommon.StackTrace;
            }
            else
            {
                InfoObject toCat = _common.GetObjectFromName(toUser, "PersonalCategory", BOECommon.CETables.CI_INFOOBJECTS);
                if (toCat == null)
                {
                    ErrMsg = BOECommon.ErrMsg;
                    StackTrace = BOECommon.StackTrace;
                }
                else
                {
                    //Get a list of the subcategories in the To category
                    List<string> toCats = new List<string>();
                    string query = string.Format("Select SI_ID, SI_NAME from CI_INFOOBJECTS where SI_KIND = 'PersonalCategory' and SI_PARENTID = {0}", toCat.ID);
                    using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
                    {
                        if (iobjs == null) //there was an error running the query
                        {
                            ErrMsg = BOECommon.ErrMsg;
                            StackTrace = BOECommon.StackTrace;
                        }
                        else  //we're not worried if there are no subcategories, so we're not checking for that...
                        {
                            foreach (InfoObject iobj in iobjs)
                            {
                                toCats.Add(iobj.Title);
                            }
                        }
                    }
                    if (StackTrace == string.Empty)
                    {
                        //First we move any subcategories
                        //Get the list of subcategories in the From category
                        query = string.Format("Select SI_ID, SI_NAME, SI_PARENTID from CI_INFOOBJECTS where SI_KIND = 'PersonalCategory' and SI_PARENTID = {0}", fromCat.ID);
                        using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
                        {
                            bool hasUpdates = false;
                            if (iobjs == null) //there was an error running the query
                            {
                                ErrMsg = BOECommon.ErrMsg;
                                StackTrace = BOECommon.StackTrace;
                            }
                            else //we're not worried if there are no subcategories, so we're not checking for that...
                            {
                                foreach (InfoObject iobj in iobjs)
                                {
                                    int copy = 1;
                                    string catName = iobj.Title;
                                    while (toCats.Contains(catName))
                                    {
                                        catName = String.Format("{0} (Copy {1})", iobj.Title, copy);
                                        copy++;
                                    }
                                    if (catName != iobj.Title) //set the subcategory name if a subcategory with that name already exists in the to category
                                    {
                                        iobj.Title = catName;
                                    }
                                    iobj.ParentID = toCat.ID;  //change the parent of the from category.
                                    hasUpdates = true;
                                }
                            }
                            //Now we move any documents under the main Personal Category
                            //Get the list of documents in the To category
                            List<int> toDocs = new List<int>();
                            Properties toProps = BOECommon.GetProperties(toCat.Properties, "SI_DOCUMENTS");
                            int fromCount = 0;
                            int currentDoc;
                            if (toProps != null)  //we have documents to work with - it's not an error if there are no docx
                            {
                                fromCount = BOECommon.GetPropertyInt(toProps, "SI_TOTAL");
                                for (int i = 1; i <= fromCount; i++)
                                {
                                    currentDoc = BOECommon.GetPropertyInt(toProps, i.ToString());
                                    if (currentDoc > 0)
                                    {
                                        toDocs.Add(currentDoc);
                                    }
                                }
                            }
                            //the the docs in the From category
                            Properties fromProps = BOECommon.GetProperties(fromCat.Properties, "SI_DOCUMENTS");
                            //we have to add the category to the report...
                            using (BOEFolderRptMgmt frmgmt = new BOEFolderRptMgmt(_common))
                            {
                                if (fromProps != null) //we have docs to move
                                {
                                    int toCount = BOECommon.GetPropertyInt(fromProps, "SI_TOTAL");
                                    for (int i = 1; i <= toCount; i++)
                                    {
                                        currentDoc = BOECommon.GetPropertyInt(fromProps, i.ToString());
                                        if (currentDoc > 0 && !toDocs.Contains(currentDoc))
                                        {
                                            frmgmt.AddPersonalCategory(currentDoc, toCat.ID);
                                            //the old personal category will be removed when the alias is merged so we don't have to delete it.
                                            toCount++;
                                            toProps.Add(toCount.ToString(), currentDoc);
                                            toProps["SI_TOTAL"].Value = toCount;
                                            hasUpdates = true;
                                        }
                                    }
                                }
                            }
                            result = true;
                            if (hasUpdates)
                            {
                                _common.CommitObjects(iobjs);
                            }
                        }
                    }
                }
            }
            if (!result && (ErrMsg == string.Empty))
            {
                ErrMsg = "Unable to move personal categories from user " + fromUser + " to user " + toUser;
            }
            return result;
        }

        private bool moveInboxes(User fromUser, User toUser)
        {
            bool result = true;
            int fromIbx = -1;
            int toIbx = -1;
            Properties props = BOECommon.GetProperties(fromUser.Properties, "SI_INBOX");
            if (props != null)
            {
                fromIbx = BOECommon.GetPropertyInt(props, "1");
            }

            props = BOECommon.GetProperties(toUser.Properties, "SI_INBOX");
            if (props != null)
            {
                toIbx = BOECommon.GetPropertyInt(props, "1");
            }

            if (fromIbx > 0 && toIbx > 0)
            {
                string query = string.Format("Select SI_ID, SI_NAME, SI_PARENTID, SI_OWNER from CI_INFOOBJECTS where SI_PARENTID = {0}", fromIbx);
                using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
                {
                    if (BOECommon.StackTrace != string.Empty)
                    {
                        ErrMsg = BOECommon.StackTrace;
                        StackTrace = BOECommon.StackTrace;
                        result = false;
                    }
                    else if (iobjs.Count > 0)  //we don't care if there are no objects - so no error for that!
                    {
                        foreach (InfoObject iobj in iobjs)
                        {
                            iobj.ParentID = toIbx;
                            _common.SetPropertyString(iobj.Properties, "SI_OWNER", toUser.Title);
                            _common.SetPropertyInt(iobj.Properties, "SI_OWNERID", toUser.ID);
                        }

                        result = _common.CommitObjects(iobjs);
                        if (!result)
                        {
                            ErrMsg = BOECommon.ErrMsg;
                            StackTrace = BOECommon.StackTrace;
                        }
                    }
                }
            }

            return result;
        }

        public List<AliasInfo> getAllUserIDs(string sNameSpace = "")
        {
            clearErr();
            List<AliasInfo> lUsers = new List<AliasInfo>();
            string query = "Select SI_ID, SI_NAME, SI_USERFULLNAME, SI_EMAIL_ADDRESS, SI_ALIASES, SI_USERGROUPS from CI_SYSTEMOBJECTS where SI_KIND = 'User'";
            using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
            {
                if ((iobjs != null) && (iobjs.Count > 0))
                {
                    User tUser;
                    AliasInfo ainfo = new AliasInfo();
                    foreach (InfoObject iobj in iobjs)
                    {
                        if (iobj.GetType() == typeof(User))
                        {
                            tUser = ((User)iobj);
                            ainfo = new AliasInfo();
                            try
                            {
                                foreach (UserAlias alias in tUser.Aliases)
                                {
                                    if (alias.Name.StartsWith(sNameSpace))
                                    {
                                        ainfo.ID = alias.ID;
                                        ainfo.UserName = tUser.Title;
                                        ainfo.UserFullName = tUser.FullName;
                                        ainfo.Alias = alias.Name;
                                        lUsers.Add(ainfo);
                                    }
                                }
                            }
                            catch (Exception) { }
                        }
                    }
                }
                else
                {
                    if (ErrMsg == string.Empty)
                        ErrMsg = "BOECommon.getAllUsers: Unable to find objects with si_kind User";
                }
            }
            return lUsers;
        }

        public User getUser(string toUser)
        {
            User user = null;
            InfoObject newObj = _common.GetObjectFromName(toUser, "User", BOECommon.CETables.CI_SYSTEMOBJECTS);
            if (BOECommon.StackTrace != string.Empty)
            {
                ErrMsg = BOECommon.ErrMsg;
                StackTrace = BOECommon.StackTrace;
            }
            else if (newObj == null)
            {
                ErrMsg = string.Format("Unable to find user {0} in BO.", toUser);
            }
            else
            {
                user = (User)newObj;
            }
            return user;
        }

        #endregion Private_Methods
    }
}