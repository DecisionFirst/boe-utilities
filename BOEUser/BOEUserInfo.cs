﻿using CrystalDecisions.Enterprise;
using CrystalDecisions.Enterprise.Desktop;

namespace DFT.BOE
{
    public class BOEUserInfo : BOEObjectInfo
    {
        public BOEUserInfo()
            : base()
        {
            FullName = string.Empty;
            EmailAddress = string.Empty;
            Password = string.Empty;
            Connection = CeConnectionType.ceConnectionConcurrent;
            PasswordExpires = true;
            ChangePasswordAtNextLogon = true;
            AllowChangePassword = true;
            DefaultGroupId = -1;
        }

        public BOEUserInfo(InfoObject iobj)
            : base(iobj)
        {
            User usr = (User)iobj;
            FullName = usr.FullName;
            EmailAddress = usr.EmailAddress;
            Password = string.Empty;
            Connection = usr.Connection;
            PasswordExpires = usr.PasswordExpires;
            ChangePasswordAtNextLogon = usr.ChangePasswordAtNextLogon;
            AllowChangePassword = usr.AllowChangePassword;
            DefaultGroupId = -1;
        }

        public string FullName { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public bool PasswordExpires { get; set; }
        public bool ChangePasswordAtNextLogon { get; set; }
        public bool AllowChangePassword { get; set; }
        public CeConnectionType Connection { get; set; }
        public int DefaultGroupId { get; set; }

        public string ConnectType
        {
            get
            {
                string result = string.Empty;
                switch (Connection)
                {
                    case CeConnectionType.ceConnectionConcurrent:
                        result = "Concurrent";
                        break;

                    case CeConnectionType.ceConnectionNamed:
                        result = "Named";
                        break;
                }
                return result;
            }

            set
            {
                switch (value)
                {
                    case "Concurrent":
                        Connection = CeConnectionType.ceConnectionConcurrent;
                        break;

                    case "Named":
                        Connection = CeConnectionType.ceConnectionNamed;
                        break;
                }
            }
        }
    }
}