﻿using Amazon.AWSMarketplaceMetering;
using Amazon.AWSMarketplaceMetering.Model;

//using Amazon.SimpleDB;
//using Amazon.SimpleDB.Model;
using Amazon.Runtime;
using DFT.BOE.Framework;
using DFT.BOE.Security;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Threading;
using System.Timers;
using Microsoft.Win32;

namespace DFT.BOE
{
    /// <summary>
    /// The actual implementation of the windows service goes here...
    /// </summary>
    [WindowsService("BOEAWSHeartbeat",
        DisplayName = "BOEAWSHeartbeat",
        Description = "BusinessObjects Amazon Web Services Heartbeat Monitor",
        EventLogSource = "BOEAWSHeartbeat",
        StartMode = ServiceStartMode.Automatic)]
    internal class BOEAWSHeartbeat : IWindowsService
    {
        private static System.Threading.Timer aTimer;

        private static bool bDryRun = true;

        private static string fileName = "UserInfo.csv";

        private static string fullfilename = "";

        private static int iPeriodSeconds = 3600;

        private static string sEventLogSource = "BOEAWSHeartbeat";

        private static string sLog = "Application";

        private static string sProductCode = "";

        //private static string sAccessID = "";
        //private static string sSecretAccessKey = "";
        private static string sRole = "";


        //private static String srcPath = ".\\";
        //Use path of running exe as default source path
        private static String srcPath = Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);

        //Test Product Codes via AWS Market Place Seller Ops
        private static string[] sUsageDimensions = { "" }; //Test Usage Dimension via AWS Market Place Seller Ops

        //Perform Dry Run of Metrics Recording
        private enum EventLogStatus
        {
            INFO, WARN, ERROR
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
        }

        /// <summary>
        /// This method is called when a service gets a request to resume
        /// after a pause is issued.
        /// </summary>
        public void OnContinue()
        {
            LogEvent("Resumed BusinessObjects Amazon Web Services Heartbeat Monitor", EventLogStatus.INFO);
            aTimer = new System.Threading.Timer(new TimerCallback(OnTimedEvent), null, 0, 3600000);
        }

        /// <summary>
        /// This method is called when a custom command is issued to the service.
        /// </summary>
        /// <param name="command">The command identifier to execute.</param >
        public void OnCustomCommand(int command)
        {
        }

        /// <summary>
        /// This method is called when a service gets a request to pause,
        /// but not stop completely.
        /// </summary>
        public void OnPause()
        {
            LogEvent("Paused BusinessObjects Amazon Web Services Heartbeat Monitor", EventLogStatus.INFO);
            aTimer.Dispose();
            aTimer = null;
        }

        /// <summary>
        /// This method is called when the machine the service is running on
        /// is being shutdown.
        /// </summary>
        public void OnShutdown()
        {
        }

        /// <summary>
        /// This method is called when the service gets a request to start.
        /// </summary>
        /// <param name="args">Any command line arguments</param>
        public void OnStart(string[] args)
        {
            if (args.Count() > 0) fullfilename = args[0];

            ProcessRegistrySettings();

            DateTime dtRunning = DateTime.Now;
            int iPeriodMillisecond = iPeriodSeconds * 1000;

            LogEvent("Starting BusinessObjects Amazon Web Services Heartbeat Monitor", EventLogStatus.INFO);
            aTimer = new System.Threading.Timer(new TimerCallback(OnTimedEvent), null, 0, iPeriodMillisecond);
            //aTimer.Elapsed += new System.Timers.ElapsedEventHandler(OnTimedEvent);
            //aTimer.Interval = 3600000;
        }

        /// <summary>
        /// This method is called when the service gets a request to stop.
        /// </summary>
        public void OnStop()
        {
            LogEvent("Shutdown BusinessObjects Amazon Web Services Heartbeat Monitor", EventLogStatus.INFO);
            //Shutdown Timer
            aTimer.Dispose();
            aTimer = null;
        }

        private static bool LogEvent(String EventMessage, EventLogStatus status)
        {
            bool bret = true;
            try
            {
                if (!EventLog.SourceExists(sEventLogSource))
                    EventLog.CreateEventSource(sEventLogSource, sLog);

                if (status == EventLogStatus.INFO)
                    EventLog.WriteEntry(sEventLogSource, EventMessage, EventLogEntryType.Information);

                if (status == EventLogStatus.WARN)
                    EventLog.WriteEntry(sEventLogSource, EventMessage, EventLogEntryType.Warning);

                if (status == EventLogStatus.ERROR)
                    EventLog.WriteEntry(sEventLogSource, EventMessage, EventLogEntryType.Error);
            }
            catch (Exception) { bret = false; }
            return bret;
        }

        // Specify what you want to happen when the Elapsed event is
        // raised.
        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            //Console.WriteLine("The Elapsed event was raised at {0}", e.SignalTime);
            bool bret = ProcessLicenses(e.SignalTime);
        }

        private static void OnTimedEvent(object source)
        {
            DateTime sDate = DateTime.Now;
            //Console.WriteLine("The Elapsed event was raised at {0}", e.SignalTime);
            bool bret = ProcessLicenses(sDate);
        }

        private static void ProcessRegistrySettings()
        {
            //
            try
            {
                RegistryKey Key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\BOEAWSHeartbeat", true);
                if (Key == null)
                {
                    Key = Registry.LocalMachine.CreateSubKey(@"SOFTWARE\BOEAWSHeartbeat");
                    Key.SetValue("FileName", "UserInfo.csv", RegistryValueKind.String);
                    Key.SetValue("DryRun", "true", RegistryValueKind.String);
                    Key.SetValue("Period", "3600", RegistryValueKind.String);
                    Key.SetValue("EventLogSource", "BOEAWSHeartbeat", RegistryValueKind.String);
                    Key.SetValue("EventLog", "Application", RegistryValueKind.String);
                    Key.SetValue("ProductCode", "", RegistryValueKind.String);
                    //Key.SetValue("AWSID", "", RegistryValueKind.String);
                    //Key.SetValue("AWSKey", "", RegistryValueKind.String);
                    Key.SetValue("AWSEnc", "false", RegistryValueKind.String);
                    Key.SetValue("AWSRole", "", RegistryValueKind.String);
                    Key.SetValue("Dimensions", new string[] { "" }, RegistryValueKind.MultiString);
                }

                bool conv;
                fileName = (string)Key.GetValue("FileName");

                conv = Boolean.TryParse((string)Key.GetValue("DryRun"), out bDryRun);
                if (!conv) bDryRun = true;

                conv = Int32.TryParse((string)Key.GetValue("Period"), out iPeriodSeconds);
                if (!conv) iPeriodSeconds = 3600;

                sEventLogSource = (string)Key.GetValue("EventLogSource");
                sLog = (string)Key.GetValue("EventLog");

                sProductCode = (string)Key.GetValue("ProductCode");

                //sAccessID = (string)Key.GetValue("AWSID");
                //sSecretAccessKey = (string)Key.GetValue("AWSKey");
                sRole = (string)Key.GetValue("AWSRole");

                bool bEnc = false;
                conv = bool.TryParse((string)Key.GetValue("AWSEnc"), out bEnc);

                //BOESecurity sec = new BOESecurity("BOEAWSHeartbeat");
                if (bEnc)
                {
                    //sAccessID = DecryptValue("BOEAWSHeartbeat", FromHexString(sAccessID));
                    //sSecretAccessKey = DecryptValue("BOEAWSHeartbeat", FromHexString(sSecretAccessKey));
                    sProductCode = DecryptValue("BOEAWSHeartbeat", FromHexString(sProductCode));
                    sRole = DecryptValue("BOEAWSHeartbeat", FromHexString(sRole));
                }
                else
                {
                    //String sID = EncryptValue("BOEAWSHeartbeat", sAccessID);
                    //String sPWD = EncryptValue("BOEAWSHeartbeat", sSecretAccessKey);
                    String sPID = EncryptValue("BOEAWSHeartbeat", sProductCode);
                    String sEncRole = EncryptValue("BOEAWSHeartbeat", sRole);


                    //Key.SetValue("AWSID", ToHexString(sID));
                    //Key.SetValue("AWSKey", ToHexString(sPWD));
                    Key.SetValue("ProductCode", ToHexString(sPID));
                    Key.SetValue("AWSRole", ToHexString(sEncRole));
                    Key.SetValue("AWSEnc", "true");
                }

                sUsageDimensions = (string[])Key.GetValue("Dimensions");

                Key.Close();
            }
            catch (Exception ex)
            {
                LogEvent("An Error Occurred Processing Registry: " + ex.Message, EventLogStatus.ERROR);
            }
        }

        public static string ToHexString(string str)
        {
            var sb = new System.Text.StringBuilder();

            var bytes = System.Text.Encoding.Unicode.GetBytes(str);
            foreach (var t in bytes)
            {
                sb.Append(t.ToString("X2"));
            }

            return sb.ToString(); // returns: "48656C6C6F20776F726C64" for "Hello world"
        }

        public static string FromHexString(string hexString)
        {
            var bytes = new byte[hexString.Length / 2];
            for (var i = 0; i < bytes.Length; i++)
            {
                bytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            }

            return System.Text.Encoding.Unicode.GetString(bytes); // returns: "Hello world" for "48656C6C6F20776F726C64"
        }

        private static string DecryptValue(string Key, string value)
        {
            BOESecurity sec = new BOESecurity(Key);
            int hashsize = 0;
            string hash = "";
            string sRet = "";
            if (Int32.TryParse(value.Substring(value.Length - 3, 3), out hashsize))
            {
                value = value.Substring(0, value.Length - 3);
                hash = value.Substring(0, hashsize);
                sRet = value.Substring(hashsize);
                sec.hash = sec.fromBase64(hash);
                sRet = sec.Decrypt(sRet);
            }

            return sRet;
        }

        private static string EncryptValue(string Key, string value)
        {
            BOESecurity sec = new BOESecurity(Key);
            int hashsize = 0;
            string hash = "";
            string sRet = "";

            string sEncValue = sec.Encrypt(value);
            hash = sec.toBase64(sec.hash);
            hashsize = hash.Length;
            sRet = hash + sEncValue + hashsize.ToString("000");
            return sRet;
        }


        private static bool ProcessLicenses(DateTime dStateDate)
        {
            if (fullfilename.Length == 0)
            {
                if (!srcPath.EndsWith("\\")) srcPath += "\\";
                fullfilename = srcPath + fileName;
            }
            StreamReader file = null;
            bool bRet = false;
            List<String> sLicenseList = new List<string>();
            try
            {
                LogEvent("BusinessObjects AWS Heartbeat started - " + dStateDate.ToString("MM/dd/yyyy hh:mm"), EventLogStatus.INFO);
                file = new StreamReader(fullfilename);
                DateTime startDt = DateTime.Now;
                string sKey = startDt.ToString("MMddyyyy");
                //BOESecurity sec = new BOESecurity(sKey + "1200");

                string line;
                //string line2;
                //string hash;
                //int hashsize = 0;

                while ((line = file.ReadLine()) != null)
                {
                    sLicenseList.Add(DecryptValue(sKey + "1200", line));

                    /*
                    if (Int32.TryParse(line.Substring(line.Length - 3, 3), out hashsize))
                    {
                        line = line.Substring(0, line.Length - 3);
                        hash = line.Substring(0, hashsize);
                        line2 = line.Substring(hashsize);
                        sec.hash = sec.fromBase64(hash);
                        line2 = sec.Decrypt(line2);
                        sLicenseList.Add(line2);
                    } */
                }
                bRet = true;

                try
                {
                    foreach (string code in Amazon.Util.EC2InstanceMetadata.ProductCodes)
                    {
                        sProductCode = code;
                        LogEvent("BusinessObjects AWS Heartbeat Product Code: " + sProductCode, EventLogStatus.INFO);
                    }
                }
                catch // Not running inside EC2 Instance, use default product code
                {
                    LogEvent("BusinessObjects AWS Heartbeat Not running in EC2 Instance", EventLogStatus.INFO);
                }
            }
            catch (IOException ex) // File Processing Error
            {
                LogEvent("BusinessObjects AWS Heartbeat threw an error: " + ex.Message, EventLogStatus.ERROR);
                bRet = false;
            }
            catch (Exception e)
            {
                LogEvent("BusinessObjects AWS Heartbeat threw an error: " + e.Message, EventLogStatus.ERROR);
                bRet = false;
            }
            finally
            {
                if (file != null) file.Close();
            }

            if (bRet)
            {
                //Process License Entries
                try
                {
                    List<LicenseInfo> sLicenseInfo = new List<LicenseInfo>();
                    foreach (String sl in sLicenseList)
                        sLicenseInfo.Add(new LicenseInfo(sl.Split(',')));

                    bool bValidFile = true;

                    //Check for age of Current Date recorded in License File.
                    //If Current Date is older than 1.5x the Period length, throw a warning.
                    //Ex: Period 60 Minutes, CD = 01/01/2016 - 10AM, Current RunTime = 01/01/2016 - 11AM = Success.
                    //Ex: Period 60 Minutes, CD = 01/01/2016 - 10AM, Current RunTime = 01/01/2016 - 12PM = Failure.
                    //      (Failure, file CD must be at least from 01/01/2016 - 10:30AM to pass).
                    foreach (LicenseInfo l in sLicenseInfo)
                    {
                        TimeSpan licensespan = dStateDate - l.CurrentDate;
                        if ((licensespan.TotalSeconds - iPeriodSeconds) > (iPeriodSeconds * 1.5))
                        {
                            bValidFile = false;
                            break;
                        }
                    }

                    if (bValidFile)
                    {
                        LogEvent("BusinessObjects AWS Heartbeat Processing Licenses", EventLogStatus.INFO);
                        foreach (string sDimension in sUsageDimensions)
                        {
                            //Return Total LicenseCounts
                            int iLicenses = 0;
                            foreach (LicenseInfo l in sLicenseInfo)
                            {
                                if (sDimension.Trim().ToLower() == "provisioneduser")
                                {
                                    iLicenses = iLicenses + l.LicensedConcurrentUsers + l.LicensedNamedUsers;
                                }
                                else
                                {
                                    iLicenses = iLicenses + l.CurrentConcurrentUsers + l.CurrentNamedUsers;
                                }
                            }

                            UpdateHeartbeat(bDryRun, sProductCode, DateTime.Now, sDimension, iLicenses);
                            //WriteTestFile(srcPath + "LicenseOut.txt", sLicenseInfo);
                        }
                    }
                    else
                    {
                        LogEvent("BusinessObjects AWS Heartbeat license file is too old to process. Please regenerate file.", EventLogStatus.WARN);
                        bRet = false;
                    }
                }
                catch (Exception e)
                {
                    bRet = false;
                }
            }
            TimeSpan ts = (DateTime.Now - dStateDate);
            LogEvent("BusinessObjects AWS Heartbeat completed in - " + ts.Hours.ToString() + ":" +
                ts.Minutes.ToString() + ":" + ts.Seconds.ToString() + "." + ts.Milliseconds.ToString(), EventLogStatus.INFO);
            return bRet;
        }

        private static bool UpdateHeartbeat(bool DryRun, String ProductCode, DateTime TimeStamp, String Dimension, int Quantity)
        {
            AmazonAWSMarketplaceMeteringClient client;
            bool bRet = true;
            try
            {
                //Amazon.Util.IAMSecurityCredentialMetadata mcred = new Amazon.Util.IAMSecurityCredentialMetadata();
                //client = new AmazonAWSMarketplaceMeteringClient(credentials, Amazon.RegionEndpoint.USEast1);     

                InstanceProfileAWSCredentials credentials = new InstanceProfileAWSCredentials(sRole);
                InstanceProfileAWSRegion curRegion = new InstanceProfileAWSRegion();
                
                client = new AmazonAWSMarketplaceMeteringClient(credentials, curRegion.Region);
                LogEvent("BusinessObjects AWS Heartbeat Role Logon: " + sRole, EventLogStatus.INFO);

                MeterUsageRequest request = new MeterUsageRequest();
                request.DryRun = DryRun;
                request.ProductCode = ProductCode;
                request.Timestamp = TimeStamp.ToUniversalTime();
                request.UsageDimension = Dimension;
                request.UsageQuantity = Quantity;
                MeterUsageResponse response = client.MeterUsage(request);

                if (response.HttpStatusCode.ToString().Equals("OK"))
                {
                    LogEvent("BusinessObjects AWS Heartbeat " + response.MeteringRecordId + " recorded successfully", EventLogStatus.INFO);
                }
            }
            catch (AmazonAWSMarketplaceMeteringException e)
            {
                LogEvent("BusinessObjects AWS Heartbeat threw a metering error: " + e.Message, EventLogStatus.ERROR);
                bRet = false;
            }
            catch (Exception ex)
            {
                LogEvent("BusinessObjects AWS Heartbeat threw an error: " + ex.Message, EventLogStatus.ERROR);
                bRet = false;
            }
            return bRet;
        }

        private static void WriteTestFile(String FileName, List<LicenseInfo> Licenses)
        {
            try
            {
                StreamWriter fout = null;
                fout = new StreamWriter(FileName);
                foreach (LicenseInfo l in Licenses)
                {
                    fout.WriteLine(l.CurrentDate.ToString("mm/dd/yyyy hh:mm") + " - " + l.LicenseKey + " - Named:" + l.LicensedNamedUsers + " - Concurrent:" + l.LicensedConcurrentUsers);
                }

                int iLicenses = 0;
                foreach (LicenseInfo l in Licenses)
                {
                    iLicenses = iLicenses + l.LicensedConcurrentUsers + l.LicensedNamedUsers;
                }
                fout.WriteLine("Total License Count: " + iLicenses.ToString());
                fout.Close();
            }
            catch (Exception e) { }
        }

        private void Main(string[] args)
        {
            if (args.Count() > 0) fullfilename = args[0];
            bool bret = ProcessLicenses(DateTime.Now);
        }
    }

    internal class LicenseInfo
    {
        private DateTime dCurrentDate = DateTime.Now;
        private DateTime dLicenseExpires = DateTime.Now;
        private int iCurrentConcurrentUsers = 0;
        private int iCurrentNamedUsers = 0;
        private int iLicensedConcurrentUsers = 0;
        private int iLicensedNamedUsers = 0;
        private String sLicenseKey = "";

        public LicenseInfo(string[] licensestring)
        {
            foreach (string s in licensestring)
            {
                switch (s.Substring(0, 3).ToLower())
                {
                    case "cd:":
                        dCurrentDate = DateTime.Parse(s.Substring(3).Replace('-', ' '));
                        break;

                    case "ed:":
                        dLicenseExpires = DateTime.Parse(s.Substring(3));
                        break;

                    case "lk:":
                        sLicenseKey = s.Substring(3);
                        break;

                    case "ln:":
                        iLicensedNamedUsers = Int32.Parse(s.Substring(3));
                        break;

                    case "lc:":
                        iLicensedConcurrentUsers = Int32.Parse(s.Substring(3));
                        break;

                    case "cn:":
                        iCurrentNamedUsers = Int32.Parse(s.Substring(3));
                        break;

                    case "cc:":
                        iCurrentConcurrentUsers = Int32.Parse(s.Substring(3));
                        break;
                }
            }
        }

        public int CurrentConcurrentUsers
        {
            get { return iCurrentConcurrentUsers; }
            set { iCurrentConcurrentUsers = value; }
        }

        public DateTime CurrentDate
        {
            get { return dCurrentDate; }
            set { dCurrentDate = value; }
        }

        public int CurrentNamedUsers
        {
            get { return iCurrentNamedUsers; }
            set { iCurrentNamedUsers = value; }
        }

        public int LicensedConcurrentUsers
        {
            get { return iLicensedConcurrentUsers; }
            set { iLicensedConcurrentUsers = value; }
        }

        public int LicensedNamedUsers
        {
            get { return iLicensedNamedUsers; }
            set { iLicensedNamedUsers = value; }
        }

        public DateTime LicenseExpires
        {
            get { return dLicenseExpires; }
            set { dLicenseExpires = value; }
        }

        public String LicenseKey
        {
            get { return sLicenseKey; }
            set { sLicenseKey = value; }
        }
    }
}