﻿using CrystalDecisions.Enterprise;
using CrystalDecisions.Enterprise.Desktop;
using System;

namespace DFT.BOE
{
    public class BOEUserGroupMaintenance : IDisposable
    {
        private BOECommon _common;
        private bool canDispose = true;

        #region Class_Methods

        /// <summary>
        /// Constructor - create the BOECommon object that holds the connection
        /// to the CMS.
        /// <param name="userName">The User ID for logging in to the CMS</param>
        /// <param name="password">The Password for loggin in to the CMS</param>
        /// <param name="cms">The CMS to log in to</param>
        /// </summary>
        public BOEUserGroupMaintenance(string userName, string password, string cms)
        {
            clearErr();
            try
            {
                _common = new BOECommon(userName, password, cms);
                ErrMsg = BOECommon.ErrMsg;
                StackTrace = BOECommon.StackTrace;
            }
            catch (Exception ex)
            {
                ErrMsg = ex.Message;
                StackTrace = ex.StackTrace;
            }
        }

        public BOEUserGroupMaintenance(BOECommon common)
        {
            clearErr();
            _common = common;
            canDispose = false;  //we don't want to dispose the BOECommon because it was passed in to the object.
        }

        ~BOEUserGroupMaintenance()
        {
            Dispose(true);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool Managed)
        {
            if (canDispose && (_common != null))
            {
                try
                {
                    _common.Dispose();
                    _common = null;
                    System.GC.SuppressFinalize(this);
                }
                catch
                { }  //eat it because we don't want any _errMsgs here
            }
        }

        public string ErrMsg { get; private set; }
        public string StackTrace { get; private set; }

        #endregion Class_Methods

        #region Public_Methods

        /// <summary>
        /// Make the child group a member of the parent group.
        /// </summary>
        /// <param name="parentGroupName">The name of the parent group.</param>
        /// <param name="childGroupName">The name of the child group.</param>
        /// <returns>True if successful</returns>
        public bool AddGroupToGroup(string parentGroupName, string childGroupName)
        {
            clearErr();
            string msg1 = string.Empty;
            string msg2 = string.Empty;
            bool result = false;

            //get the parent id
            int parentId = _common.GetIDFromName(parentGroupName, "UserGroup", BOECommon.CETables.CI_SYSTEMOBJECTS);
            ErrMsg = BOECommon.ErrMsg;
            StackTrace = BOECommon.StackTrace;
            if (StackTrace != string.Empty)
            {
                return false;
            }
            else if (parentId < 0)
                msg1 = string.Format("Unable to find parent group {0}. Err={1}\n", parentGroupName, BOECommon.ErrMsg);

            //get the child id
            int childId = _common.GetIDFromName(childGroupName, "UserGroup", BOECommon.CETables.CI_SYSTEMOBJECTS);
            ErrMsg = BOECommon.ErrMsg;
            StackTrace = BOECommon.StackTrace;
            if (BOECommon.StackTrace != string.Empty)
            {
                return false;
            }
            else if (childId < 0)
                msg2 = string.Format("Unable to find child group {0}. Err={1}", childGroupName, BOECommon.ErrMsg);

            if ((parentId > 0) && (childId > 0))
            {
                //both groups were found so we'll process them
                result = AddGroupToGroup(parentId, childId);
            }
            else
            {
                ErrMsg = String.Format("{0}{1}{2}", msg1, (msg2 == string.Empty ? string.Empty : "\n"), msg2);
            }
            return result;
        }

        /// <summary>
        /// Make the child group a member of the parent group.
        /// </summary>
        /// <param name="parentId">The SI_ID of the parent group.</param>
        /// <param name="childId">The SI_ID of the child group.</param>
        /// <returns>True of successful</returns>
        public bool AddGroupToGroup(int parentId, int childId)
        {
            ErrMsg = string.Empty;
            bool result = false;
            try
            {
                string query = string.Format("Select * from CI_SYSTEMOBJECTS where SI_ID in ({0}, {1})", parentId, childId);
                using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
                {
                    ErrMsg = BOECommon.ErrMsg;
                    StackTrace = BOECommon.StackTrace;
                    if ((iobjs == null) || (StackTrace != string.Empty))
                    {
                        return false;  //we've had an error
                    }
                    else if (iobjs.Count == 0) //no groups found
                    {
                        ErrMsg = string.Format("Parent Group ID {0} and Child Group ID {1} not found", parentId, childId);
                    }
                    else if (iobjs.Count == 1)  //only one group was found
                    {
                        if (parentId == iobjs[1].ID)
                            ErrMsg = string.Format("Child Group with ID {0} not found", childId);
                        else
                            ErrMsg = string.Format("Parent Group with ID {0} not found", parentId);
                    }
                    else //both groups were found
                    {
                        UserGroup parentGroup;
                        if (iobjs[1].ID == parentId)
                            parentGroup = (UserGroup)iobjs[1];
                        else
                        {
                            parentGroup = (UserGroup)iobjs[2];
                        }
                        parentGroup.SubGroups.Add(childId);
                        _common.CommitObjects(iobjs);
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = ex.Message;
                StackTrace = ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// Add a user to a group.
        /// </summary>
        /// <param name="groupName">The name of the group.</param>
        /// <param name="userName">The user ID of the user.</param>
        /// <returns>True if successful.</returns>
        public bool AddUserToGroup(string groupName, string userName)
        {
            clearErr();
            string msg1 = string.Empty;
            string msg2 = string.Empty;
            //get the group id
            int groupId = _common.GetIDFromName(groupName, "UserGroup", BOECommon.CETables.CI_SYSTEMOBJECTS);
            ErrMsg = BOECommon.ErrMsg;
            StackTrace = BOECommon.StackTrace;
            if (StackTrace != string.Empty)
            {
                return false;
            }
            else if (groupId < 0)
                msg1 = string.Format("Unable to find user group {0}. Err={1}\n", groupName, BOECommon.ErrMsg);

            //get the user id
            int userId = _common.GetIDFromName(userName, "User", BOECommon.CETables.CI_SYSTEMOBJECTS);
            ErrMsg = BOECommon.ErrMsg;
            StackTrace = BOECommon.StackTrace;
            if (BOECommon.StackTrace != string.Empty)
            {
                return false;
            }
            else if (userId < 0)
            {
                msg2 = string.Format("Unable to find user {0}. Err={1}", userName, BOECommon.ErrMsg);
            }
            if ((groupId > 0) && (userId > 0))
            {
                return AddGroupToGroup(groupId, userId);
            }
            else
            {
                ErrMsg = String.Format("{0}{1}{2}", msg1, (msg2 == string.Empty ? string.Empty : "\n"), msg2);
                return false;
            }
        }

        /// <summary>
        /// Add a user to a group.
        /// </summary>
        /// <param name="groupId">The SI_ID of the group.</param>
        /// <param name="userId">The SI_ID of the user.</param>
        /// <returns>True if successful.</returns>
        public bool AddUserToGroup(int groupId, int userId)
        {
            clearErr();
            string userName = string.Empty;
            if (_common.ObjectExistsByID(groupId, BOECommon.CETables.CI_SYSTEMOBJECTS))
            {
                try
                {
                    using (InfoObjects iobjs = _common.ExecuteRawQuery(string.Format("Select * from CI_SYSTEMOBJECTS where SI_ID = {0}", userId)))
                    {
                        if ((iobjs != null) && (iobjs.Count > 0))
                        {
                            User usr = (User)iobjs[1];
                            userName = usr.Title;
                            usr.Groups.Add(groupId);
                            _common.CommitObjects(iobjs);
                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrMsg = String.Format("Unable to add user {0} to group {1}.  Err={2}", (userName == string.Empty ? userId.ToString() : userName), groupId, ex.Message);
                }
            }
            else
            {
                StackTrace = BOECommon.StackTrace;
                if (BOECommon.ErrMsg == string.Empty)
                {
                    ErrMsg = String.Format("Unable to add user {0} to group{1}.  User Group {1} does not exist", userId, groupId);
                }
                else
                {
                    ErrMsg = String.Format("Unable to add user {0} to group{1}.  Err={2}", userId, groupId, BOECommon.ErrMsg);
                }
            }
            return false;
        }

        /// <summary>
        /// Create a user group.
        /// </summary>
        /// <param name="groupName">The name of the group to create.</param>
        /// <returns>The SI_ID of the new group if successful, 0 if the group already exists, -1 if there was an error.</returns>
        public int CreateGroup(string groupName)
        {
            clearErr();
            int result = 0;
            try
            {
                //does the user group already exist?
                if (!_common.ObjectExistsByName(groupName, "SI_KIND = 'UserGroup", BOECommon.CETables.CI_SYSTEMOBJECTS))
                {
                    //Add the group
                    using (PluginInfo pi = _common.GetPluginInfo("CrystalEnterprise.UserGroup"))
                    {
                        using (InfoObjects grpObjs = _common.NewInfoObjects())
                        {
                            grpObjs.Add(pi);
                            UserGroup newGrp = (UserGroup)grpObjs[1];
                            newGrp.Title = groupName;
                            _common.CommitObjects(grpObjs);
                            ErrMsg = BOECommon.ErrMsg;
                            StackTrace = BOECommon.StackTrace;
                            if (ErrMsg != string.Empty) //there was an error committing the new group
                            {
                                result = -1;
                            }
                            else
                            {
                                //Get the ID of the new group
                                result = _common.GetIDFromName(groupName, "UserGroup", BOECommon.CETables.CI_SYSTEMOBJECTS);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = string.Format("Unable to add group named {0}. Err={1}", groupName, ex.Message);
                StackTrace = ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// Delete a user group.
        /// </summary>
        /// <param name="groupName">The name of the group to delete.</param>
        /// <param name="deleteUsers">If true, also delete all of the users who are members of the group.</param>
        /// <returns>True if successful.</returns>
        public bool DeleteGroup(string groupName, bool deleteUsers)
        {
            clearErr();
            bool result = true;
            String query = string.Format("Select SI_ID, SI_NAME from CI_SYSTEMOBJECTS where SI_KIND = 'UserGroup' and SI_NAME = '{0}'", groupName);
            using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
            {
                if (BOECommon.StackTrace != string.Empty)
                {
                    ErrMsg = BOECommon.ErrMsg;
                    StackTrace = BOECommon.StackTrace;
                    result = false;
                }
                else
                {
                    try
                    {
                        if ((iobjs != null) && (iobjs.Count > 0))
                        {
                            InfoObject grp = iobjs[1];
                            if (deleteUsers)
                            {
                                result = DeleteUsersInGroup(grp.ID);
                            }
                            if (result)
                            {
                                iobjs.Delete(grp);
                                _common.CommitObjects(iobjs);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrMsg = string.Format("Unable to delete group {0}. Err={1}", groupName, ex.Message);
                        StackTrace = ex.StackTrace;
                        result = false;
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Delete all of the users in a group.
        /// </summary>
        /// <param name="groupName">The name of the group whose users will be deleted.</param>
        /// <returns></returns>
        public bool DeleteUsersInGroup(string groupName)
        {
            clearErr();
            int groupId = _common.GetIDFromName(groupName, "UserGroup", BOECommon.CETables.CI_SYSTEMOBJECTS);
            ErrMsg = BOECommon.ErrMsg;
            StackTrace = BOECommon.StackTrace;
            if ((StackTrace != string.Empty) || (ErrMsg != string.Empty)) //there was an error
            {
                return false;
            }
            if (groupId > 0)
            {
                return DeleteUsersInGroup(groupId);
            }
            else
            {
                ErrMsg = "Unable to find user group named " + groupName;
                return false;
            }
        }

        /// <summary>
        /// Delete all of the users in a group.
        /// </summary>
        /// <param name="groupId">The SI_ID of the group whose users will be deleted.</param>
        /// <returns></returns>
        public bool DeleteUsersInGroup(int groupId)
        {
            clearErr();
            string query = string.Format("Select SI_ID, SI_NAME, SI_GROUP_MEMBERS from CI_SYSTEMOBJECTS where SI_ID = {0}", groupId);
            string groupName = string.Empty;
            bool result = false;
            try
            {
                using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
                {
                    if (BOECommon.StackTrace != string.Empty)  // there was an error running the query
                    {
                        ErrMsg = BOECommon.ErrMsg;
                        StackTrace = BOECommon.StackTrace;
                    }
                    else if ((iobjs != null) && (iobjs.Count > 0))  //the group was found
                    {
                        UserGroup grp = (UserGroup)iobjs[1];
                        Properties members = grp.Properties["SI_GROUP_MEMBERS"].Properties;
                        int count = (int)members["SI_TOTAL"].Value;
                        using (BOEUserMaintenance _userMaint = new BOEUserMaintenance(_common))
                        {
                            int id;
                            //Get the IDs of all of the group members
                            for (int i = 1; i <= count; i++)
                            {
                                if (result)
                                {
                                    id = (int)members[i.ToString()].Value;
                                    result = _userMaint.DeleteUser(id);
                                }
                            }
                            if (!result)
                            {
                                ErrMsg = _userMaint.ErrMsg;
                                StackTrace = _userMaint.StackTrace;
                            }
                        } //using _userMaint
                    }  //(iobjs != null) && (iobjs.Count > 0)
                    else
                    {
                        ErrMsg = string.Format("Unable to find User Group with SI_ID={0}", groupId);
                    }
                } //using iobjs
            } //try
            catch (Exception ex)
            {
                ErrMsg = ex.Message;
                StackTrace = ex.StackTrace;
            }
            return true;
        }

        /// <summary>
        /// Remove a user from a group's membership
        /// </summary>
        /// <param name="groupName">The name of the group the user will be deleted from.</param>
        /// <param name="userName">The user ID of the user who will be removed.</param>
        /// <returns>True if successful.</returns>
        public bool RemoveUserFromGroup(string groupName, string userName)
        {
            clearErr();
            string msg1 = string.Empty;
            string msg2 = string.Empty;
            //get the group id
            int groupId = _common.GetIDFromName(groupName, "UserGroup", BOECommon.CETables.CI_SYSTEMOBJECTS);
            ErrMsg = BOECommon.ErrMsg;
            StackTrace = BOECommon.StackTrace;
            if (StackTrace != string.Empty)
            {
                return false;
            }
            else if (groupId < 0)
                msg1 = string.Format("Unable to find user group {0}. Err={1}\n", groupName, BOECommon.ErrMsg);

            //get the user id
            int userId = _common.GetIDFromName(userName, "User", BOECommon.CETables.CI_SYSTEMOBJECTS);
            ErrMsg = BOECommon.ErrMsg;
            StackTrace = BOECommon.StackTrace;
            if (BOECommon.StackTrace != string.Empty)
            {
                return false;
            }
            else if (userId < 0)
            {
                msg2 = string.Format("Unable to find user {0}. Err={1}", userName, BOECommon.ErrMsg);
            }
            if ((groupId > 0) && (userId > 0))
            {
                return RemoveUserFromGroup(groupId, userId);
            }
            else
            {
                ErrMsg = String.Format("{0}{1}{2}", msg1, (msg2 == string.Empty ? string.Empty : "\n"), msg2);
                return false;
            }
        }

        /// <summary>
        /// Remove the specified user from the group's membership.
        /// </summary>
        /// <param name="groupId">The SI_ID of the group.</param>
        /// <param name="userId">The SI_ID of the user to be removed.</param>
        /// <returns>True if successful.</returns>
        public bool RemoveUserFromGroup(int groupId, int userId)
        {
            clearErr();
            string query = string.Format("Select SI_ID, SI_NAME, SI_USER_GROUPS from CI_SYSTEMOBJECTS where SI_ID = {0}", userId);
            using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
            {
                if (BOECommon.StackTrace != string.Empty) //there was an error running the query
                {
                    ErrMsg = BOECommon.ErrMsg;
                    StackTrace = BOECommon.StackTrace;
                    return false;
                }
                if ((iobjs != null) && (iobjs.Count > 0))
                {
                    User usr = (User)iobjs[1];
                    int count = usr.Groups.Count;
                    int i = 0;
                    bool found = false;
                    while (!found && i <= count)
                    {
                        i++;
                        found = (usr.Groups[i] == groupId);
                    }
                    if (found)
                    {
                        usr.Groups.Delete(i);
                        _common.CommitObjects(iobjs);
                        ErrMsg = BOECommon.ErrMsg;
                        StackTrace = BOECommon.StackTrace;
                        if (StackTrace != string.Empty)
                        {
                            return false;
                        }
                    }
                    return true;
                }
                else
                {
                    ErrMsg = string.Format("User with ID {0} not found", userId);
                    return false;
                }
            }
        }

        public bool AddGroupToFolder(int groupId, int folderId, int accessLevelId)
        {
            string query = string.Format("Select SI_ID, SI_NAME from CI_INFOOBJECTS where SI_ID = {0}", folderId);
            using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
            {
                if (BOECommon.StackTrace != string.Empty)  //there was an error in processing the query
                {
                    ErrMsg = BOECommon.ErrMsg;
                    StackTrace = BOECommon.StackTrace;
                    return false;
                }
                else if ((iobjs != null) && (iobjs.Count > 0)) //the folder was found
                {
                    //Note that we're using SecurityInfo2 - the old way using SecurityInfo is being deprecated and
                    //  doesn't give us access to work with custom access levels.
                    ExplicitPrincipals eps = iobjs[1].SecurityInfo2.ExplicitPrincipals;
                    if (!objectPrincipalIsAssigned(groupId, eps)) //Group is not already assigned to folder
                    {
                        ExplicitPrincipal ep = eps.Add(groupId);
                        ep.Roles.Add(accessLevelId);
                        if (!_common.CommitObjects(iobjs)) //there was an error committing the change
                        {
                            ErrMsg = BOECommon.ErrMsg;
                            StackTrace = BOECommon.StackTrace;
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                }
                else //the folder was not found
                {
                    ErrMsg = string.Format("Unable to locate folder with ID={0}", folderId);
                }
            }
            return false;
        }

        #endregion Public_Methods

        #region Private_Methods

        private void clearErr()
        {
            ErrMsg = string.Empty;
            StackTrace = string.Empty;
        }

        private bool objectPrincipalIsAssigned(int objId, ExplicitPrincipals eps)
        {
            foreach (ExplicitPrincipal ep in eps)
            {
                if (ep.ID == objId)
                {
                    return true;
                }
            }
            return false;
        }

        #endregion Private_Methods
    }
}