﻿using CrystalDecisions.Enterprise;
using System;
using System.Diagnostics;

namespace DFT.BOE
{
    public class BOECommon : IDisposable
    {
        internal const string C_GET_USER = "Select Top 1* From CI_SYSTEMOBJECTS Where SI_KIND='User' and SI_NAME='{0}'";

        protected string _userName = string.Empty;
        protected string _password = string.Empty;
        protected string _CMS = string.Empty;
        protected string _Authentication = "secEnterprise";
        protected string _token = string.Empty;
        protected int _userID = -1;
        protected int _favId = -1;
        protected EnterpriseSession _BOESession = null;
        protected EnterpriseService _BOEService = null;
        protected bool _initialized = false;
        protected InfoStore _BOEInfoStore = null;
        protected InfoObjects bOEInfoObjects = null;
        protected InfoObject _BOEInfoObject = null;
        protected InfoObjects _BOEInfoObjects;

        public enum CETables
        {
            CI_INFOOBJECTS,
            CI_SYSTEMOBJECTS,
            CI_APPOBJECTS
        }

        #region Class_Methods

        /// <summary>
        /// Default constructor.  Included in case we need to serialize the object.
        /// DO NOT USE!
        /// </summary>
        public BOECommon()
        {
            //default constructor - this will generally NOT be used!
            clearErr();
            LoggedOn = false;
        }

        /// <summary>
        /// Create a BOECommon that logs in to the CMS using the supplied parameters
        /// </summary>
        /// <param name="userName"> UserName to log in to CMS</param>
        /// <param name="password"> Password to log in to CMS</param>
        /// <param name="cms"> CMS machine or cluster name</param>
        public BOECommon(string userName, string password, string cms)
        {
            clearErr();
            try
            {
                _userName = userName;
                _password = password;
                _CMS = cms;
                LoggedOn = false;
                initialize();
            }
            catch (Exception ex)
            {
                ErrMsg = "BOECommon.Constructor: Unable to initiate BOECommon.  Err=" + ex.Message;
                StackTrace = ex.StackTrace;
            }
        }

        /// <summary>
        /// Create a BOECommon that uses trusted authentication to log in to the CMS.
        /// </summary>
        public BOECommon(string userName, string cms)
        {
            clearErr();
            _userName = userName;
            _CMS = cms;
            LoggedOn = false;
            initialize();
        }

        public BOECommon(string token)
        {
            clearErr();
            _token = token;
            LoggedOn = false;
            initialize();
        }

        public BOECommon(ISEnterpriseSession Session)
        {
            if (Session != null)
            {
                _BOESession = (EnterpriseSession)Session;
                _BOEService = _BOESession.GetService("InfoStore");
                _BOEInfoStore = (InfoStore)_BOEService;

                _initialized = true;
                LoggedOn = true;
            }
        }

        public BOECommon(ISEnterpriseSession Session, ISInfoStore Store)
        {
            if (Session != null)
            {
                _BOESession = (EnterpriseSession)Session;
                _BOEService = _BOESession.GetService("InfoStore");
                if (Store != null) _BOEInfoStore = (InfoStore)Store;
                else _BOEInfoStore = (InfoStore)_BOEService;

                _initialized = true;
                LoggedOn = true;
            }
        }

        ~BOECommon()
        {
            Dispose(true);
        }

        /// <summary>
        /// Explicitly Close the connection to the CMS.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool Managed)
        {
            if (_BOEInfoStore != null)
                _BOEInfoStore.Dispose();
            if (_BOESession != null)
            {
                try
                {
                    _BOESession.Logoff();
                    _BOESession.Dispose();
                }
                catch
                {
                    //Eat the error.  This is needed to prevent errors when debugging.  In a
                    //"normal" run, these errors are ignored.
                }
            }
            System.GC.SuppressFinalize(this);
        }

        public static string ErrMsg { get; private set; }
        public static string StackTrace { get; private set; }
        public int TokenExpirationMinutes { get; set; }
        public int TokenLogons { get; set; }
        public InfoStore BOEInfoStore { get { return _BOEInfoStore; } }
        public InfoObject BOEInfoObject { get { return _BOEInfoObject; } set { _BOEInfoObject = value; } }
        public InfoObjects BOEInfoObjects { get { return _BOEInfoObjects; } set { _BOEInfoObjects = value; } }
        public string CurrentUser { get { return _userName; } }
        public bool LoggedOn { get; private set; }

        public string GetLastErrMsg
        {
            get
            {
                return ErrMsg;
            }
        }

        public int CurrentUserID
        {
            get
            {
                clearErr();
                if (_userID == -1)
                {
                    _userID = GetIDFromName(_userName, "User", CETables.CI_SYSTEMOBJECTS);
                }
                return _userID;
            }
        }

        public int UserFavoritesID
        {
            get
            {
                clearErr();
                if (_favId == -1)
                {
                    _favId = this.GetIDFromName(_userName, "FavoritesFolder", CETables.CI_INFOOBJECTS);
                    if (ErrMsg != string.Empty)
                    {
                        _favId = -1;
                    }
                }
                return _favId;
            }
        }

        public string LogonToken
        {
            get
            {
                if (_token == string.Empty)
                {
                    _token = _BOESession.LogonTokenMgr.CreateLogonTokenEx("", TokenExpirationMinutes, TokenLogons);
                }
                return _token;
            }
        }

        #endregion Class_Methods

        #region LogOn/LogOff

        public bool LogOn(string userName, string cms)
        {
            _userName = userName;
            _CMS = cms;
            _password = string.Empty;
            _token = string.Empty;
            initialize();
            return LoggedOn;
        }

        public bool LogOn(string userName, string pw, string cms)
        {
            _userName = userName;
            _CMS = cms;
            _password = pw;
            _token = string.Empty;
            initialize();
            return LoggedOn;
        }

        public bool LogOn(string userName, string pw, string cms, string auth)
        {
            _userName = userName;
            _CMS = cms;
            _password = pw;
            _Authentication = auth;
            _token = string.Empty;
            initialize();
            return LoggedOn;
        }

        public bool LogOn(string token)
        {
            _userName = string.Empty;
            _CMS = string.Empty;
            _password = string.Empty;
            _token = token;
            initialize();
            return LoggedOn;
        }

        public bool LogOff()
        {
            //Get rid of the InfoStore - it will be recreated when another user logs in.
            if (_BOEInfoStore != null)
                _BOEInfoStore.Dispose();
            //Log off of the session
            if (_BOESession != null)
            {
                try
                {
                    _BOESession.Logoff();
                    LoggedOn = false;
                    _userName = string.Empty;
                    _userID = -1;
                    _password = string.Empty;
                    _CMS = string.Empty;
                    _token = string.Empty;
                }
                catch
                {
                    //Eat the error.  This is needed to prevent errors when debugging.  In a
                    //"normal" run, these errors are ignored.
                }
            }
            return !LoggedOn;
        }

        #endregion LogOn/LogOff

        #region Public_Methods

        /// <summary>
        /// Commit one or more InfoObjects to the CMS database
        /// </summary>
        /// <param name="iobjs">The set of objects to commit.</param>
        /// <returns>True if successful.</returns>
        public bool CommitObjects(InfoObjects iobjs)
        {
            clearErr();
            try
            {
                _BOEInfoStore.Commit(iobjs);
                return true;
            }
            catch (Exception ex)
            {
                ErrMsg = string.Format("BOECommon.CommitObjects: Error committing objects: {0}", ex.Message);
                StackTrace = ex.StackTrace;
                return false;
            }
        }

        /// <summary>
        /// Delete the specified object
        /// </summary>
        /// <param name="name">Name of the object</param>
        /// <param name="kind">Kind of the object</param>
        /// <param name="table">CMS Table to query</param>
        /// <returns>True if successful</returns>
        public bool DeleteObject(string name, string kind, CETables table)
        {
            clearErr();
            int objId = GetIDFromName(name, kind, table);
            if (objId > 0)
            {
                return DeleteObject(objId, table);
            }
            else
            {
                ErrMsg = string.Format("BOECommon.DeleteObject: Unable to delete {0} named {1}. {2}", kind, name, ErrMsg);
            }
            return false;
        }

        /// <summary>
        /// Delete the specified object.
        /// </summary>
        /// <param name="objId">The SI_ID for the object to be deleted</param>
        /// <param name="table">The CMS table to query.</param>
        /// <returns>True if successful.</returns>
        public bool DeleteObject(int objId, CETables table)
        {
            clearErr();
            try
            {
                string query = string.Format("Select SI_ID, SI_NAME from {0} where SI_ID = {1}", table.ToString(), objId);
                using (InfoObjects iobjs = ExecuteRawQuery(query))
                {
                    if ((iobjs != null) && (iobjs.Count > 0))
                    {
                        iobjs.Delete(1);
                        CommitObjects(iobjs);
                        return true;
                    }
                    else
                    {
                        if (iobjs != null)
                        {
                            ErrMsg = string.Format("BOECommon.DeleteObject: Unable to delete object with ID {0}.  Object does not exist.", objId);
                        }
                        else
                        {
                            ErrMsg = string.Format("BOECommon.DeleteObject: Unable to delete object with ID {0}.  Err={1}", objId, ErrMsg);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = string.Format("BOECommon.DeleteObject: Unable to delete object with ID {0}. Err={1}", objId, ex.Message);
                StackTrace = ex.StackTrace;
            }
            return false;
        }

        /// <summary>
        /// Run a query against the CMS database
        /// </summary>
        /// <param name="query">The query to run.</param>
        /// <returns>The set of InfoObjects returned by the query.</returns>
        public InfoObjects ExecuteRawQuery(string query)
        {
            Debug.WriteLine("ExecuteRawQuery: " + query);
            clearErr();
            InfoObjects result = null;
            try
            {
                result = _BOEInfoStore.Query(query);
            }
            catch (Exception ex)
            {
                ErrMsg = string.Format("BOECommon.ExecuteRawQuery: Error in query: {0}. Err={1}", query, ex.Message);
                StackTrace = ex.StackTrace;
            }
            return result;
        }

        /*
      public InfoObjects ExecutePagedQuery(string query, int batchSize)
      {
          Debug.WriteLine(("Executing Paged Query : " + query);
          InfoObjects iObjs = _BOEInfoStore.NewInfoObjectCollection();

          // IInfoStore store = boe.getInfoStore();

          // PagingQueryOptions pqo = new PagingQueryOptions(qh.getBatchSize());
          PagingQueryOptions pqo = new PagingQueryOptions(batchSize);
          PageFactoryFacade pf = new PageFactoryFacade();

          if (!query.StartsWith("query"))
          {
              query = "query://{" + query + "}";
          }

          try
          {
              IPageResult result = _BOEInfoStore.
              logIt("Page count for query = " + result.getPageCount(), DEBUG);
              Iterator iter = result.iterator();
              int pageNumber = 0;
              int resultSize;
              int objCount = 0;

              while (iter.hasNext())
              {
                  logIt("Fetching Page " + pageNumber, DEBUG);

                  IStatelessPageInfo spi = _BOEInfoStore.getStatelessPageInfo((String)iter.next(), pqo);
                  IInfoObjects page = store.query(spi.getPageSQL());

                  resultSize = page.getResultSize();
                  if (resultSize > 0)
                  {
                      //Iterator infoIter = page.iterator();
                      iobjs.merge(page);
                      objCount += page.getResultSize();
                      pageNumber++;
                  }
              }
          }
          catch (SDKException e)
          {
              logIt("addPagedResults: ", e, ERROR);
              //run = false;
          }
          catch (Exception e)
          {
              // TODO Auto-generated catch block
              logIt("", e, ERROR);
              //run = false;
          }
          return iobjs;
      }
      */

        /// <summary>
        /// Get the PluginInfo for a specific type of object.  Plugins are used when adding
        ///   objects to the CMS.
        /// </summary>
        /// <param name="pluginType">The type of plugin to get.</param>
        /// <returns>A PluginInfo of the specified type.</returns>
        public PluginInfo GetPluginInfo(string pluginType)
        {
            clearErr();
            PluginInfo result = null;
            try
            {
                using (PluginManager pm = _BOEInfoStore.PluginManager)
                {
                    result = pm.GetPluginInfo(pluginType);
                }
            }
            catch (Exception ex)
            {
                ErrMsg = string.Format("BOECommon.GetPluginInfo: Unable to get PluginInfo for {0}. Err={1}", pluginType, ex.Message);
                StackTrace = ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// Get an SI_ID value for any type of object based on the object's Name and Kind
        /// </summary>
        /// <param name="name">The name of the object to look for.</param>
        /// <param name="kind">The kind of the object to look for.</param>
        /// <param name="table">The CMS table to query for the object.</param>
        /// <returns>The SI_ID value for the object, if found, otherwise -1.</returns>
        public int GetIDFromName(string name, string kind, CETables table)
        {
            clearErr();
            int result = -1;
            string query = string.Format("Select SI_ID from {0} where SI_NAME='{1}' and SI_KIND='{2}'", table.ToString(), name, kind);
            try
            {
                using (InfoObjects iobjs = ExecuteRawQuery(query))
                {
                    if (iobjs != null)
                    {
                        if (iobjs.Count == 1)
                        {
                            result = iobjs[1].ID;
                        }
                        else if (iobjs.Count > 1)
                        {
                            if (ErrMsg == string.Empty)
                                ErrMsg = string.Format("BOECommon.GetIDFromName: More than one {0} named {1} exists.", kind, name);
                        }
                        else if (iobjs.Count == 0)
                        {
                            if (ErrMsg == string.Empty)
                                ErrMsg = string.Format("BOECommon.GetIDFromName: {0} named {1} not found.", kind, name);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = string.Format("BOECommon.GetIDFromName: Unable to find {0} named {1}. Err={2}", kind, name, ex.Message);
                StackTrace = ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// Get an SI_ID value for any type of object based on the object's Name and Kind
        /// </summary>
        /// <param name="name">The name of the object to look for.</param>
        /// <param name="kind">The kind of the object to look for.</param>
        /// <param name="parentID">The SI_ID of the object's parent.</param>
        /// <param name="table">The CMS table to query for the object.</param>
        /// <returns>The SI_ID value for the object, if found, otherwise -1.</returns>
        public int GetIDFromName(string name, string kind, int parentID, CETables table)
        {
            clearErr();
            int result = -1;
            string query = string.Format("Select SI_ID from {0} where SI_NAME='{1}' and SI_KIND='{2}' and SI_PARENTID={3}", table.ToString(), name, kind, parentID);
            using (InfoObjects iobjs = ExecuteRawQuery(query))
            {
                if ((iobjs != null) && (iobjs.Count > 0))
                {
                    result = iobjs[1].ID;
                }
                else
                {
                    if (ErrMsg == string.Empty)
                        ErrMsg = string.Format("BOECommon.GetIDFromName: Unable to find {0} named {1} with Parent ID {2}.", kind, name, parentID);
                }
            }
            return result;
        }

        public int GetIDFromCUID(string cuid, CETables table)
        {
            int result = -1;
            string query = string.Format("Select SI_ID from {0} where SI_CUIDE = {1}", table.ToString(), cuid);
            using (InfoObjects iobjs = ExecuteRawQuery(query))
            {
                if ((iobjs != null) && (iobjs.Count > 0))
                {
                    result = iobjs[1].ID;
                }
                else
                {
                    if (ErrMsg == string.Empty)
                        ErrMsg = string.Format("BOECommon.GetIDFromCUID: Unable to find object with CUID={0}.", cuid);
                }
            }
            return result;
        }

        /// <summary>
        /// Get the Name of an object based on it's SI_ID value.
        /// </summary>
        /// <param name="id">The ID of the object to look for.</param>
        /// <param name="table">The CMS table to query for the object.</param>
        /// <returns>The name of the specified object if found, otherwise a blank string.</returns>
        public string GetNameFromID(int id, CETables table)
        {
            clearErr();
            string result = string.Empty;
            string query = string.Format("Select SI_NAME from {0} where SI_ID='{1}'", table.ToString(), id);
            using (InfoObjects iobjs = ExecuteRawQuery(query))
            {
                if ((iobjs != null) && (iobjs.Count > 0))
                {
                    result = iobjs[1].Title;
                }
                else
                {
                    if (ErrMsg == string.Empty)
                        ErrMsg = string.Format("BOECommon.GetNameFromID: Unable to find object with ID {0}", id);
                }
            }
            return result;
        }

        /// <summary>
        /// Get an InfoObject based on its SI_ID.
        /// </summary>
        /// <param name="id">The SI_ID of the object to look for.</param>
        /// <param name="fields">The specific fields to return.  If this is a blank string, it will default to "*"</param>
        /// <param name="table">The table to query for the object.</param>
        /// <returns>The specified InfoObject, if found, otherwise null.</returns>
        public InfoObject GetObjectById(int id, string fields, CETables table)
        {
            clearErr();
            InfoObject result = null;
            string flds = (fields == string.Empty ? "*" : fields);
            string query = string.Format("Select {0} from {1} where SI_ID = {2}", flds, table.ToString(), id);
            using (InfoObjects iobjs = ExecuteRawQuery(query))
            {
                if ((ErrMsg == string.Empty) && (StackTrace == string.Empty) && (iobjs.Count > 0))
                {
                    result = iobjs[1];
                }
                else
                {
                    if (ErrMsg == string.Empty)
                        ErrMsg = string.Format("BOECommon.GetObjectById: Unable to find object with ID {0}.", id);
                }
            }
            return result;
        }

        /// <summary>
        /// Get an InfoObject based on the object's Name and Kind
        /// </summary>
        /// <param name="name">The name of the object to look for.</param>
        /// <param name="kind">The kind of the object to look for.</param>
        /// <param name="table">The CMS table to query for the object.</param>
        /// <returns>The specified InfoObject, if found, otherwise null.</returns>
        public InfoObject GetObjectFromName(string name, string kind, CETables table)
        {
            clearErr();
            string query = string.Format("Select * from {0} where SI_NAME='{1}' and SI_KIND='{2}'", table.ToString(), name, kind);
            try
            {
                using (InfoObjects iobjs = ExecuteRawQuery(query))
                {
                    if ((iobjs != null) && (iobjs.Count > 0))
                    {
                        return iobjs[1];
                    }
                    else
                    {
                        if (ErrMsg == string.Empty)
                            ErrMsg = string.Format("BOECommon.GetObjectFromName: {0} named {1} not found", kind, name);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "Unable to get object.  Err=" + ex.Message;
                StackTrace = ex.StackTrace;
            }
            return null;
        }

        /// <summary>
        /// Get an InfoObject based on the object's Name and Kind
        /// </summary>
        /// <param name="name">The name of the object to look for.</param>
        /// <param name="kind">The kind of the object to look for.</param>
        /// <param name="parentID">The SI_ID of the object's parent.</param>
        /// <param name="table">The CMS table to query for the object.</param>
        /// <returns>The specified InfoObject, if found, otherwise null.</returns>
        public InfoObject GetObjectFromName(string name, string kind, int parentID, CETables table)
        {
            clearErr();
            string query = string.Format("Select * from {0} where SI_NAME='{1}' and SI_KIND='{2}' and SI_PARENTID={3}", table.ToString(), name, kind, parentID);
            try
            {
                using (InfoObjects iobjs = ExecuteRawQuery(query))
                {
                    if ((iobjs != null) && (iobjs.Count > 0))
                    {
                        return iobjs[1];
                    }
                    else
                    {
                        if (ErrMsg == string.Empty)
                            ErrMsg = string.Format("BOECommon.GetObjectFromName: {0} named {1} with Parent ID {2} not found", kind, name, parentID);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "Unable to get object. Err=" + ex.Message;
                StackTrace = ex.StackTrace;
            }
            return null;
        }

        public InfoObject GetObjectFromName(string name, string fields, string kind, CETables table)
        {
            clearErr();
            string query = string.Format("Select {0} from {1} where SI_NAME='{2}' and SI_KIND = '{3}'", fields, table.ToString(), name, kind);
            using (InfoObjects iobjs = ExecuteRawQuery(query))
            {
                if ((iobjs != null) && (iobjs.Count > 0))
                {
                    return iobjs[1];
                }
                else
                {
                    if (ErrMsg == string.Empty)
                        ErrMsg = string.Format("BOECommon.GetObjectFromName: {0} named {1} not found", kind, name);
                }
            }
            return null;
        }

        /// <summary>
        /// Get a count based on the supplied query.
        /// </summary>
        /// <param name="query">The query to run.</param>
        /// <param name="field">The field to get the count from.</param>
        /// <returns>The count from the query, if found, otherwise -1.</returns>
        public int GetObjectCount(string query, string field)
        {
            clearErr();
            int result = -1;
            try
            {
                using (InfoObjects iobjs = ExecuteRawQuery(query))
                {
                    if ((iobjs != null) && (iobjs.Count > 0))
                    {
                        InfoObject iobj = iobjs[1];
                        Properties props = GetProperties(iobj.Properties, "SI_AGGREGATE_COUNT");
                        string countStr = GetPropertyString(props, field);
                        if (countStr != string.Empty)
                            result = int.Parse(countStr);
                    }
                    else
                    {
                        if (ErrMsg == string.Empty)
                            ErrMsg = string.Format("BOECommon.GetObjectCount: Count not found for query {0}", query);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = ex.Message;
                StackTrace = ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// Get the folder path to a report or instance.
        /// </summary>
        /// <param name="id">The SI_ID of the object whose path will be found.</param>
        /// <returns>The folder path to the object.</returns>
        public string GetObjectPath(int id, CETables table)
        {
            clearErr();
            string result = string.Empty;
            string query = "Select SI_NAME, SI_PARENT_FOLDER from {0} where SI_ID={1}";
            string parentName = string.Empty;
            int parentId;
            try
            {
                using (InfoObjects iobjs = ExecuteRawQuery(string.Format(query, table, id)))
                {
                    if ((iobjs != null) && (iobjs.Count > 0))
                    {
                        parentId = int.Parse(iobjs[1].Properties["SI_PARENT_FOLDER"].Value.ToString());
                        if (parentId > 0)
                        {
                            result = GetPathFromFolderID(parentId, table);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "BOECommon.GetObjectPath: Unable to get folder path.  Err=" + ex.Message;
                StackTrace = ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// Get the kind of an object based on its SI_ID.
        /// </summary>
        /// <param name="id">The SI_ID to search for.</param>
        /// <returns>The SI_KIND value, if found, otherwise "Unknown".</returns>
        public string GetObjectType(int id)
        {
            clearErr();
            string result = "Unknown";
            string query = string.Format("Select SI_KIND from CI_INFOOBJECTS, CI_SYSTEMOBJECTS, CI_APPOBJECTS where SI_ID = {0}", id);
            try
            {
                using (InfoObjects iobjs = ExecuteRawQuery(query))
                {
                    if ((iobjs != null) && (iobjs.Count > 0))
                    {
                        result = iobjs[1].Kind;
                    }
                    else
                    {
                        if (ErrMsg == string.Empty)
                            ErrMsg = string.Format("BOECommon.GetObjectType: Object with ID {0} not found", id);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "BOECommon.GetObjectType: Unable to get object kind. Err=" + ex.Message;
                StackTrace = ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// Get the parent SI_ID of an object
        /// </summary>
        /// <param name="id">The SI_ID of the child object.</param>
        /// <param name="table">The CMS table to get the parent from.</param>
        /// <returns></returns>
        public int GetParentId(int id, CETables table)
        {
            int result = -1;
            InfoObject iobj = GetObjectById(id, "SI_ID, SI_PARENT_ID", table);
            if (iobj != null)
            {
                result = iobj.ParentID;
            }
            return result;
        }

        /// <summary>
        /// Walk up the list of parent ID's to generate the path of a specific folder
        /// </summary>
        /// <param name="folderID">The id of the folder we're looking for</param>
        /// <returns></returns>
        public string GetPathFromFolderID(int folderID, CETables table)
        {
            clearErr();
            string result = string.Empty;
            try
            {
                string query = string.Format("Select SI_NAME, SI_PATH from {0} where SI_ID = {1}", table.ToString(), folderID);

                using (InfoObjects iObj = _BOEInfoStore.Query(query))
                {
                    if (iObj.Count > 0)
                    {
                        result = iObj[1].Title;
                        int folderCount = (int)iObj[1].Properties["SI_PATH"].Properties["SI_NUM_FOLDERS"].Value;
                        if (folderCount > 0)
                        {
                            for (int i = 1; i <= folderCount; i++)
                            {
                                result = iObj[1].Properties["SI_PATH"].Properties["SI_FOLDER_NAME" + i.ToString()].Value.ToString() +
                                  "\\" + result;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = string.Format("BOECommon.GetPathFromFolderID: Unable to get path for Folder ID {0}. Err={1}", folderID, ex.Message);
                StackTrace = ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// Returns a new, empty InfoObjects collection.
        /// </summary>
        /// <returns>An InfoObjects collection.</returns>
        public InfoObjects NewInfoObjects()
        {
            return _BOEInfoStore.NewInfoObjectCollection();
        }

        public ISInfoStore getInfoStore()
        {
            return _BOEInfoStore.Interface;
        }

        public ISEnterpriseSession getSession()
        {
            return _BOESession.Interface;
        }

        /// <summary>
        /// Determines whether an object exists based on its name.
        /// </summary>
        /// <param name="objName">The name of the object.</param>
        /// <param name="qryFilter">Any query filter to use in addition to the name.</param>
        /// <param name="table">The name of the table to query.</param>
        /// <returns>True if the object is found, false if it isn't.</returns>
        public bool ObjectExistsByName(string objName, string qryFilter, CETables table)
        {
            clearErr();
            bool result = false;
            string query = "Select SI_ID from {0} where SI_NAME = '{1}' {2}";
            string filter = (qryFilter != string.Empty ? "and " + qryFilter : string.Empty);
            using (InfoObjects iobjs = ExecuteRawQuery(string.Format(query, table.ToString(), objName, filter)))
            {
                result = ((iobjs != null) && (iobjs.Count > 0));
            }
            return result;
        }

        /// <summary>
        /// Determine whether an object exists based on its SI_ID.
        /// </summary>
        /// <param name="id">The SI_ID of the object.</param>
        /// <param name="table">The name of the table to query.</param>
        /// <returns>True if the object is found, false if it isn't.</returns>
        public bool ObjectExistsByID(int id, CETables table)
        {
            string query = string.Format("Select SI_ID from {0} where SI_ID = {1}", table.ToString(), id);
            using (InfoObjects iobjs = ExecuteRawQuery(query))
            {
                return ((iobjs != null) && (iobjs.Count > 0));
            }
        }

        /// <summary>
        /// Return the value of the specified property as a string.  If the property doesn't exist, return null.
        /// </summary>
        /// <param name="props"></param>
        /// <param name="propName"></param>
        /// <returns></returns>
        public static string GetPropertyString(Properties props, string propName)
        {
            string value = "";
            clearErr();
            try
            {
                if (propName == "SI_AGGREGATE_COUNT")
                {
                    value = props[propName].Properties["SI_ID"].Value.ToString();
                }
                else
                {
                    value = props[propName].Value.ToString();
                }

                return value;
            }
            catch (Exception ex)
            {
                ErrMsg = string.Format("BOECommon.GetPropertyString:  Unable to get value for {0}.  Err={1}", propName, ex.Message);
                StackTrace = ex.StackTrace;
                return string.Empty;
            }
        }

        public static int GetPropertyInt(Properties props, string propName)
        {
            clearErr();
            try
            {
                int value = int.Parse(props[propName].Value.ToString());
                return value;
            }
            catch (Exception ex)
            {
                ErrMsg = string.Format("BOECommon.GetPropertyString:  Unable to get value for {0}.  Err={1}", propName, ex.Message);
                StackTrace = ex.StackTrace;
                return -1;
            }
        }

        public static DateTime GetPropertyDateTime(Properties props, string propName)
        {
            DateTime result = DateTime.Parse("01/01/1990 12:00:00 AM");
            try
            {
                string s = GetPropertyString(props, propName);
                if (s != string.Empty)
                {
                    result = DateTime.Parse(s);
                }
            }
            catch (Exception ex)
            {
                ErrMsg = string.Format("BOECommon.GetPropertyDateTime:  Unable to get value for {0}.  Err={1}", propName, ex.Message);
                StackTrace = ex.StackTrace;
            }
            return result;
        }

        public static Properties GetProperties(Properties props, string propName)
        {
            clearErr();
            try
            {
                Properties result = props[propName].Properties;
                return result;
            }
            catch (Exception ex)
            {
                ErrMsg = string.Format("BOECommon.GetProperties:  Unable to get properties for {0}.  Err={1}", propName, ex.Message);
                StackTrace = ex.StackTrace;
                return null;
            }
        }

        /// <summary>
        /// Schedule one or more reports.
        /// </summary>
        /// <param name="iobjs">The list of objects to schedule</param>
        /// <returns>True if the objects were successfully scheduled.</returns>
        public bool ScheduleObjects(InfoObjects iobjs)
        {
            try
            {
                _BOEInfoStore.Schedule(iobjs);
                return true;
            }
            catch (Exception ex)
            {
                ErrMsg = "BOECommon: Unable to Schedule Report(s).  Err=" + ex.Message;
                StackTrace = ex.StackTrace;
                return false;
            }
        }

        public string GetKindFromID(int id, CETables table)
        {
            //Error handling is taken care of in GetObjectById
            string result = string.Empty;
            using (InfoObject iobj = GetObjectById(id, "SI_KIND", table))
            {
                result = iobj.Kind;
            }
            return result;
        }

        public bool SetPropertyString(Properties props, string propName, string value)
        {
            clearErr();
            try
            {
                props[propName].Value = value;
                return true;
            }
            catch (Exception ex)
            {
                ErrMsg = string.Format("BOECommon.GetPropertyString:  Unable to set value for {0}.  Err={1}", propName, ex.Message);
                StackTrace = ex.StackTrace;
                return false;
            }
        }

        public bool SetPropertyInt(Properties props, string propName, int value)
        {
            clearErr();
            try
            {
                props[propName].Value = value;
                return true;
            }
            catch (Exception ex)
            {
                ErrMsg = string.Format("BOECommon.GetPropertyString:  Unable to set value for {0}.  Err={1}", propName, ex.Message);
                StackTrace = ex.StackTrace;
                return false;
            }
        }

        public string GetCUIDfromId(int id)
        {
            clearErr();
            string result = string.Empty;
            using (InfoObject iobj = GetObjectById(id, "SI_CUID", CETables.CI_INFOOBJECTS))
            {
                if (iobj != null)
                {
                    result = iobj.CUID;
                }
            }
            return result;
        }

        public int CreateObject(string pluginName, string objName, string descrip, int parentId, CETables table)
        {
            clearErr();
            int result = -1;
            try
            {
                PluginInfo newFldr = GetPluginInfo(pluginName);
                InfoObjects newObjs = NewInfoObjects();
                newObjs.Add(newFldr);
                newObjs[1].Title = objName;
                newObjs[1].Description = descrip;
                newObjs[1].ParentID = parentId;
                if (CommitObjects(newObjs))
                {
                    result = GetIDFromName(objName, "Folder", parentId, table);
                }
            }
            catch (Exception ex)
            {
                ErrMsg = string.Format("Unable to create folder {0}. Err={1}", objName, ex.Message);
                StackTrace = ex.StackTrace;
            }
            return result;
        }

        #endregion Public_Methods

        #region Private_methods

        /// <summary>
        /// Set ErrMsg and StackTrace to empty strings.
        /// </summary>
        private static void clearErr()
        {
            ErrMsg = string.Empty;
            StackTrace = string.Empty;
        }

        /// <summary>
        /// Log in to the CMS and set up the session and service required
        /// to access an InfoStore
        /// </summary>
        private void initialize()
        {
            TokenExpirationMinutes = 10;
            TokenLogons = 100;
            if ((_userName == string.Empty))
            {
                throw new Exception("Unable to log in to BOE - UserName not set");
            }
            if (!_initialized)
            {
                System.Threading.Thread.Sleep(500);
                //log in to BOE
                using (SessionMgr sessionMgr = new SessionMgr())
                {
                    try
                    {
                        if (_password == string.Empty)
                        {
                            if (_token == string.Empty)
                            {
                                //do trusted authentication
                                TrustedPrincipal tp = sessionMgr.CreateTrustedPrincipal(_userName, _CMS);
                                _BOESession = sessionMgr.LogonTrustedPrincipal(tp);
                            }
                            else
                            {
                                //log on with a token
                                _BOESession = sessionMgr.LogonWithToken(_token);
                            }
                        }
                        else
                        {
                            _BOESession = sessionMgr.Logon(_userName, _password, _CMS, _Authentication);
                        }
                        _BOEService = _BOESession.GetService("InfoStore");
                        _BOEInfoStore = (InfoStore)_BOEService;
                        _initialized = true;
                        LoggedOn = true;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Unable to log in to BOE - " + ex.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Log off of the CMS
        /// </summary>
        private void closeSession()
        {
            if (_initialized)
            {
                try
                {
                    _BOESession.Logoff();
                }
                catch
                {
                    //eat the error...it only seems to happen when the session BOESession has been
                    //disposed prior to getting here...
                }
                _initialized = false;
            }
        }

        #endregion Private_methods

        #region Static_methods

        /// <summary>
        /// Get a logon token using trusted authentication without instantiating a BOECommon object.
        /// </summary>
        /// <param name="userID">The user name of the user.</param>
        /// <param name="cms">The CMS to log the user into.</param>
        /// <param name="validMinutes">The number of minutes the token is valid.</param>
        /// <param name="validLogons">The number of logons the token can be used for.</param>
        /// <returns></returns>
        public static string GetTrustedLogonToken(string userID, string cms, int validMinutes, int validLogons)
        {
            string result = string.Empty;
            using (SessionMgr smgr = new SessionMgr())
            {
                TrustedPrincipal tp = smgr.CreateTrustedPrincipal(userID, cms);
                EnterpriseSession sess = smgr.LogonTrustedPrincipal(tp);
                result = sess.LogonTokenMgr.CreateLogonTokenEx("", validMinutes, validLogons);
                sess.Logoff();
                sess.Dispose();
            }
            return result;
        }

        #endregion Static_methods
    }
}