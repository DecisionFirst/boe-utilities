﻿using CrystalDecisions.Enterprise;

namespace DFT.BOE
{
    public class BOEObjectInfo
    {
        public BOEObjectInfo()
        {
            ID = -1;
            ParentID = -1;
            Name = string.Empty;
            Kind = string.Empty;
            CUID = string.Empty;
            Description = string.Empty;
            Path = string.Empty;
        }

        public BOEObjectInfo(InfoObject iobj)
        {
            ID = iobj.ID;
            ParentID = iobj.ParentID;
            Name = iobj.Title;
            Kind = iobj.Kind;
            CUID = iobj.CUID;
            Description = iobj.Description;
            Path = string.Empty;
        }

        public BOEObjectInfo(InfoObject iobj, BOECommon common, BOECommon.CETables table)
        {
            ID = iobj.ID;
            ParentID = iobj.ParentID;
            Name = iobj.Title;
            Kind = iobj.Kind;
            CUID = iobj.CUID;
            Description = iobj.Description;
            Path = common.GetObjectPath(iobj.ID, table);
        }

        public int ID { get; set; }
        public int ParentID { get; set; }
        public string Name { get; set; }
        public string Kind { get; set; }
        public string CUID { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
    }
}