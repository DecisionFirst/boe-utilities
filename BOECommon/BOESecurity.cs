﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace DFT.BOE.Security
{
    internal class InvalidHashException : Exception
    {
        public InvalidHashException()
        {
        }

        public InvalidHashException(string message)
            : base(message) { }

        public InvalidHashException(string message, Exception inner)
            : base(message, inner) { }
    }

    internal class CannotPerformOperationException : Exception
    {
        public CannotPerformOperationException()
        {
        }

        public CannotPerformOperationException(string message)
            : base(message) { }

        public CannotPerformOperationException(string message, Exception inner)
            : base(message, inner) { }
    }

    public class BOESecurity
    {
        public string keyStr = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
        public static byte[] hash_key_Array;
        public static String PBKDF2_ALGORITHM = "SHA1";

        public const int SALT_BYTES = 8;
        public const int HASH_BYTES = 16;

        public const int HASH_SECTIONS = 5;
        public const int HASH_ALGORITHM_INDEX = 0;
        public const int ITERATION_INDEX = 1;
        public const int HASH_SIZE_INDEX = 2;
        public const int SALT_INDEX = 3;
        public const int PBKDF2_INDEX = 4;

        public const int AES_BLOCK_SIZE = 128;
        public const int AES_KEY_SIZE = 128;

        private static byte[] bhash;
        public static byte[] salt;

        public BOESecurity(string Key)
        {
            //
            keyStr = Key;
            //salt = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };
            //key_Array = pbkdf2(keyStr, salt, 1, 16);
            //Generate Initial Random Hash Key Array
            hash_key_Array = CreateHash(Key);
        }

        public byte[] hash
        {
            get
            {
                return bhash;
            }
            set { bhash = value; }
        }

        public byte[] CreateHash(string password)
        {
            // Generate a random salt
            salt = new byte[SALT_BYTES];
            try
            {
                using (RNGCryptoServiceProvider csprng = new RNGCryptoServiceProvider())
                {
                    csprng.GetBytes(salt);
                }
            }
            catch (CryptographicException ex)
            {
                throw new CannotPerformOperationException(
                    "Random number generator not available.",
                    ex
                );
            }
            catch (ArgumentNullException ex)
            {
                throw new CannotPerformOperationException(
                    "Invalid argument given to random number generator.",
                    ex
                );
            }

            byte[] hash = pbkdf2(password, salt, 1, HASH_BYTES);

            // format: algorithm:iterations:hashSize:salt:hash

            return hash;
        }

        private byte[] pbkdf2(string password, byte[] salt, int iterations, int bytes)
        {
            using (Rfc2898DeriveBytes pbkdf2 = new Rfc2898DeriveBytes(password, salt))
            {
                pbkdf2.IterationCount = iterations;
                return pbkdf2.GetBytes(bytes);
            }
        }

        public bool VerifyPassword(string password, string goodHash)
        {
            char[] delimiter = { ':' };
            string[] split = goodHash.Split(delimiter);

            if (split.Length != HASH_SECTIONS)
            {
                throw new InvalidHashException(
                    "Fields are missing from the password hash."
                );
            }

            // We only support SHA1 with C#.
            if (split[HASH_ALGORITHM_INDEX] != "sha1")
            {
                throw new CannotPerformOperationException(
                    "Unsupported hash type."
                );
            }

            int iterations = 0;
            try
            {
                iterations = Int32.Parse(split[ITERATION_INDEX]);
            }
            catch (ArgumentNullException ex)
            {
                throw new CannotPerformOperationException(
                    "Invalid argument given to Int32.Parse",
                    ex
                );
            }
            catch (FormatException ex)
            {
                throw new InvalidHashException(
                    "Could not parse the iteration count as an integer.",
                    ex
                );
            }
            catch (OverflowException ex)
            {
                throw new InvalidHashException(
                    "The iteration count is too large to be represented.",
                    ex
                );
            }

            if (iterations < 1)
            {
                throw new InvalidHashException(
                    "Invalid number of iterations. Must be >= 1."
                );
            }

            byte[] salt = null;
            try
            {
                salt = Convert.FromBase64String(split[SALT_INDEX]);
            }
            catch (ArgumentNullException ex)
            {
                throw new CannotPerformOperationException(
                    "Invalid argument given to Convert.FromBase64String",
                    ex
                );
            }
            catch (FormatException ex)
            {
                throw new InvalidHashException(
                    "Base64 decoding of salt failed.",
                    ex
                );
            }

            byte[] hash = null;
            try
            {
                hash = Convert.FromBase64String(split[PBKDF2_INDEX]);
            }
            catch (ArgumentNullException ex)
            {
                throw new CannotPerformOperationException(
                    "Invalid argument given to Convert.FromBase64String",
                    ex
                );
            }
            catch (FormatException ex)
            {
                throw new InvalidHashException(
                    "Base64 decoding of pbkdf2 output failed.",
                    ex
                );
            }

            int storedHashSize = 0;
            try
            {
                storedHashSize = Int32.Parse(split[HASH_SIZE_INDEX]);
            }
            catch (ArgumentNullException ex)
            {
                throw new CannotPerformOperationException(
                    "Invalid argument given to Int32.Parse",
                    ex
                );
            }
            catch (FormatException ex)
            {
                throw new InvalidHashException(
                    "Could not parse the hash size as an integer.",
                    ex
                );
            }
            catch (OverflowException ex)
            {
                throw new InvalidHashException(
                    "The hash size is too large to be represented.",
                    ex
                );
            }

            if (storedHashSize != hash.Length)
            {
                throw new InvalidHashException(
                    "Hash length doesn't match stored hash length."
                );
            }

            byte[] testHash = pbkdf2(password, salt, iterations, hash.Length);
            return SlowEquals(hash, testHash);
        }

        private static bool SlowEquals(byte[] a, byte[] b)
        {
            uint diff = (uint)a.Length ^ (uint)b.Length;
            for (int i = 0; i < a.Length && i < b.Length; i++)
            {
                diff |= (uint)(a[i] ^ b[i]);
            }
            return diff == 0;
        }

        public byte[] fromBase64(String hex)
        {
            return Convert.FromBase64String(hex);
        }

        public String toBase64(byte[] array)
        {
            return Convert.ToBase64String(array);
        }

        public string Encrypt(string PlainText)
        {
            RijndaelManaged aes = new RijndaelManaged();
            aes.BlockSize = AES_BLOCK_SIZE;
            aes.KeySize = AES_KEY_SIZE;

            // It is equal in java
            /// Cipher _Cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;

            //byte[] keyArr = Convert.FromBase64String(keyStr);
            //byte[] KeyArrBytes32Value = new byte[32];
            //Array.Copy(keyArr, KeyArrBytes32Value, 32);

            hash = CreateHash(DateTime.Now.ToString("MMddyyyyhhmm"));

            // Initialization vector.
            // It could be any value or generated using a random number generator.
            byte[] ivArr = { 1, 2, 3, 4, 5, 6, 6, 5, 4, 3, 2, 1, 7, 7, 7, 7 };
            byte[] IVBytes16Value = new byte[16];
            Array.Copy(ivArr, IVBytes16Value, 16);

            aes.Key = hash;
            aes.IV = IVBytes16Value;

            ICryptoTransform encrypto = aes.CreateEncryptor();

            byte[] plainTextByte = ASCIIEncoding.UTF8.GetBytes(PlainText);
            byte[] CipherText = encrypto.TransformFinalBlock(plainTextByte, 0, plainTextByte.Length);
            return Convert.ToBase64String(CipherText);
        }

        public string Decrypt(string CipherText, byte[] hash)
        {
            RijndaelManaged aes = new RijndaelManaged();
            aes.BlockSize = AES_BLOCK_SIZE;
            aes.KeySize = AES_KEY_SIZE;

            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;

            //byte[] keyArr = Convert.FromBase64String(keyStr);
            //byte[] keyArr = hash;
            //byte[] KeyArrBytes32Value = new byte[32];
            //Array.Copy(keyArr, KeyArrBytes32Value, 32);

            // Initialization vector.
            // It could be any value or generated using a random number generator.
            byte[] ivArr = { 1, 2, 3, 4, 5, 6, 6, 5, 4, 3, 2, 1, 7, 7, 7, 7 };
            byte[] IVBytes16Value = new byte[16];
            Array.Copy(ivArr, IVBytes16Value, 16);

            //aes.Key = KeyArrBytes32Value;
            aes.Key = hash;
            aes.IV = IVBytes16Value;

            ICryptoTransform decrypto = aes.CreateDecryptor();

            byte[] encryptedBytes = Convert.FromBase64CharArray(CipherText.ToCharArray(), 0, CipherText.Length);
            byte[] decryptedData = decrypto.TransformFinalBlock(encryptedBytes, 0, encryptedBytes.Length);
            return ASCIIEncoding.UTF8.GetString(decryptedData);
        }

        public string Decrypt(string CipherText)
        {
            RijndaelManaged aes = new RijndaelManaged();
            aes.BlockSize = AES_BLOCK_SIZE;
            aes.KeySize = AES_KEY_SIZE;

            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;

            //byte[] keyArr = Convert.FromBase64String(keyStr);
            //byte[] KeyArrBytes32Value = new byte[32];
            //Array.Copy(keyArr, KeyArrBytes32Value, 32);

            // Initialization vector.
            // It could be any value or generated using a random number generator.
            byte[] ivArr = { 1, 2, 3, 4, 5, 6, 6, 5, 4, 3, 2, 1, 7, 7, 7, 7 };
            byte[] IVBytes16Value = new byte[16];
            Array.Copy(ivArr, IVBytes16Value, 16);

            aes.Key = hash;
            aes.IV = IVBytes16Value;

            ICryptoTransform decrypto = aes.CreateDecryptor();

            byte[] encryptedBytes = Convert.FromBase64CharArray(CipherText.ToCharArray(), 0, CipherText.Length);
            byte[] decryptedData = decrypto.TransformFinalBlock(encryptedBytes, 0, encryptedBytes.Length);
            return ASCIIEncoding.UTF8.GetString(decryptedData);
        }
    }
}