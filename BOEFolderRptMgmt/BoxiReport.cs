﻿using CrystalDecisions.Enterprise;
using System;
using System.Collections.Generic;

namespace DFT.BOE
{
    /// <summary>
    /// Moved over from IHPReportingInterface.Classes for BO 4.1 upgrade by Dell Stinnett-Christy 6/18/2014
    /// </summary>
    public abstract class BoxiReport
    {
        /// <summary>
        /// Report Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Report Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// BOXI ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Number of Instances
        /// </summary>
        public int NumberOfInstances { get; set; }

        /// <summary>
        /// BOXI Parent Folder ID
        ///  </summary>
        public int ParentId { get; set; }

        /// <summary>
        /// Last Successfull Instance Id
        /// </summary>
        public int LastSuccessInstanceId { get; set; }

        /// <summary>
        /// If Object is not CR or WEBI (DOC,PDF,WORD)
        /// </summary>
        public bool IsExtFormat { get; set; }

        /// <summary>
        /// Array of Corporate Categories Id's
        /// </summary>
        public List<int> Categories { get; set; }

        /// <summary>
        /// Instance Last Run JH 5/25/2011
        /// </summary>
        public DateTime? LastRun { get; set; }

        /// <summary>
        /// Icon to display with Report             JH 5/25/2011
        /// </summary>
        public string IconFileName { get; set; }

        /// <summary>
        /// Category Associated with Report         JH 5/25/2011
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// List of Report Type Incons              JH 5/27/2011
        /// </summary>
        public List<string> ReportTypeIcons { get; set; }

        /// <summary>
        /// List of Report Type Incons              JH 5/31/2011
        /// </summary>
        public string ReportTypeIconsString { get; set; }

        public string Kind { get; set; }

        public string CUID { get; set; }

        public int PersonalId { get; set; }

        public BoxiReport()
        {
            Categories = new List<int>();
            PersonalId = 0;
        }

        /// <summary>
        /// Creates a BoxiReport based on an InfoObject.
        /// Dell Stinnett-Christy 6/22/2014
        /// </summary>
        /// <param name="rpt">The InfoObject that contains the report's information.</param>
        public BoxiReport(InfoObject rpt)
        {
            Name = rpt.Title;
            Description = rpt.Description;
            Id = rpt.ID;
            ParentId = rpt.ParentID;
            Kind = rpt.Kind;
            IsExtFormat = (rpt.Kind != "CrystalReport");
            NumberOfInstances = BOECommon.GetPropertyInt(rpt.Properties, "SI_CHILDREN");
            Categories = new List<int>();
            loadCategories(rpt);
            PersonalId = 0;
        }

        private void loadCategories(InfoObject rpt)
        {
            try
            {
                Properties catList = BOECommon.GetProperties(rpt.Properties, "SI_CORPORATE_CATEGORIES");
                int catCount = (int)catList["SI_TOTAL"].Value;
                for (int i = 1; i <= catCount; i++)
                {
                    Categories.Add((int)catList[i.ToString()].Value);
                }
            }
            catch
            {
                //eat the exception - this usually means there are no categories...
            }
        }
    }
}