﻿using System;

namespace DFT.BOE
{
    /// <summary>
    /// Represents Recurring Instances
    /// </summary>
    public class StaticReport : BoxiReport
    {
        /// <summary>
        /// Daily, Weekly, Montnhly
        /// </summary>
        public string Frequency { get; set; }

        /// <summary>
        /// Custom Property Values to find children of recurring parent
        /// </summary>
        public int RecurringParentId { get; set; }

        /// <summary>
        /// JH 5/25/11
        /// </summary>
        public string CategoryIconFilename { get; set; }

        /// <summary>
        /// Indicates if location is in Personal Folder
        /// </summary>
        public bool IsPersonalFolder { get; set; }

        public StaticReport(string[] rpt)
            : base()
        {
            Id = Convert.ToInt32(rpt[0]);
            ParentId = Convert.ToInt32(rpt[1]);
            Name = rpt[2];
            Description = rpt[3];
            Category = rpt[10];                  //WE NEED THIS
        }
    }
}