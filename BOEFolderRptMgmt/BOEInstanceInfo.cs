﻿using CrystalDecisions.Enterprise;
using CrystalDecisions.Enterprise.Desktop;
using System;

namespace DFT.BOE
{
    public class BOEInstanceInfo : BOEObjectInfo
    {
        public const string STR_PARAMETER_COMPANY_ID = "IHP COMPANY ID";
        public const string STR_PARAMETER_USER_ID = "IHP USER ID ";

        public BOEInstanceInfo(InfoObject inst, BOECommon common)
        {
            this.ID = inst.ID;
            this.Name = inst.Title;
            switch (inst.Kind)
            {
                case "Excel":
                    this.Kind = "Microsoft Excel";
                    break;

                case "PDF":
                    this.Kind = "Adobe Acrobat";
                    break;

                case "CrystalReport":
                    this.Kind = "Crystal Report";
                    break;
            }
            this.Kind = (inst.Kind == "CrystalReport" ? "Crystal Report" : inst.Kind);
            this.ParentID = inst.ParentID;
            loadParams(inst);
            loadStatus(inst.SchedulingInfo.Status);
            loadRunTime(inst);
        }

        public string Parameters { get; private set; }
        public string Status { get; private set; }
        public string ErrMsg { get; private set; }
        public DateTime InstanceTime { get; private set; }

        private void loadParams(InfoObject inst)
        {
            Parameters = string.Empty;
            try
            {
                using (Report rpt = new Report(inst.GetPluginInterface("Report")))
                {
                    ReportParameters rptParams = rpt.ReportParameters;

                    foreach (ReportParameter param in rptParams)
                    {
                        if (param.ParameterName.ToUpper().CompareTo(STR_PARAMETER_COMPANY_ID.ToUpper().TrimEnd()) != 0 && param.ParameterName.ToUpper().CompareTo(STR_PARAMETER_USER_ID.ToUpper().TrimEnd()) != 0)  //bypass showing company and userid
                        {
                            Parameters += string.Format("{0}: ", param.ParameterName);
                            foreach (ReportParameterValue paramValue in param.CurrentValues)
                            {
                                Parameters += paramValue.SingleValue.Description.ToString() + ", ";
                            }
                            Parameters = FixParameterList(Parameters, true) + ";";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = String.Format("BOEInstanceInfo: Unable to load parameters for report {0}.  Err={1}", inst.Title, ex.Message);
            }
            Parameters = FixParameterList(Parameters, false);
        }

        private string FixParameterList(string _parametersList, bool isComma)
        {
            char charToRemove = isComma ? ',' : ';';
            string _outParamsList = _parametersList;
            try
            {
                int _removeIndex = _parametersList.LastIndexOf(charToRemove);
                _outParamsList = _parametersList.Remove(_removeIndex);
                return _outParamsList;
            }
            catch
            {
                return _parametersList;
            }
        }

        private void loadStatus(CeScheduleStatus stat)
        {
            switch (stat)
            {
                case CeScheduleStatus.ceStatusPending:
                    Status = "Pending";
                    break;

                case CeScheduleStatus.ceStatusRunning:
                    Status = "Running";
                    break;

                case CeScheduleStatus.ceStatusPaused:
                    Status = "Paused";
                    break;

                case CeScheduleStatus.ceStatusSuccess:
                    Status = "Success";
                    break;

                case CeScheduleStatus.ceStatusFailure:
                    Status = "Failed";
                    break;

                default:
                    Status = "N/A";
                    break;
            }
        }

        private void loadRunTime(InfoObject inst)
        {
            try
            {
                DateTime dateTime = DateTime.Parse(inst.Properties["SI_ENDTIME"].ToString());
                InstanceTime = dateTime;
            }
            catch
            {
                InstanceTime = DateTime.Now;
            }
        }
    }
}