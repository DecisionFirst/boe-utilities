﻿using CrystalDecisions.Enterprise;
using CrystalDecisions.Enterprise.Desktop;

namespace DFT.BOE
{
    //LAST_RUN_ID, LAST_RUN_DATE, CATEGORY_ID, CATEGORY_NAME, PERSONAL_VERSION_COUNT, RECURRING_VERSION_COUNT
    internal class BOERptInfo : BOEObjectInfo
    {
        public int SchedStatusInt { get; set; }
        public string SchedStatus { get; set; }
        public bool Instance { get; set; }
        public int OwnerID { get; set; }
        public string OwnerName { get; set; }
        public int LastRunID { get; set; }
        public string LastRunDate { get; set; }
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public int PersonalInstanceCount { get; set; }
        public int RecurringInstanceCount { get; set; }

        //public int PersonalID { get; set; } //RptId identity value from tb_PersonalRpts in database.
        private Report rpt { get; set; }

        public BOERptInfo()
            : base()
        {
            SchedStatusInt = 0;
            SchedStatus = string.Empty;
            Instance = false;
            OwnerID = 0;
            OwnerName = string.Empty;
            LastRunID = 0;
            LastRunDate = string.Empty;
            CategoryID = 0;
            CategoryName = string.Empty;
            PersonalInstanceCount = 0;
            RecurringInstanceCount = 0;
            //PersonalID = 0;
            rpt = null;
        }

        public BOERptInfo(InfoObject iobj, BOECommon common)
            : base(iobj)
        {
            Properties props = iobj.Properties;
            Instance = iobj.Instance;
            if (Instance)
            {
                SchedStatusInt = BOECommon.GetPropertyInt(props, "SI_SCHEDULE_STATUS");
                SchedStatus = iobj.SchedulingInfo.Status.ToString();
            }
            else
            {
                SchedStatusInt = -1;
                SchedStatus = string.Empty;
            }
            OwnerID = BOECommon.GetPropertyInt(props, "SI_OWNERID");
            OwnerName = BOECommon.GetPropertyString(props, "SI_OWNER");
            CategoryID = 0;
            CategoryName = string.Empty;
            PersonalInstanceCount = 0;
            RecurringInstanceCount = 0;
            //PersonalID = 0;
            rpt = null;
        }

        public BOERptInfo(InfoObject iobj, BOECommon common, bool saveRpt)
            : base(iobj)
        {
            Properties props = iobj.Properties;
            Instance = iobj.Instance;
            if (Instance)
            {
                SchedStatusInt = BOECommon.GetPropertyInt(props, "SI_SCHEDULE_STATUS");
                SchedStatus = iobj.SchedulingInfo.Status.ToString();
            }
            else
            {
                SchedStatusInt = -1;
                SchedStatus = string.Empty;
            }
            OwnerID = BOECommon.GetPropertyInt(props, "SI_OWNERID");
            OwnerName = BOECommon.GetPropertyString(props, "SI_OWNER");
            CategoryID = 0;
            CategoryName = string.Empty;
            PersonalInstanceCount = 0;
            RecurringInstanceCount = 0;
            //PersonalID = 0;
            if (saveRpt && iobj.Kind == "CrystalReport")
            {
                rpt = (Report)iobj;
            }
            else
            {
                rpt = null;
            }
        }

        public string GetArrayString(string delimiter)
        {
            string result = string.Format("{1}{0}{2}{0}{3}{0}{4}{0}{5}{0}{6}{0}{7}{0}{8}{0}{9}{0}{10}{0}{11}{0}{12}{0}{13}{0}{14}{0}{15}{0}{16}{0}{17}",
              delimiter, ID, ParentID, Name, Description, Kind, (Instance ? 1 : 0), SchedStatusInt, string.Empty, OwnerID,
              OwnerName, CategoryID, CategoryName, LastRunID, LastRunDate, PersonalInstanceCount, RecurringInstanceCount, string.Empty);
            //PersonalID);
            return result;
        }
    }
}