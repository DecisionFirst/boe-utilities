﻿namespace DFT.BOE
{
    public enum CeReportVariableValueType
    {
        ceRVNumber,
        ceRVCurrency,
        ceRVBoolean,
        ceRVDate,
        ceRVTime,
        ceRVDateTime,
        ceRVString
    }
}