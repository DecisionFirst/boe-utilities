﻿using CrystalDecisions.Enterprise;
using System;

namespace DFT.BOE
{
    public class BOEScheduleInfo
    {
        public DateTime BeginDate { get; private set; }
        public int Interval { get; private set; }

        public BOEScheduleInfo(SchedulingInfo si)
        {
            BeginDate = si.BeginDate;
            switch (si.Type)
            {
                case CeScheduleType.ceScheduleTypeDaily:
                    Interval = 0;
                    break;

                case CeScheduleType.ceScheduleTypeWeekly:
                    Interval = 1;
                    break;

                case CeScheduleType.ceScheduleTypeMonthly:
                    Interval = 2;
                    break;

                default:
                    Interval = 99;
                    break;
            }
        }
    }
}