﻿using System;

namespace DFT.BOE
{
    public class PromptValue
    {
        private string _mValue;
        private readonly string _mDescription;

        /// <summary>
        /// Initializes a new instance of the <see cref="PromptValue"/> class.
        /// </summary>
        public PromptValue()
        {
            _mValue = String.Empty;
            _mDescription = String.Empty;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PromptValue"/> class.
        /// </summary>
        /// <param name="valueAndDescription">The value_and_description.</param>
        public PromptValue(string valueAndDescription)
        {
            _mValue = valueAndDescription;
            _mDescription = valueAndDescription;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PromptValue"/> class.
        /// </summary>
        /// <param name="val">The val.</param>
        /// <param name="desc">The desc.</param>
        public PromptValue(string val, string desc)
        {
            _mValue = val;
            _mDescription = desc;
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value>The value.</value>
        public string Value
        {
            get { return _mValue; }
            set { _mValue = value; }
        }

        /// <summary>
        /// Gets the description.
        /// </summary>
        /// <value>The description.</value>
        public string Description
        {
            get { return _mDescription; }
        }
    }
}