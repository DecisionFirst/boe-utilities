﻿using CrystalDecisions.Enterprise;
using CrystalDecisions.Enterprise.Desktop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace DFT.BOE
{
    public class BOEFolderRptMgmt : IDisposable
    {
        public const int C_INT_REPORT_FAILED = -1;
        public const int C_INT_REPORT_SUCCESS = 1;
        public const int C_INT_REPORT_SUCCESS_FAILURE_UNDERTERMINED = -3;
        public const int C_INT_METHOD_FAIL = 0;
        private const string STR_FORMAT_PDF = "PDF";
        private const string STR_FORMAT_EXCEL = "Excel";

        private BOECommon _common;
        private bool canDispose = true;
        private string _crystalIconFile = string.Empty;

        #region Class_Methods

        /// <summary>
        /// Constructor - create the BOECommon object that holds the connection
        /// to the CMS.
        /// <param name="userName">The User ID for logging in to the CMS</param>
        /// <param name="password">The Password for loggin in to the CMS</param>
        /// <param name="cms">The CMS to log in to</param>
        /// </summary>
        public BOEFolderRptMgmt(string userName, string password, string cms)
        {
            clearErr();
            _common = new BOECommon(userName, password, cms);
            ErrMsg = BOECommon.ErrMsg;
            StackTrace = BOECommon.StackTrace;
            ExcelUseDataOnlyFormat = false;
            CustomColumnWidth = 10;
            IconFile = string.Empty;
        }

        public BOEFolderRptMgmt(string token)
        {
            clearErr();
            _common = new BOECommon(token);
            ErrMsg = BOECommon.ErrMsg;
            StackTrace = BOECommon.StackTrace;
            ExcelUseDataOnlyFormat = false;
            CustomColumnWidth = 10;
            IconFile = string.Empty;
        }

        public BOEFolderRptMgmt(BOECommon common)
        {
            clearErr();
            _common = common;
            canDispose = false;  //we don't want to dispose the BOECommon because it was passed in to the object.
            ExcelUseDataOnlyFormat = false;
            CustomColumnWidth = 10;
            IconFile = string.Empty;
        }

        ~BOEFolderRptMgmt()
        {
            Dispose(true);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool Managed)
        {
            if (canDispose && (_common != null))
            {
                try
                {
                    _common.Dispose();
                    _common = null;
                    System.GC.SuppressFinalize(this);
                }
                catch
                { }  //eat it because we don't want any _errMsgs here
            }
        }

        public string ErrMsg { get; private set; }
        public string StackTrace { get; private set; }
        public bool ExcelUseDataOnlyFormat { get; set; }
        public int CustomColumnWidth { get; set; }
        public string IconFile { get; set; }

        #endregion Class_Methods

        #region Public_Folders

        /// <summary>
        /// Copy a folder and its contents to another folder
        /// </summary>
        /// <param name="fromId">The SI_ID of the source folder.</param>
        /// <param name="toName">The Name of the destination folder.  This folder must exist in the system.</param>
        /// <returns>True if successful.</returns>
        public bool CopyFolder(int fromId, string toName)
        {
            clearErr();
            int toId = _common.GetIDFromName(toName, "Folder", BOECommon.CETables.CI_INFOOBJECTS);
            if (toId > 0)
            {
                return CopyFolder(fromId, toId);
            }
            else
            {
                ErrMsg = string.Format("Destination Folder {0} does not exist!", toName);
                return false;
            }
        }

        /// <summary>
        /// Copy a folder and its contents to another folder.
        /// </summary>
        /// <param name="fromId">The SI_ID of the source folder.</param>
        /// <param name="toId">The SI_ID of the destination folder.</param>
        /// <returns>True if successful.</returns>
        public bool CopyFolder(int fromId, int toId)
        {
            clearErr();
            string fromName = string.Empty;
            try
            {
                //Does to folder exist in destination? If so, delete it.
                fromName = _common.GetNameFromID(toId, BOECommon.CETables.CI_INFOOBJECTS);
                deleteExistingFolder(fromId, toId, fromName);

                //Now copy the source folder
                InfoObject sourceFldr = _common.GetObjectById(fromId, "*", BOECommon.CETables.CI_INFOOBJECTS);
                if (sourceFldr != null)
                {
                    InfoObjects newObjs = _common.NewInfoObjects();
                    newObjs.Copy(sourceFldr, CeCopyObject.ceCopySameObject);
                    newObjs[1].ParentID = toId;
                    newObjs[1].Title = sourceFldr.Title;
                    _common.CommitObjects(newObjs);
                    return true;
                }
                else
                {
                    ErrMsg = string.Format("Unable to get source folder {0}. Err={1}", (fromName == string.Empty ? fromId.ToString() : fromName), BOECommon.ErrMsg);
                }
            }
            catch (Exception ex)
            {
                ErrMsg = String.Format("Unable to copy folder {0}. Err={1}", (fromName == string.Empty ? fromId.ToString() : fromName), ex.Message);
                StackTrace = ex.StackTrace;
            }
            return false;
        }

        /// <summary>
        /// Move a folder and its contents to another folder.
        /// </summary>
        /// <param name="fromId">The SI_ID of the source folder.</param>
        /// <param name="toName">The name of the destination folder.  This folder must already exist.</param>
        /// <returns>True if successful.</returns>
        public bool MoveFolder(int fromId, string toName)
        {
            clearErr();
            int toId = _common.GetIDFromName(toName, "Folder", BOECommon.CETables.CI_INFOOBJECTS);
            if (toId > 0)
            {
                return MoveFolder(fromId, toId);
            }
            else
            {
                if (BOECommon.ErrMsg == string.Empty)
                {
                    ErrMsg = "Unable to locate destination folder " + toName;
                }
                else
                {
                    ErrMsg = string.Format("Unable to locate destination folder {0}. Err={1}", toName, BOECommon.ErrMsg);
                }
                return false;
            }
        }

        /// <summary>
        /// Move the source folder and its contents to another folder.
        /// </summary>
        /// <param name="fromId">The SI_ID of the source folder.</param>
        /// <param name="toId">The SI_ID of the destinatio folder.</param>
        /// <returns></returns>
        public bool MoveFolder(int fromId, int toId)
        {
            clearErr();
            string fromName = string.Empty;
            try
            {
                string query = string.Format("select SI_ID, SI_NAME, SI_PARENTID from CI_INFOOBJECTS where SI_PARENTID = {0}", fromId);
                //Does to folder exist in destination? If so, delete it.
                fromName = _common.GetNameFromID(toId, BOECommon.CETables.CI_INFOOBJECTS);
                if (deleteExistingFolder(fromId, toId, fromName))
                {
                    using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
                    {
                        if (iobjs != null)
                        {
                            foreach (InfoObject iobj in iobjs)
                            {
                                iobj.ParentID = toId;
                            }
                            if (_common.CommitObjects(iobjs))
                            {
                                return true;
                            }
                            else
                            {
                                ErrMsg = BOECommon.ErrMsg;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = string.Format("Unable to move folder {0}.  Err={1}", (fromName == string.Empty ? fromId.ToString() : fromName), ex.Message);
                StackTrace = ex.StackTrace;
            }
            return false;
        }

        /// <summary>
        /// Move the contents of one folder into another folder - renaming objects that have the
        ///   same name.  Optionally, change the objects' owner.
        /// </summary>
        /// <param name="fromID">The folder we're moving from.</param>
        /// <param name="toId">The folder we're moving to.</param>
        /// <returns></returns>
        public bool MergeFolders(int fromID, int toId, string newOwnerName, int newOwnerId)
        {
            string query = string.Format("Select SI_ID, SI_NAME, SI_KIND, SI_OWNER, SI_OWNERID from CI_INFOOBJECTS where SI_PARENTID={0}", fromID);
            using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
            {
                if (iobjs == null) //there was an error in the query
                {
                    ErrMsg = BOECommon.ErrMsg;
                    StackTrace = BOECommon.StackTrace;
                    return false;
                }
                else if (iobjs.Count == 0)  //no objects were found
                {
                    return true;
                }
                else  //we have objects to move
                {
                    bool updateIt = true;
                    foreach (InfoObject iobj in iobjs)
                    {
                        string title = iobj.Title;
                        int existObjectId = _common.GetIDFromName(iobj.Title, iobj.Kind, toId, BOECommon.CETables.CI_INFOOBJECTS);
                        if (existObjectId > 0) //an object exists with the same name and kind
                        {
                            if (iobj.Kind == "Folder")  //merge the contents of the from folder with the to folder
                            {
                                MergeFolders(iobj.ID, existObjectId, newOwnerName, newOwnerId);
                                updateIt = false;  //we're not moving this folder over, so we won't update the iobj.
                            }
                            else  //rename it - searching for a title that doesn't exist
                            {
                                int item = 1;
                                while (existObjectId > 0)
                                {
                                    title = string.Format("{0} (Copy {1})", iobj.Title, item);
                                    existObjectId = _common.GetIDFromName(title, iobj.Keyword, toId, BOECommon.CETables.CI_INFOOBJECTS);
                                    item++;
                                }
                            }
                        }
                        if (updateIt)  //set all of the properties and save iobj
                        {
                            iobj.Title = title;
                            iobj.ParentID = toId;
                            if (newOwnerId > 0)
                                _common.SetPropertyInt(iobj.Properties, "SI_OWNERID", newOwnerId);
                            if (newOwnerName != string.Empty)
                                _common.SetPropertyString(iobj.Properties, "SI_OWNER", newOwnerName);
                            iobj.Save();
                        }
                    }
                    return true;
                }
            }
        }

        /// <summary>
        /// Create a new folder.
        /// </summary>
        /// <param name="fldr">A BOEObjectInfo which contains information about the folder that will be added.</param>
        /// <returns>True if successful.</returns>
        public int CreateFolder(BOEObjectInfo fldr)
        {
            clearErr();
            int result = -1;
            result = _common.CreateObject("CrystalEnterprise.Folder", fldr.Name, fldr.Description, fldr.ParentID, BOECommon.CETables.CI_INFOOBJECTS);
            return result;
        }

        public int CreatePersonalFolder(string user, string folderName, string folderDesc)
        {
            clearErr();
            int result = -1;
            int favId = _common.GetIDFromName(user, "FavoritesFolder", BOECommon.CETables.CI_INFOOBJECTS);
            if (favId > 0)
            {
                InfoObject fldr = _common.GetObjectFromName(folderName, "Folder", favId, BOECommon.CETables.CI_INFOOBJECTS);
                if (fldr == null)
                {
                    int newId = _common.CreateObject("CrystalEnterprise.Folder", folderName, folderDesc, favId, BOECommon.CETables.CI_INFOOBJECTS);
                    if (newId <= 0)
                    {
                        ErrMsg = BOECommon.ErrMsg;
                        StackTrace = BOECommon.StackTrace;
                    }
                    else
                    {
                        ErrMsg = string.Format("Personalized Folder Created for User {0}. Id: {1}", user, newId);
                        result = newId;
                    }
                }
                else
                {
                    ErrMsg = string.Format("Personalized Folder Found for User {0}. Id: {1}", user, fldr.ID);
                    result = fldr.ID;
                }
            }
            else
            {
                ErrMsg = "Favorites Folder for User:" + user + " not found";
            }
            return result;
        }

        #endregion Public_Folders

        #region Public_Reports

        public List<int> GetListIds(string filter)
        {
            clearErr();
            List<int> result = new List<int>();
            string query = string.Format("SELECT TOP 10000 SI_ID FROM CI_INFOOBJECTS WHERE SI_INSTANCE=0 AND SI_KIND ='CrystalReport'AND SI_NAME LIKE '{0}%' ORDER BY SI_ID ASC", filter);
            try
            {
                using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
                {
                    if ((iobjs != null) && (iobjs.Count > 0))
                    {
                        foreach (InfoObject iobj in iobjs)
                        {
                            result.Add(iobj.ID);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "Unable to get list of report IDs.  Err=" + ex.Message;
                StackTrace = ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// Get a list of all of the reports in a folder or all of the instances of a report.
        /// </summary>
        /// <param name="parentID">The SI_ID of the folder to search for schedules.</param>
        /// <param name="isGetChildren">Indicates whether to look at all of the reports in the parent folder.</param>
        /// <param name="rptId">The SI_ID of a specific report to look for.</param>
        /// <param name="objLimit">The maximum number of schedules to return.</param>
        /// <param name="usrFavorites"></param>
        /// <returns>The list of schedules</returns>
        public List<GenericReport> GetReportList(int parentID, bool isGetChildren, int rptId, int objLimit)
        {
            clearErr();
            List<GenericReport> result = new List<GenericReport>();
            string filter = string.Empty;
            //build the query filter
            if (parentID > 0)
            {
                filter = string.Format(" and SI_PARENTID={0}", parentID);
            }
            if (rptId > 0)
            {
                filter += string.Format(" and SI_ID={0}", rptId);
            }
            else if (rptId <= 0)
            {
                filter += string.Format(" and SI_PARENTID!={0}", _common.UserFavoritesID);
            }

            //build the query
            if (objLimit == 0) objLimit = 1000;
            string query;
            if (!isGetChildren)
            {
                query = string.Format("select TOP {0} SI_ID, SI_LAST_RUN_TIME, SI_NAME, SI_DESCRIPTION, SI_KIND, SI_PARENTID, " +
                          "SI_LAST_SUCCESSFUL_INSTANCE_ID, SI_UPDATE_TS, SI_CORPORATE_CATEGORIES " +
                          "from  CI_INFOOBJECTS where SI_INSTANCE=0 {1} order by SI_NAME", objLimit, filter);
            }
            else
            {
                query = string.Format("select SI_ID, SI_LAST_RUN_TIME, SI_NAME, SI_DESCRIPTION, SI_KIND, SI_PARENTID, SI_CHILDREN, " +
                        "SI_LAST_SUCCESSFUL_INSTANCE_ID, SI_UPDATE_TS, SI_CORPORATE_CATEGORIES " +
                        "from CI_INFOOBJECTS where SI_INSTANCE=0 {0} order by SI_UPDATE_TS desc", filter);
            }
            try
            {
                using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
                {
                    if ((iobjs != null) && (iobjs.Count > 0))
                    {
                        GenericReport rptInfo = null;
                        foreach (InfoObject iobj in iobjs)
                        {
                            rptInfo = new GenericReport(iobj);
                            //Generic constuctor sets the following properties:
                            //  Name, Description, Id, ParentId, IsExtFormat, NumberOfInstances, Categories, LastSuccessInstanceId, LastRun
                            //  so we just have to set a couple of additional props that require additional information.
                            rptInfo.IconFileName = _crystalIconFile;
                            if (isGetChildren)
                            {
                                rptInfo.NumberOfInstances = BOECommon.GetPropertyInt(iobj.Properties, "SI_CHILDREN");
                                if (rptInfo.NumberOfInstances == -1)
                                    rptInfo.NumberOfInstances = 0;
                            }
                            result.Add(rptInfo);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = string.Format("Unable to get report list for ID={0}.  Err={1}", (parentID <= 0 ? rptId : parentID), ex.Message);
                StackTrace = ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// Gets a list of reports based on the name of a report.
        /// </summary>
        /// <param name="rptName">The name to search for.</param>
        /// <param name="startId">The lowest SI_ID number to include in the report.</param>
        /// <param name="numberOfRows">The maximum number of rows to include.</param>
        /// <returns></returns>
        public List<BOEObjectInfo> GetReportListByName(string rptName, int startId, int numberOfRows)
        {
            clearErr();
            List<BOEObjectInfo> result = new List<BOEObjectInfo>();
            string query = string.Format("SELECT TOP {0} SI_ID, SI_NAME, SI_KIND, SI_PARENTID, SI_CUID, SI_DESCRIPTION from CI_INFOOBJECTS " +
                           "where SI_INSTANCE=0 and SI_KIND='CrystalReport' and SI_NAME like '{1}%' and SI_ID >= {2} order by SI_ID",
                           numberOfRows, rptName, startId);
            try
            {
                using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
                {
                    if ((iobjs != null) && (iobjs.Count > 0))
                    {
                        foreach (InfoObject iobj in iobjs)
                        {
                            result.Add(new BOEObjectInfo(iobj, _common, BOECommon.CETables.CI_INFOOBJECTS));
                        }
                    }
                    else
                    {
                        if ((iobjs == null) && (BOECommon.ErrMsg != string.Empty))
                        {
                            ErrMsg = BOECommon.ErrMsg;
                            result.Clear();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "Unable to get report list. Err=" + ex.Message;
                StackTrace = ex.StackTrace;
                result.Clear();
            }
            return result;
        }

        /// <summary>
        /// Get a list of the children of a specific parent object.
        /// </summary>
        /// <param name="parentId">The SI_ID of the parent.</param>
        /// <returns></returns>
        public List<BoxiObject> GetObjectsFromParent(int parentId)
        {
            List<BoxiObject> result = new List<BoxiObject>();
            string query = string.Format("SELECT SI_ID,SI_NAME,SI_PROGID,SI_PARENTID,SI_CHILDREN,SI_PROCESSINFO.SI_PROMPTS,SI_KIND " +
                                "FROM CI_INFOOBJECTS WHERE SI_PARENTID = {0} and SI_INSTANCE=0 AND " +
                                "SI_KIND IN ('CrystalReport','Folder') ", parentId);
            using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
            {
                if (iobjs == null)
                {
                    ErrMsg = BOECommon.ErrMsg;
                    StackTrace = BOECommon.StackTrace;
                }
                else
                {
                    foreach (InfoObject iobj in iobjs)
                    {
                        result.Add(new BoxiObject(iobj, _common));
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Get a BOERptInfo that represents a specific object.
        /// </summary>
        /// <param name="rptId"></param>
        /// <param name="fields"></param>
        /// <param name="forScheduling"></param>
        /// <returns></returns>
        public BOERptSchedInfo GetReportInfo(int rptId, string fields, bool forScheduling)
        {
            string query = string.Format("Select {0} from CI_INFOOBJECTS where SI_ID = {1}", fields, rptId);
            //NOTE:  We're not working the InfoObjects in a "Using" clause here because we don't want it
            //       to be disposed.  We need the InfoObjects in order to schedule the report in the future;
            InfoObjects iobjs = _common.ExecuteRawQuery(query);
            if (iobjs == null)
            {
                ErrMsg = BOECommon.ErrMsg;
                return null;
            }
            else
            {
                BOERptSchedInfo rpt = new BOERptSchedInfo(iobjs, _common, forScheduling);
                if (rpt.ErrMsg == string.Empty)
                {
                    return rpt;
                }
                else
                {
                    ErrMsg = rpt.ErrMsg;
                    return null;
                }
            }
        }

        public List<string[]> GetReportsForDataGrid(string docIds, bool doScheds, string personalFldrId)
        {
            clearErr();
            List<string[]> result = new List<string[]>();

            //Build the query to get the correct report objects
            string filter;
            string field;
            if (doScheds)
            {
                filter = "SI_SCHEDULE_STATUS = 9 ";
                field = "SI_PARENTID";
            }
            else
            {
                filter = "SI_INSTANCE = 0 ";
                field = "SI_ID";
            }
            string query = string.Format(
                            "SELECT SI_ID, SI_PARENTID, SI_NAME, SI_DESCRIPTION, SI_KIND, SI_INSTANCE, SI_SCHEDULE_STATUS, SI_ENDTIME, " +
                            "SI_OWNERID, SI_OWNER, SI_CUID, SI_INSTANCE FROM CI_INFOOBJECTS WHERE {0} IN ({1}) and SI_KIND!='Folder' and {2} " +
                            "ORDER BY SI_NAME", field, docIds, filter);
            using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
            {
                string lastInstQuery = "Select SI_ID, SI_ENDTIME from CI_INFOOBJECTS where SI_PARENTID = {0} and SI_INSTANCE = 1 and SI_SCHEDULE_STATUS in (1, 3) " +
                        "and SI_OWNERID = {1} {2} order by SI_ENDTIME desc";
                string schedCountQuery = "Select Count(SI_ID) from CI_INFOOBJECTS where SI_PARENTID = {0} and SI_INSTANCE = 1 and " +
                        " SI_SCHEDULE_STATUS in (9) and SI_OWNERID = {1} {2}"; //success and fail
                string personalCountQuery = "Select Count(SI_ID) from CI_INFOOBJECTS where SI_PARENTID = {0} and SI_INSTANCE = 0 and " +
                        "SI_NAME like '{1}%'";

                if (iobjs == null)  //there was an error
                {
                    ErrMsg = BOECommon.ErrMsg;
                    StackTrace = BOECommon.StackTrace;
                }
                else if (iobjs.Count <= 0) //no data found
                {
                    ErrMsg = "BOEFolderRptMgmt.GetReportsForDataGrid: No Reports Found for IDs in " + docIds;
                }
                else  //we have data
                {
                    foreach (InfoObject iobj in iobjs)
                    {
                        bool isInstance = iobj.Instance;
                        BOERptInfo rptInfo = new BOERptInfo(iobj, _common);
                        //if (doScheds)
                        //{
                        //get last run ID and its end time
                        string schedFilter = string.Empty;
                        if (isInstance)
                        {
                            schedFilter = " and SI_NAME = '" + iobj.Title + "'";
                        }
                        query = string.Format(lastInstQuery, (isInstance ? rptInfo.ParentID : rptInfo.ID), _common.CurrentUserID, schedFilter);
                        using (InfoObjects inst = _common.ExecuteRawQuery(query))
                        {
                            if (!string.IsNullOrEmpty(BOECommon.StackTrace))
                            {
                                ErrMsg = BOECommon.ErrMsg;
                                StackTrace = BOECommon.StackTrace;
                                return null;
                            }
                            else if (inst.Count > 0) //the query results are descending by end time - we only need the first one.
                            {
                                rptInfo.LastRunID = inst[1].ID;
                                rptInfo.LastRunDate = BOECommon.GetPropertyString(inst[1].Properties, "SI_ENDTIME");
                            }
                        }
                        //}
                        //else //this is a report template
                        //{
                        //get Completed/Failed instance count - Schedules
                        query = string.Format(schedCountQuery, (isInstance ? rptInfo.ParentID : rptInfo.ID), _common.CurrentUserID, schedFilter);
                        int count = _common.GetObjectCount(query, "SI_ID");
                        if (count >= 0)
                        {
                            rptInfo.RecurringInstanceCount = count;
                        }
                        else if (!string.IsNullOrEmpty(BOECommon.StackTrace))
                        {
                            ErrMsg = BOECommon.ErrMsg;
                            StackTrace = BOECommon.StackTrace;
                            return null;
                        }

                        //get Completed/Failed instance count - Personal

                        query = string.Format(personalCountQuery, personalFldrId, rptInfo.Name);
                        count = _common.GetObjectCount(query, "SI_ID");
                        if (count >= 0)
                        {
                            rptInfo.PersonalInstanceCount = count;
                        }
                        else if (!string.IsNullOrEmpty(BOECommon.StackTrace))
                        {
                            ErrMsg = BOECommon.ErrMsg;
                            StackTrace = BOECommon.StackTrace;
                            return null;
                        }
                        //}
                        string s = rptInfo.GetArrayString("|");
                        if (!string.IsNullOrEmpty(s))
                        {
                            result.Add(s.Split('|'));
                        }
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Change the owner of all documents that belong to the Old Owner.
        /// </summary>
        /// <param name="oldOwnerId"></param>
        /// <param name="newOwnerId"></param>
        /// <param name="newOwnerName"></param>
        /// <returns></returns>
        public bool ChangeReportOwners(int oldOwnerId, int newOwnerId, string newOwnerName)
        {
            bool result = true;
            string query = string.Format("Select top 100000 SI_ID, SI_NAME, SI_OWNER, SI_OWNERID from CI_INFOOBJECTS where SI_OWNERID = {0} and SI_KIND not in ('FavoritesFolder', 'Personal Category')", oldOwnerId);
            using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
            {
                if (iobjs == null)  //there was an error in the query
                {
                    ErrMsg = BOECommon.ErrMsg;
                    StackTrace = BOECommon.StackTrace;
                    result = false;
                }
                else //if there are no objects in iobjs, it's not an issue, so we don't look for it.
                {
                    foreach (InfoObject iobj in iobjs)
                    {
                        _common.SetPropertyString(iobj.Properties, "SI_OWNER", newOwnerName);
                        _common.SetPropertyInt(iobj.Properties, "SI_OWNERID", newOwnerId);
                        iobj.Save();
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Determine whether the user has access to any of the documents in the list of IDs
        /// </summary>
        /// <param name="docIds">List of document IDs to check</param>
        /// <returns></returns>
        public bool UserHasAccess(string docIds)
        {
            //NOTE: The user we're checking access for is the one who is logged in to BOECommon
            bool result = false;
            string query = string.Format("Select SI_ID from CI_INFOOBJECTS where SI_ID in ({0})", docIds);
            using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
            {
                result = (iobjs != null && iobjs.Count > 0);
                ErrMsg = BOECommon.ErrMsg;
                StackTrace = BOECommon.StackTrace;
            }
            return result;
        }

        #endregion Public_Reports

        #region Public_Personal_Reports

        public List<string[]> GetPersonalForDataGrid(int personalFolderid)
        {
            clearErr();
            string notFound = string.Empty;
            List<string[]> result = new List<string[]>();
            //Get the "master" report
            string query = "select Top 1 SI_ID, SI_PARENTID, SI_NAME, SI_DESCRIPTION, SI_KIND, SI_INSTANCE, SI_SCHEDULE_STATUS, SI_ENDTIME, " +
                            "SI_OWNERID, SI_OWNER, SI_CUID from CI_INFOOBJECTS where SI_PARENTID = {0} " +
                            "orderby SI_NAME";
            //Get the latest personal instance
            string lastInstQuery = "Select top 1 SI_ID, SI_ENDTIME from CI_INFOOBJECTS where SI_ID = {0} and SI_INSTANCE = 1 and SI_SCHEDULE_STATUS in (1, 3) ";
            //Get the personal instance count
            string instCountQuery = "Select Count(SI_ID) from CI_INFOOBJECTS where SI_ID = {0} and SI_INSTANCE = 1 and SI_SCHEDULE_STATUS in (1, 3) and SI_NAME = '{1}' "; //success and fail
            using (InfoObjects iobjs = _common.ExecuteRawQuery(string.Format(query, personalFolderid)))
            {
                if (BOECommon.StackTrace != string.Empty) //there was an error
                {
                    ErrMsg = BOECommon.ErrMsg;
                    StackTrace = BOECommon.StackTrace;
                    return null;
                }
                else
                {
                    foreach (InfoObject rptObj in iobjs)
                    {
                        BOERptInfo rptInfo = new BOERptInfo(rptObj, _common);
                        using (InfoObjects inst = _common.ExecuteRawQuery(string.Format(lastInstQuery, rptObj.ID)))
                        {
                            if (BOECommon.StackTrace != string.Empty)
                            {
                                ErrMsg = BOECommon.ErrMsg;
                                StackTrace = BOECommon.StackTrace;
                            }
                            else if (inst.Count > 0)
                            {
                                rptInfo.LastRunID = inst[1].ID;
                                rptInfo.LastRunDate = BOECommon.GetPropertyString(inst[1].Properties, "SI_ENDTIME");
                            }
                        }
                        int count = _common.GetObjectCount(string.Format(instCountQuery, rptObj.ID, _common.CurrentUserID), "SI_ID");
                        if (count >= 0)
                        {
                            rptInfo.PersonalInstanceCount = count;
                        }
                        else if (!string.IsNullOrEmpty(BOECommon.StackTrace))
                        {
                            ErrMsg = BOECommon.ErrMsg;
                            StackTrace = BOECommon.StackTrace;
                            return null;
                        }
                        //add array to result
                        string s = rptInfo.GetArrayString("|");
                        if (!string.IsNullOrEmpty(s))
                        {
                            result.Add(s.Split('|'));
                        }
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Add a personal category to a report.
        /// </summary>
        /// <param name="rptId"></param>
        /// <param name="catId"></param>
        /// <returns></returns>
        public bool AddPersonalCategory(int rptId, int catId)
        {
            clearErr();
            bool result = false;
            InfoObject doc = _common.GetObjectById(rptId, "SI_ID, SI_PERSONAL_CATEGORIES", BOECommon.CETables.CI_INFOOBJECTS);
            if (doc == null)
            {
                ErrMsg = BOECommon.ErrMsg;
                StackTrace = BOECommon.StackTrace;
            }
            else
            {
                try
                {
                    //get the personal categories assigned to the report
                    Properties cats = BOECommon.GetProperties(doc.Properties, "SI_PERSONAL_CATEGORIES");

                    if (cats == null)
                    {
                        //if the personal categories don't exist, add them and then get them
                        doc.Properties.Add("SI_PERSONAL_CATEGORIES", null, CePropFlags.cePropFlagBag);
                        cats = BOECommon.GetProperties(doc.Properties, "SI_PERSONAL_CATEGORIES");
                    }
                    int count = BOECommon.GetPropertyInt(cats, "SI_TOTAL");
                    if (count < 0)
                    {
                        count = 1;
                        cats.Add("SI_TOTAL", 0);
                    }
                    else
                        count++;
                    cats.Add(count.ToString(), catId);
                    cats["SI_TOTAL"].Value = count;
                    doc.Save();
                    result = true;
                }
                catch (Exception ex)
                {
                    ErrMsg = ex.Message;
                    StackTrace = ex.StackTrace;
                }
            }
            return result;
        }

        /// <summary>
        /// Remove a personal category from a report
        /// </summary>
        /// <param name="rptId"></param>
        /// <param name="catId"></param>
        /// <returns></returns>
        public bool RemovePersonalCategory(int rptId, int catId)
        {
            clearErr();
            bool result = false;
            InfoObject doc = _common.GetObjectById(rptId, "SI_ID, SI_PERSONAL_CATEGORIES", BOECommon.CETables.CI_INFOOBJECTS);
            if (doc == null)
            {
                ErrMsg = BOECommon.ErrMsg;
                StackTrace = BOECommon.StackTrace;
            }
            else
            {
                try
                {
                    //get the personal categories assigned to the report
                    Properties cats = BOECommon.GetProperties(doc.Properties, "SI_PERSONAL_CATEGORIES");

                    if (cats == null)
                    {
                        //if the personal categories don't exist, we're done.
                        result = false;
                        ErrMsg = string.Format("Document {0} has no personal categories.", rptId);
                    }
                    else
                    {
                        int count = BOECommon.GetPropertyInt(cats, "SI_TOTAL");
                        if (count == 0)
                        {
                            //there are no personal categories, so we're done.
                            result = false;
                            ErrMsg = string.Format("Personal Category {0} not found in document {1})", catId, rptId);
                        }
                        else
                        {
                            int id;
                            bool found = false;
                            for (int i = 1; i <= count; count++)
                            {
                                id = BOECommon.GetPropertyInt(cats, i.ToString());
                                if (id == catId) //delete the requested category
                                {
                                    found = true;
                                    cats.Delete(i.ToString());
                                }
                                else if (found)
                                {
                                    //rename the catgories "above" the requested one so there's no break in the numbers
                                    cats[i.ToString()].Name = (i - 1).ToString();
                                }
                            }
                            if (found)
                            {
                                cats["SI_TOTAL"].Value = count - 1;
                                doc.Save();
                                result = true;
                            }
                        }
                        cats.Add(count.ToString(), catId);
                        doc.Save();
                        result = true;
                    }
                }
                catch (Exception ex)
                {
                    ErrMsg = ex.Message;
                    StackTrace = ex.StackTrace;
                }
            }
            return result;
        }

        /// <summary>
        /// Get a comma-delimited list of the SI_ID values for all reports in the user's Personalized Reports folder
        /// </summary>
        /// <param name="fldrId">The SI_ID of the folder we're searching for.</param>
        /// <returns></returns>
        public string GetPersonalDocIds(string folderName)
        {
            clearErr();
            string result = string.Empty;
            int fldrId = _common.GetIDFromName(folderName, "Folder", _common.UserFavoritesID, BOECommon.CETables.CI_INFOOBJECTS);
            string query = string.Format("Select SI_ID from CI_INFOOBJECTS where SI_KIND = 'CrystalReport' and SI_PARENTID = {0}", fldrId);
            using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
            {
                if (iobjs == null)
                {
                    ErrMsg = BOECommon.ErrMsg;
                    StackTrace = BOECommon.StackTrace;
                }
                else
                {
                    //iobjs.count == 0 is a valid as not all users have personalized reports.  So we don't have to test for it here.
                    foreach (InfoObject iobj in iobjs)
                    {
                        if (result == string.Empty)
                            result = iobj.ID.ToString();
                        else
                            result += "," + iobj.ID.ToString();
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Save a personalized report to a user's personal folder
        /// </summary>
        /// <param name="oldRptName"></param>
        /// <param name="newRptName"></param>
        /// <param name="reportDescription"></param>
        /// <param name="destFormat"></param>
        /// <param name="parentId"></param>
        /// <param name="checkId"></param>
        /// <param name="prompts"></param>
        /// <returns></returns>
        public int SavePersonalRpt(string oldRptName, string newRptName, string reportDescription, string destFormat, int parentId, int checkId, List<object> prompts)
        {
            clearErr();
            int result = -1;
            InfoObject rpt = null;
            Report newRpt = null;
            InfoObjects newObjs = null;
            try
            {
                bool isSameRpt = _common.ObjectExistsByName(oldRptName, string.Format("SI_PARENTID = {0}", parentId), BOECommon.CETables.CI_INFOOBJECTS);
                rpt = _common.GetObjectFromName(oldRptName, "CrystalReport", parentId, BOECommon.CETables.CI_INFOOBJECTS);
                if (!isSameRpt)
                {
                    rpt = _common.GetObjectById(checkId, "*", BOECommon.CETables.CI_INFOOBJECTS);
                }
                else
                {
                    rpt = _common.GetObjectFromName(oldRptName, "CrystalReport", parentId, BOECommon.CETables.CI_INFOOBJECTS);
                }

                newObjs = _common.NewInfoObjects();
                newObjs.Copy(rpt, (isSameRpt ? CeCopyObject.ceCopySameObject : CeCopyObject.ceCopyNewObjectNewFiles));
                InfoObject newObj = newObjs[1];
                newObj.ParentID = parentId;
                newObj.Title = newRptName;

                newRpt = (Report)newObj;
                newRpt.Description = reportDescription;
                //Set output format
                if (destFormat != null && destFormat.Equals(STR_FORMAT_PDF, StringComparison.CurrentCultureIgnoreCase))
                {
                    newRpt.ReportFormatOptions.Format = CeReportFormat.ceFormatPDF;
                }
                else //Excel is the default output format
                {
                    newRpt.ReportFormatOptions.Format = CeReportFormat.ceFormatExcelDataOnly;
                    newRpt.ReportFormatOptions.ExcelDataOnlyFormat.UseFormat = ExcelUseDataOnlyFormat;
                    int customWidth = CustomColumnWidth;
                    if (customWidth > 0)
                    {
                        newRpt.ReportFormatOptions.ExcelDataOnlyFormat.UseConstColWidth = true;
                        newRpt.ReportFormatOptions.ExcelDataOnlyFormat.ConstColWidth = customWidth;
                    }
                }
                //Set current and default prompt values
                foreach (object obj in prompts)
                {
                    BOEPromptInfo prompt = (BOEPromptInfo)obj;
                    ReportParameter param = GetReportParameter(newRpt.ReportParameters, prompt.Name);
                    string value = "";
                    if (param != null)
                    {
                        param.CurrentValues.Clear();
                        param.DefaultValues.Clear();

                        if (!prompt.AllowMultipleValue)
                        // Single Value
                        {
                            if (prompt.AllowDiscreteValues)
                            {
                                // Discrete(not ranges)
                                if (prompt.LowerValue.Count > 0)
                                {
                                    ReportParameterValue paramValue = param.CreateSingleValue();
                                    //value = getValue(param, prompt.LowerValue[0].Value);
                                    value = HttpUtility.UrlDecode(prompt.LowerValue[0].Value);

                                    try
                                    {
                                        DateTime startdate = DateTime.MinValue;
                                        bool isdate = DateTime.TryParse(value, out startdate);

                                        if (isdate)
                                        {
                                            string res = prompt.Type.Equals(CeReportVariableValueType.ceRVDate.ToString(),
                                                                       StringComparison.CurrentCultureIgnoreCase)
                                                  ? String.Concat("Date(", startdate.Year.ToString(), ",", startdate.Month.ToString(), ",",
                                                                  startdate.Day.ToString(), ")")
                                                  : String.Concat("DateTime(", startdate.Year.ToString(), ",", startdate.Month.ToString(), ",",
                                                                  startdate.Day.ToString(), ",", startdate.Hour.ToString(), ",", startdate.Minute.ToString(), ",",
                                                                  startdate.Second.ToString(), ")");
                                            paramValue.SingleValue.Value = HttpUtility.HtmlDecode(res);
                                        }
                                        else
                                        {
                                            paramValue.SingleValue.Value = value;
                                        }
                                    }
                                    catch (ArgumentException)
                                    {
                                        paramValue.SingleValue.Value = value;
                                    }

                                    //paramValue.SingleValue.Value = value;
                                    paramValue.SingleValue.Description = prompt.LowerValue[0].Description;
                                    if (string.IsNullOrEmpty(paramValue.SingleValue.Description))
                                        paramValue.SingleValue.Description = prompt.LowerValue[0].Value;
                                    param.CurrentValues.Add(paramValue);
                                    param.DefaultValues.Add(paramValue);
                                }
                            }
                            else
                            {
                                // Ranges
                                String stringValueFrom = HttpUtility.UrlDecode(prompt.LowerValue[0].Value);
                                String stringValueTo = HttpUtility.UrlDecode(prompt.UpperValue[0].Value);
                                ReportParameterValue reportParameterValue = param.CreateRangeValue();
                                try
                                {
                                    DateTime startdate = DateTime.MinValue;
                                    bool isdate = DateTime.TryParse(stringValueFrom, out startdate);

                                    if (isdate)
                                    {
                                        string res = prompt.Type.Equals(CeReportVariableValueType.ceRVDate.ToString(),
                                                                   StringComparison.CurrentCultureIgnoreCase)
                                              ? String.Concat("Date(", startdate.Year.ToString(), ",", startdate.Month.ToString(), ",",
                                                              startdate.Day.ToString(), ")")
                                              : String.Concat("DateTime(", startdate.Year.ToString(), ",", startdate.Month.ToString(), ",",
                                                              startdate.Day.ToString(), ",", startdate.Hour.ToString(), ",", startdate.Minute.ToString(), ",",
                                                              startdate.Second.ToString(), ")");
                                        reportParameterValue.RangeValue.FromValue.Value = HttpUtility.HtmlDecode(res);
                                    }
                                    else
                                    {
                                        reportParameterValue.RangeValue.FromValue.Value = stringValueFrom;
                                    }
                                }
                                catch (ArgumentException)
                                {
                                    reportParameterValue.RangeValue.FromValue.Value = stringValueFrom;
                                }

                                try
                                {
                                    DateTime enddate = DateTime.MinValue;
                                    bool isdate = DateTime.TryParse(stringValueTo, out enddate);

                                    if (isdate)
                                    {
                                        string res = prompt.Type.Equals(CeReportVariableValueType.ceRVDate.ToString(),
                                                                   StringComparison.CurrentCultureIgnoreCase)
                                              ? String.Concat("Date(", enddate.Year.ToString(), ",", enddate.Month.ToString(), ",",
                                                              enddate.Day.ToString(), ")")
                                              : String.Concat("DateTime(", enddate.Year.ToString(), ",", enddate.Month.ToString(), ",",
                                                              enddate.Day.ToString(), ",", enddate.Hour.ToString(), ",", enddate.Minute.ToString(), ",",
                                                              enddate.Second.ToString(), ")");
                                        reportParameterValue.RangeValue.ToValue.Value = HttpUtility.HtmlDecode(res);
                                    }
                                    else
                                    {
                                        reportParameterValue.RangeValue.ToValue.Value = stringValueTo;
                                    }
                                }
                                catch (ArgumentException)
                                {
                                    reportParameterValue.RangeValue.ToValue.Value = stringValueTo;
                                }

                                //reportParameterValue.RangeValue.FromValue.Value = stringValueFrom;
                                //reportParameterValue.RangeValue.ToValue.Value = stringValueTo;
                                param.CurrentValues.Add(reportParameterValue);
                                param.DefaultValues.Add(reportParameterValue);
                            }
                        }
                        else
                        {
                            // Multiple Values
                            foreach (PromptValue v in prompt.LowerValue)
                            {
                                ReportParameterValue reportParameterValue = param.CreateSingleValue();
                                value = HttpUtility.UrlDecode(v.Value);
                                reportParameterValue.SingleValue.Value = value;
                                reportParameterValue.SingleValue.Description = v.Description;
                                if (string.IsNullOrEmpty(reportParameterValue.SingleValue.Description))
                                    reportParameterValue.SingleValue.Description = v.Value;

                                param.CurrentValues.Add(reportParameterValue);
                                param.DefaultValues.Add(reportParameterValue);
                            }
                        }
                    }
                }
                newRpt.Save();
                result = newRpt.ID;
            }
            catch (Exception ex)
            {
                ErrMsg = ex.Message;
                StackTrace = ex.StackTrace;
            }
            finally
            {
                if (newObjs != null)
                {
                    newObjs.Dispose();
                }

                if (rpt != null)
                    rpt.Dispose();
            }
            return result;
        }

        #endregion Public_Personal_Reports

        #region Public_Schedules

        /// <summary>
        /// Delete all of the schedules that have been created by the specified user.
        /// </summary>
        /// <param name="userName">The user ID of the user whose schedules will be deleted.</param>
        /// <returns>True if successful.</returns>
        public bool DeleteUserSchedules(string userName)
        {
            clearErr();
            int usrId = _common.GetIDFromName(userName, "User", BOECommon.CETables.CI_SYSTEMOBJECTS);
            if (usrId > 0)
            {
                return DeleteUserInstances(usrId);
            }
            else
            {
                if (BOECommon.ErrMsg == string.Empty)
                {
                    ErrMsg = "Unable to delete schedules for " + userName + ". User not found.";
                }
                else
                {
                    ErrMsg = String.Format("Unable to delete schedules for {0}. Err={1}", userName, BOECommon.ErrMsg);
                }
            }
            return false;
        }

        /// <summary>
        /// Delete all of the schedules that have been created by the specified user.
        /// </summary>
        /// <param name="userId">The SI_ID of the user whose schedules will be deleted.</param>
        /// <returns>True if successful.</returns>
        public bool DeleteUserInstances(int userId)
        {
            clearErr();
            string query = string.Format("select top 10000 SI_ID, SI_NAME from CI_INFOOBJECTS where SI_INSTANCE = 1 and SI_OWNERID = {0}", userId);
            try
            {
                using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
                {
                    if ((iobjs != null) && (iobjs.Count > 0))
                    {
                        int count = iobjs.Count;
                        for (int i = iobjs.Count; i > 0; i--)
                        {
                            iobjs.Delete(iobjs[i]);
                        }
                        if (_common.CommitObjects(iobjs))
                        {
                            return true;
                        }
                        else
                        {
                            ErrMsg = BOECommon.ErrMsg;
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = string.Format("Unable to delete schedules for {0}. Err={1}", userId, ex.Message);
                StackTrace = ex.StackTrace;
                return false;
            }
            return true;
        }

        /// <summary>
        /// Get the Error Message (if any) from an instance.
        /// </summary>
        /// <param name="id">The SI_ID of the instance to look at.</param>
        /// <returns></returns>
        public string GetInstanceError(int id)
        {
            clearErr();
            string result = string.Empty;
            string query = string.Format("SELECT SI_SCHEDULEINFO,SI_STATUSINFO,SI_PROCESSINFO,SI_SCHEDULE_STATUS FROM CI_INFOOBJECTS where SI_ID={0}", id);
            try
            {
                using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
                {
                    if ((iobjs != null) && (iobjs.Count > 0))
                    {
                        result = iobjs[1].SchedulingInfo.ErrorMessage;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = ex.Message;
                StackTrace = ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// Get a list of either recurring or completed instances.
        /// </summary>
        /// <param name="parentId">The SI_ID of the report to find instances of.</param>
        /// <param name="isRecurring">Flag indicating whether to get recurring or non-recurring instances.</param>
        /// <returns></returns>
        public List<BOEInstanceInfo> GetInstanceList(int parentId, bool isRecurring)
        {
            clearErr();
            List<BOEInstanceInfo> result = new List<BOEInstanceInfo>();

            string query = string.Format("SELECT SI_ID, SI_SCHEDULE_STATUS,SI_ENDTIME,SI_NAME,SI_PROCESSINFO.SI_PROMPTS," +
                                         "SI_PROGID, SI_STATUSINFO,SI_KIND FROM CI_INFOOBJECTS WHERE SI_INSTANCE=1 and " +
                                         "SI_RECURRING = {0} and SI_PARENTID = {1} and SI_SCHEDULE_STATUS in (1, 3)", (isRecurring ? 1 : 0), parentId);

            try
            {
                using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
                {
                    if ((iobjs != null) && (iobjs.Count > 0))
                    {
                        foreach (InfoObject iobj in iobjs)
                        {
                            result.Add(new BOEInstanceInfo(iobj, _common));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "Unable to get instance list.  Err= " + ex;
                StackTrace = ex.StackTrace;
            }

            return result;
        }

        public List<BOEInstanceInfo> GetInstanceList(int parentId, string rptName)
        {
            clearErr();
            List<BOEInstanceInfo> result = new List<BOEInstanceInfo>();

            string query = string.Format("SELECT SI_ID, SI_SCHEDULE_STATUS,SI_ENDTIME,SI_NAME,SI_PROCESSINFO.SI_PROMPTS," +
                                         "SI_PROGID, SI_STATUSINFO,SI_KIND FROM CI_INFOOBJECTS WHERE SI_INSTANCE=1 and " +
                                         "SI_PARENTID = {0} and SI_NAME = '{1}' and SI_SCHEDULE_STATUS in (1, 3)", parentId, rptName);
            try
            {
                using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
                {
                    if ((iobjs != null) && (iobjs.Count > 0))
                    {
                        foreach (InfoObject iobj in iobjs)
                        {
                            result.Add(new BOEInstanceInfo(iobj, _common));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "Unable to get instance list.  Err= " + ex;
                StackTrace = ex.StackTrace;
            }

            return result;
        }

        /// <summary>
        /// Get a list of all of the schedules for a specific report.
        /// </summary>
        /// <param name="parentId">The SI_ID of the report whose schedules will be returned.</param>
        /// <returns>True if successful.</returns>
        public List<RecurringReport> GetScheduleList(int parentId, string recurringPropName, string frequencyPropName)
        {
            clearErr();
            List<RecurringReport> result = new List<RecurringReport>();
            string query = string.Format("select SI_DESCRIPTION, SI_CORPORATE_CATEGORIES,SI_ID, SI_LAST_RUN_TIME, SI_SCHEDULE_STATUS," +
                            "SI_ENDTIME, SI_NAME, SI_PROCESSINFO.SI_PROMPTS, SI_STATUSINFO, SI_KIND, SI_UPDATE_TS, SI_CHILDREN, " +
                            "SI_LAST_SUCCESSFUL_INSTANCE_ID, SI_CORPORATE_CATEGORIES, {0}, {1} " +
                            "from CI_INFOOBJECTS where SI_INSTANCE=1 AND SI_RECURRING=1 AND SI_PARENTID = {2} " +
                            " order by SI_UPDATE_TS DESC",
                            recurringPropName, frequencyPropName, parentId);

            RecurringReport rptInfo = null;
            using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
            {
                foreach (InfoObject iobj in iobjs)
                {
                    rptInfo = new RecurringReport(iobj, recurringPropName, frequencyPropName);
                    //RecurringReport constuctor sets the following properties:
                    //  Name, Description, Id, ParentId, IsExtFormat, NumberOfInstances, Categories, RecurringParentId, Frequency
                    //  so we just have to set a couple of additional props that require CMS queries.

                    if (recurringPropName != string.Empty)
                    {
                        rptInfo.CategoryIconFilename = _crystalIconFile;
                        getLastSuccessRecurringInstanceInfo(iobj, recurringPropName, rptInfo);
                        rptInfo.NumberOfInstances = _common.GetObjectCount(
                                                    String.Format("select count(SI_ID) from CI_INFOOBJECTS where where SI_RECURRING=0 " +
                                                          "AND SI_INSTANCE=1 and SI_PARENTID={0} and {1}={2}",
                                                          rptInfo.ParentId, recurringPropName, rptInfo.RecurringParentId), "SI_ID");
                    }
                    result.Add(rptInfo);
                }
            }
            return result;
        }

        /// <summary>
        /// Schedule a report to run "now" with the default parameter values.
        /// </summary>
        /// <param name="rptId">The ID of the report to schedule.</param>
        /// <returns>The SI_ID of the new instance.  -1 if there's a query error, 0 if there's a scheduling error.</returns>
        public int ScheduleReportDefault(int rptId)
        {
            clearErr();
            int result = -1;
            string query = string.Format("Select * from CI_INFOOBJECTS where SI_ID = {0}", rptId);
            using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
            {
                if (iobjs == null) //there was an error in the query
                {
                    ErrMsg = BOECommon.ErrMsg;
                }
                else if (iobjs.Count <= 0) //nothing was returned
                {
                    ErrMsg = String.Format("Report with ID: {0} Not Found", rptId);
                }
                else //we have a report, so schedule it
                {
                    SchedulingInfo si = iobjs[1].SchedulingInfo;
                    si.RightNow = true;
                    _common.BOEInfoStore.Schedule(iobjs);
                    CeScheduleOutcome so = iobjs[1].SchedulingInfo.Outcome;
                    if ((so == CeScheduleOutcome.ceOutcomeSuccess) || (so == CeScheduleOutcome.ceOutcomePending))
                    {
                        result = BOECommon.GetPropertyInt(iobjs[1].Properties, "SI_NEW_JOB_ID");
                        if (result == -1)
                            result = 0;
                    }
                    else
                    {
                        result = 0;
                        ErrMsg = getScheduleErrorMessage(so);
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Update the value of one or more custom properties in a report.
        /// </summary>
        /// <param name="rptId">The SI_ID of the report being updated.</param>
        /// <param name="props">A list of Name/Value pairs - one for each property being updated.</param>
        /// <returns></returns>
        public bool UpdateCustomProperties(int rptId, List<PromptValue> props)
        {
            try
            {
                string query = string.Format("Select * from CI_INFOOBJECTS where SI_ID={0}", rptId);
                using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
                {
                    if ((iobjs != null) && (iobjs.Count > 0))
                    {
                        using (InfoObject iobj = iobjs[1])
                        {
                            foreach (PromptValue prop in props)
                            {
                                string tmp = BOECommon.GetPropertyString(iobj.Properties, prop.Description);
                                if (BOECommon.ErrMsg == string.Empty)
                                {
                                }
                                else
                                {
                                    iobj.Properties.Add(prop.Description, prop.Value);
                                }
                            }
                            try
                            {
                                iobj.Save();
                                return true;
                            }
                            catch (Exception ex)
                            {
                                ErrMsg = "BOEFolderRptMgmt.UpdateCustomProperties: Unable to save properties.  Err=" + ex.Message;
                                StackTrace = ex.StackTrace;
                            }
                        }
                    }
                    else
                    {
                        if (iobjs == null)
                        {
                            ErrMsg = "BOEFolderRptMgmt.UpdateCustomProperties: " + BOECommon.ErrMsg;
                        }
                        else
                        {
                            ErrMsg = string.Format("BOEFolderRptMgmt.UpdateCustomProperties: Object with Id: {0} Not Found!", rptId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "BOEFolderRptMgmt.UpdateCustomProperties: " + ex.Message;
                StackTrace = ex.StackTrace;
            }
            return false;
        }

        /// <summary>
        /// Identifies whether an instance has finished running.
        /// </summary>
        /// <param name="instanceId">The SI_ID of the instance.</param>
        /// <param name="timeout">Time between each status check query.</param>
        /// <param name="limit">The maximum number status check queries to run.</param>
        /// <returns>The status of the report instance.</returns>
        public int IsInstanceFinished(int instanceId, int timeout, int limit)
        {
            int result = C_INT_REPORT_SUCCESS_FAILURE_UNDERTERMINED;
            string query = string.Format("SELECT SI_SCHEDULEINFO,SI_STATUSINFO,SI_PROCESSINFO,SI_SCHEDULE_STATUS FROM CI_INFOOBJECTS where SI_ID = {0}", instanceId);

            for (int i = 1; i <= limit; i++)
            {
                using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
                {
                    if ((iobjs != null) && (iobjs.Count > 0))
                    {
                        switch (iobjs[1].SchedulingInfo.Status)
                        {
                            case CeScheduleStatus.ceStatusSuccess:
                                result = C_INT_REPORT_SUCCESS;
                                break;

                            case CeScheduleStatus.ceStatusFailure:
                                result = C_INT_REPORT_FAILED;
                                break;

                            default:
                                result = C_INT_REPORT_SUCCESS_FAILURE_UNDERTERMINED;
                                break;
                        }
                    }
                    else
                    {
                        if (iobjs == null)
                        {
                            ErrMsg = BOECommon.ErrMsg;
                        }
                        else
                        {
                            ErrMsg = string.Format("BOEFolderRptMgmt.IsInstanceFinished: Unable to find instance with id {0}", instanceId);
                        }
                        result = C_INT_METHOD_FAIL;
                        break;
                    }
                }
                if (result != C_INT_REPORT_SUCCESS_FAILURE_UNDERTERMINED) break;
                Thread.Sleep(timeout);
            }
            return result;
        }

        /// <summary>
        /// Get the begin date and the schedule type of a report schedule.
        /// </summary>
        /// <param name="instanceId">The SI_ID of the instance to search for.</param>
        /// <returns>A BOEScheduleInfo that contains the begin date and schedule type.</returns>
        public BOEScheduleInfo GetScheduleInfo(int instanceId)
        {
            BOEScheduleInfo result = null;
            using (InfoObject iobj = _common.GetObjectById(instanceId, "*", BOECommon.CETables.CI_INFOOBJECTS))
            {
                if ((iobj != null) && (BOECommon.ErrMsg == string.Empty))
                {
                    result = new BOEScheduleInfo(iobj.SchedulingInfo);
                }
                else
                {
                    if (BOECommon.ErrMsg != string.Empty)
                        ErrMsg = BOECommon.ErrMsg;
                    else
                        ErrMsg = string.Format("Unable to find instance with ID {0}", instanceId);
                    result = null;
                }
            }

            return result;
        }

        /// <summary>
        /// Get the SI_ID most recent successful instance of a report.
        /// </summary>
        /// <param name="parentID">The SI_ID of the report template.</param>
        /// <param name="recurring">Boolean indicating whether to only look for recurring reports.</param>
        /// <param name="customIDFieldName">The name of the custom field to use when searching for a recurring instance.</param>
        /// <returns></returns>
        public int GetLatestSuccessInstanceID(int parentID, bool recurring, string customIDFieldName)
        {
            clearErr();
            string query = "SELECT TOP 1 SI_ID FROM CI_INFOOBJECTS WHERE SI_RECURRING=0 AND SI_INSTANCE=1 {0}";
            string filter = string.Empty;
            int result = -1;
            if (recurring && !string.IsNullOrEmpty(customIDFieldName))
            {
                filter = string.Format("and {0} = {1}", customIDFieldName, parentID);
            }
            else
            {
                filter = string.Format(" and SI_PARENTID = {0}", parentID);
            }

            using (InfoObjects iobjs = _common.ExecuteRawQuery(string.Format(query, filter)))
            {
                if ((iobjs != null) && (BOECommon.ErrMsg != string.Empty))
                {
                    result = iobjs[1].ID;
                }
                else
                {
                    if (BOECommon.ErrMsg != string.Empty)
                    {
                        ErrMsg = BOECommon.ErrMsg;
                        StackTrace = BOECommon.StackTrace;
                    }
                    else
                    {
                        ErrMsg = "No instances found";
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Get the SI_ID and SI_PARENTID values for all schedules "owned" by a specific user.
        /// </summary>
        /// <param name="ownerID">The ID of the owner</param>
        /// <returns>Comma-delimited string of ID and ParentID values.</returns>
        public string GetScheduledDocIDs(string ownerID)
        {
            clearErr();
            List<string> result = new List<string>();
            string query = string.Format("SELECT SI_ID,SI_PARENTID FROM CI_INFOOBJECTS WHERE SI_SCHEDULE_STATUS=9 AND SI_OWNERID='{0}'", ownerID);
            using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
            {
                if ((iobjs != null) && (iobjs.Count > 0))
                {
                    foreach (InfoObject iobj in iobjs)
                    {
                        result.Add(string.Format("{0},{1}", iobj.ID, iobj.ParentID));
                    }
                }
                else
                {
                    if (iobjs == null) //there was an error
                    {
                        ErrMsg = BOECommon.ErrMsg;
                        StackTrace = BOECommon.StackTrace;
                    }
                    else  //Objects not found
                    {
                        ErrMsg = "BOEFolderRptMgmt.GetScheduledDocIDs: No schedules found";
                    }
                }
            }
            return string.Join(",", result.ToArray());
        }

        public string GetScheduledDocIDsForARpt(int ownerId, string rptName)
        {
            clearErr();
            List<string> result = new List<string>();
            string query = string.Format("SELECT SI_ID,SI_PARENTID FROM CI_INFOOBJECTS WHERE SI_SCHEDULE_STATUS=9 AND SI_OWNERID={0} AND SI_NAME='{1}'", ownerId, rptName);
            using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
            {
                if ((iobjs != null) && (iobjs.Count > 0))
                {
                    foreach (InfoObject iobj in iobjs)
                    {
                        string chkName = string.Empty;
                        if (!iobj.Title.Contains(rptName))
                        {
                            chkName = _common.GetNameFromID(iobj.ParentID, BOECommon.CETables.CI_INFOOBJECTS);
                            if (chkName.Contains(rptName))
                            {
                                result.Add(string.Format("{0},{1}", iobj.ID, iobj.ParentID));
                            }
                        }
                        else
                        {
                            result.Add(string.Format("{0},{1}", iobj.ID, iobj.ParentID));
                        }
                    }
                }
                else
                {
                    if (iobjs == null) //there was an error
                    {
                        ErrMsg = BOECommon.ErrMsg;
                        StackTrace = BOECommon.StackTrace;
                    }
                    else  //Objects not found
                    {
                        ErrMsg = "BOEFolderRptMgmt.GetScheduledDocIDs: No schedules found";
                    }
                }
            }
            return string.Join(",", result.ToArray());
        }

        #endregion Public_Schedules

        #region Public_Categories

        /// <summary>
        /// Get a list of category IDs and names.
        /// </summary>
        /// <returns>A List of the category information.  Each category is stored in an array with the following data:
        /// [0] SI_ID
        /// [1] Title/Name
        /// [2] Description</returns>
        public List<string[]> GetCategoryList()
        {
            clearErr();
            List<string[]> result = new List<string[]>();
            string query = "Select SI_ID, SI_NAME, SI_DESCRIPTION from CI_INFOOBJECTS where SI_KIND = 'Category' order by SI_NAME";
            List<string> details = new List<string>();
            try
            {
                using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
                {
                    if ((iobjs != null) && (iobjs.Count > 0))
                    {
                        foreach (InfoObject iobj in iobjs)
                        {
                            details.Add(iobj.ID.ToString());
                            details.Add(iobj.Title);
                            details.Add(iobj.Description);
                            result.Add(details.ToArray());
                            details.Clear();
                        }
                    }
                    else
                    {
                        if (iobjs == null) //there was an error
                        {
                            ErrMsg = "Unable to get categories.  Err=" + BOECommon.ErrMsg;
                            StackTrace = BOECommon.StackTrace;
                        }
                        else
                        {
                            ErrMsg = "No Categories found.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "Unable to get Categories.  Err=" + ex.Message;
                StackTrace = ex.StackTrace;
                result = null;
            }
            return result;
        }

        public List<string> GetCategoryIds()
        {
            clearErr();
            List<string> result = new List<string>();
            string query = "Select SI_ID from CI_INFOOBJECTS where SI_KIND = 'Category' order by SI_NAME";
            List<string> details = new List<string>();
            try
            {
                using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
                {
                    if ((iobjs != null) && (iobjs.Count > 0))
                    {
                        foreach (InfoObject iobj in iobjs)
                        {
                            result.Add(iobj.ID.ToString());
                        }
                    }
                    else
                    {
                        if (iobjs == null) //there was an error
                        {
                            ErrMsg = "BOEFolderRptMgmt.GetCategoryIDs:  Err=" + BOECommon.ErrMsg;
                            StackTrace = BOECommon.StackTrace;
                        }
                        else
                        {
                            ErrMsg = "BOEFolderRptMgmt.GetCategoryIDs: No Categories found.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "Unable to get Categories.  Err=" + ex.Message;
                StackTrace = ex.StackTrace;
                result = null;
            }
            return result;
        }

        /// <summary>
        /// Get the list of reports contained in one or more categories
        /// </summary>
        /// <param name="categories">The list of categories to look at.
        /// The SI_ID is in the first element of the category array.</param>
        /// <returns></returns>
        public Dictionary<int, string[]> GetCategoryRptInfo()
        {
            clearErr();
            Dictionary<int, string[]> result = new Dictionary<int, string[]>();
            try
            {
                string query = "Select SI_ID, SI_NAME, SI_DESCRIPTION, SI_DOCUMENTS from CI_INFOOBJECTS where SI_KIND = 'Category' order by SI_NAME";
                using (InfoObjects cats = _common.ExecuteRawQuery(query))
                {
                    if (cats == null)
                    {
                        ErrMsg = BOECommon.ErrMsg;
                        StackTrace = BOECommon.StackTrace;
                    }
                    else if (cats.Count == 0)
                    {
                        ErrMsg = "BOEFolderRptMgmt.GetCategoryRptInfo: No categories found";
                    }
                    else
                    {
                        string docIds = string.Empty;
                        foreach (InfoObject cat in cats)
                        {
                            docIds = GetCategoryDocIds(cat.ID);
                            string catInfo = string.Format("{0}|{1}|{2}", cat.ID, cat.Title, cat.Description);
                            if (docIds != string.Empty)
                            {
                                query = string.Format("Select SI_ID, SI_NAME from CI_INFOOBJECTS where SI_KIND = 'CrystalReport' and SI_ID in ({0}) order by SI_NAME", docIds);
                                using (InfoObjects rpts = _common.ExecuteRawQuery(query))
                                {
                                    if (rpts == null)
                                    {
                                        ErrMsg = BOECommon.ErrMsg;
                                        StackTrace = BOECommon.StackTrace;
                                        break;
                                    }
                                    else
                                    {
                                        foreach (InfoObject rpt in rpts)
                                        {
                                            catInfo += "|" + rpt.Title;
                                        }
                                    }
                                }
                            }
                            result.Add(cat.ID, catInfo.Split('|'));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "BOEFolderRptMgmt.GetCategoryRptInfo: Unable to get reports in categories.  Err=" + ex.Message;
                StackTrace = ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// Get the SI_ID value for all of the documents in a category
        /// </summary>
        /// <param name="catId">The SI_ID of the category.</param>
        /// <returns>Comma-delimited string with the IDs.  Empty string if there are none of if there's an error.</returns>
        public string GetCategoryDocIds(int catId)
        {
            clearErr();
            string result = string.Empty;
            string query = string.Format("SELECT SI_ID, SI_NAME, SI_DESCRIPTION, SI_DOCUMENTS FROM CI_INFOOBJECTS WHERE SI_KIND='Category' and SI_ID={0}", catId);
            using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
            {
                if ((iobjs != null) && (iobjs.Count > 0))
                {
                    Properties docs = BOECommon.GetProperties(iobjs[1].Properties, "SI_DOCUMENTS");
                    int docCount = BOECommon.GetPropertyInt(docs, "SI_TOTAL");
                    if (docCount > 0)
                    {
                        for (int i = 1; i <= docCount; i++)
                        {
                            string id = BOECommon.GetPropertyString(docs, i.ToString());
                            if (result == string.Empty)
                            {
                                result = id;
                            }
                            else
                            {
                                if (id != string.Empty)
                                {
                                    result += "," + id;
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (iobjs == null)
                    {
                        ErrMsg = BOECommon.ErrMsg;
                        StackTrace = BOECommon.StackTrace;
                    }
                    else
                    {
                        ErrMsg = "BOEFolderRptMgmt.GetCategoryDocIds: Unable to find Category with SI_ID=" + catId;
                    }
                }
            }
            return result;
        }

        #endregion Public_Categories

        #region Private_Methods

        /// <summary>
        /// Set ErrMsg and StackTrace to empty strings.
        /// </summary>
        private void clearErr()
        {
            ErrMsg = string.Empty;
            StackTrace = string.Empty;
        }

        /// <summary>
        /// Search for a named subfolder within a folder.  If it exists and its SI_ID is not the same as checkId, delete it and, consequently, its contents.
        /// </summary>
        /// <param name="checkId">The SI_ID of the folder we're checking against.</param>
        /// <param name="parentId">The SI_ID of the folder's parent.</param>
        /// <param name="fldrName">The name of the folder to search for.</param>
        /// <returns>True if successful or if the folder does not exist.</returns>
        private bool deleteExistingFolder(int checkId, int parentId, string fldrName)
        {
            bool result = true;
            string query = string.Format("select SI_ID, SI_NAME from CI_INFOOBJECTS where SI_KIND = 'Folder' where SI_NAME = '{0}' and SI_PARENTID = {1} and SI_ID != {2}", fldrName, parentId, checkId);
            using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
            {
                if ((iobjs != null) && (iobjs.Count > 0))
                {
                    iobjs.Delete(1);
                    if (!_common.CommitObjects(iobjs))
                    {
                        ErrMsg = string.Format("Unable to delete existing folder named {0}. Err={1}", fldrName, BOECommon.ErrMsg);
                        result = false;
                    }
                }
                else
                {
                    if (iobjs == null)
                    {
                        if (BOECommon.ErrMsg != string.Empty)
                        {
                            ErrMsg = string.Format("Unable to delete existing folder named {0}. Err={1}", fldrName, BOECommon.ErrMsg);
                            result = false;
                        }
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Get the ID and EndTime of the most recent recurring instance of a report schedule.
        /// </summary>
        /// <param name="rpt">The InfoObject which contains the report schedule.</param>
        /// <param name="recurringPropName">The name of the APOS recurring report field</param>
        /// <param name="rptInfo">The RecurringReport object that will be updated with the ID and EndTime.</param>
        private void getLastSuccessRecurringInstanceInfo(InfoObject rpt, string recurringPropName, RecurringReport rptInfo)
        {
            int apos_id = int.Parse(rpt.Properties[recurringPropName].ToString());
            string query = string.Format("select top 1 SI_ID,SI_ENDTIME from CI_INFOOBJECTS where SI_NAME = '{0}' and " +
                                "{1} = {2} AND SI_RECURRING=0 order by SI_ENDTIME DESC",
                                rpt.Title, recurringPropName, apos_id);
            using (InfoObjects iobjs = _common.ExecuteRawQuery(query))
            {
                if ((iobjs != null) && (iobjs.Count > 0))
                {
                    rptInfo.LastRun = BOECommon.GetPropertyDateTime(iobjs[1].Properties, "SI_ENDTIME");
                    rptInfo.Id = iobjs[1].ID;
                }
                //else - there are no instances, so we don't worry about it.
            }
        }

        /// <summary>
        /// Get Schedule OutCome Error Message
        /// </summary>
        /// <param name="outcome">The schedule outcome to translate into an error message.</param>
        /// <returns>The error message.</returns>
        private string getScheduleErrorMessage(CeScheduleOutcome outcome)
        {
            String errorMessage = "Scheduling "
                                  + " failed with code: ";
            switch (outcome)
            {
                case CeScheduleOutcome.ceOutcomeFailComponentFailed:
                    errorMessage = errorMessage
                                   + "FAIL_COMPONENT_FAILED";
                    break;

                case CeScheduleOutcome.ceOutcomeFailEndTime:
                    errorMessage = errorMessage + "FAIL_ENDTIME";
                    break;

                case CeScheduleOutcome.ceOutcomeFailJobServer:
                    errorMessage = errorMessage + "FAIL_JOBSERVER";
                    break;

                case CeScheduleOutcome.ceOutcomeFailJobServerChild:
                    errorMessage = errorMessage
                                   + "FAIL_JOBSERVER_CHILD";
                    break;

                case CeScheduleOutcome.ceOutcomeFailJobServerPlugin:
                    errorMessage = errorMessage
                                   + "FAIL_JOBSERVER_PLUGIN";
                    break;

                case CeScheduleOutcome.ceOutcomeFailObjectPackageFailed:
                    errorMessage = errorMessage
                                   + "FAIL_OBJECT_PACKAGE_FAILED";
                    break;

                case CeScheduleOutcome.ceOutcomeFailSchedule:
                    errorMessage = errorMessage + "FAIL_SCHEDULE";
                    break;

                case CeScheduleOutcome.ceOutcomeFailSecurity:
                    errorMessage = errorMessage + "FAIL_SECURITY";
                    break;
            }

            return errorMessage;
        }

        private ReportParameter GetReportParameter(ReportParameters reportParameters, String parameterName)
        {
            ReportParameter reportParameter = null;
            foreach (ReportParameter parameter in reportParameters)
            {
                if (parameter.ParameterName.Equals(parameterName))
                {
                    reportParameter = parameter;
                    break;
                }
            }
            return reportParameter;
        }

        private string getValue(ReportParameter param, string value)
        {
            string result = string.Empty;
            CrystalDecisions.Enterprise.Desktop.CeReportVariableValueType valType = param.ValueType;
            if (valType == CrystalDecisions.Enterprise.Desktop.CeReportVariableValueType.ceRVDateTime)
            {
                if (value.Contains('/') && !value.Contains(':'))  //this is a date with no time on it...
                {
                    result = value + " 12:00:00 AM";
                }
                else
                {
                    result = value;
                }
            }
            else if (valType == CrystalDecisions.Enterprise.Desktop.CeReportVariableValueType.ceRVTime)
            {
            }
            else
            {
                result = value;
            }
            return result;
        }

        #endregion Private_Methods
    }
}