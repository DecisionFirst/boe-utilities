﻿using CrystalDecisions.Enterprise;

namespace DFT.BOE
{
    /// <summary>
    /// Represents Recurring Instances
    /// Moved over from IHPReportingInterface.Classes for BO 4.1 upgrade by Dell Stinnett-Christy 6/18/2014
    /// </summary>
    public class RecurringReport : BoxiReport
    {
        /// <summary>
        /// Daily, Weekly, Montnhly
        /// </summary>
        public string Frequency { get; set; }

        /// <summary>
        /// Custom Property Values to find children of recurring parent
        /// </summary>
        public int RecurringParentId { get; set; }

        /// <summary>
        /// JH 5/25/11
        /// </summary>
        public string CategoryIconFilename { get; set; }

        /// <summary>
        /// Create a RecurringReport based on an InfoObject.
        /// Dell Stinnett-Christy 6/22/2014
        /// </summary>
        /// <param name="rpt">The InfoObject that contains the report's information.</param>
        /// <param name="recurringPropName">The name of the APOS Recurring Parent field.</param>
        /// <param name="frequencyPropName">The name of the APOS Frequency field.</param>
        public RecurringReport(InfoObject rpt, string recurringPropName, string frequencyPropName)
            : base(rpt)
        {
            try
            {
                RecurringParentId = (int)rpt.Properties[recurringPropName].Value;
                Frequency = rpt.Properties[frequencyPropName].ToString();
            }
            catch
            {
                //eat the error - these properties don't exist.
            }
        }
    }
}