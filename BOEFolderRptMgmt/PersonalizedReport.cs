﻿using System;
using System.Collections.Generic;

namespace DFT.BOE
{
    /// <summary>
    /// Personalized Report
    /// Moved over from IHPReportingInterface.Classes for BO 4.1 upgrade by Dell Stinnett-Christy 6/18/2014
    /// </summary>
    public class PersonalizedReport : BoxiReport
    {
        /// <summary>
        /// JH 5/25/11
        /// </summary>
        public string CategoryIconFilename { get; set; }

        public int ParentRptId { get; set; }

        public List<BOEPromptInfo> Prompts { get; set; }

        public string Format { get; set; }

        public PersonalizedReport(string rptName, string format, string parentCuid, int id, BOECommon common)
            : base()
        {
            this.Name = rptName;
            this.CUID = parentCuid;
            this.Id = id;
            this.IsExtFormat = (format != "CrystalReport");
            this.Format = format;
            Prompts = new List<BOEPromptInfo>();
        }

        public PersonalizedReport(string[] rpt)
            : base()
        {
            Id = Convert.ToInt32(rpt[0]);
            ParentId = Convert.ToInt32(rpt[1]);
            Name = rpt[2];
            Description = rpt[3];
            Category = rpt[10];
        }
    }
}