﻿//using System.Web.UI;
using CrystalDecisions.Enterprise.Desktop;
using System;
using System.Collections.Generic;

namespace DFT.BOE
{
    public enum BOEPromptFormat
    {
        Single,
        Multiple,
        Range
    }

    /// <summary>
    /// Represents the Report Prompt
    /// </summary>
    public class BOEPromptInfo
    {
        private const string STR_EMPTY_VALUE = "Empty";

        public bool AllowCustomValues { get; set; }
        public bool AllowDiscreteValues { get; set; }
        public bool AllowMultipleValue { get; set; } // Multiple value parameter flag (from BOE)
        public bool AllowNullValue { get; set; }  //new for BI 4.1 upgrade - can a paramter be left blank.
        public int Id { get; set; } // Prompt ID (Sequential)
        public bool IsEnableDescriptionOnly { get; set; }
        public bool IsProccess { get; set; } // Completly Ignore Parameter if it is False
        public bool IsPromptUserForPrompt { get; set; } // Process Parameter but do not prompt (ex for system varaiables)
        public BOEPromptFormat PromptFormat { get; set; }

        public List<PromptValue> LowerValueCurrent { get; set; } // BOE Current Value
        public List<PromptValue> LowerValueDefault { get; set; } // BOE Default Value
        public List<PromptValue> UpperValueCurrent { get; set; } // BOE Current Value
        public List<PromptValue> UpperValueDefault { get; set; } // BOE Default Value

        public string Name { get; set; } // Prompt Name
        public string Guid { get; set; } // GUID
        public string PromptText { get; set; } // Text from BOE
        public int ReportId { get; set; } // from BOE
        public string ReportName { get; set; } // from BOE
        public string Type { get; set; } // Type from BOE

        public bool IsSystemPrompt { get; set; }
        public List<PromptValue> LowerValue { get; set; } // BOE Current Value
        public List<PromptValue> UpperValue { get; set; } // BOE Current Value
        public bool IsFreeForm { get; set; } // Parameter is in the Free From - does not exist in the database - only in report
        public string LovSql { get; set; } // List of Values SQL statement
        public string InititialValueSql { get; set; } // List of Values SQL statement
        public int LovLimit { get; set; } // List of Values Limit
        public bool IsLovSqlStoredProcedure { get; set; }
        public bool IsInitSqlStoredProcedure { get; set; }
        public bool IsTextBox { get; set; }
        public bool IsEnableLoadOnDemand { get; set; }

        public bool IsDependency { get; set; }
        public List<BOEPromptInfo> initLovDependancy { get; set; }
        public List<BOEPromptInfo> lovDependancy { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BOEPromptInfo"/> class.
        /// </summary>
        public BOEPromptInfo()
        {
            Id = 0;
            Name = String.Empty;
            AllowCustomValues = false;
            AllowDiscreteValues = true;
            Type = String.Empty;
            IsProccess = false;
            IsPromptUserForPrompt = true;
            PromptText = String.Empty;
            ReportName = "N/A";
            AllowMultipleValue = false;
            AllowNullValue = false;

            LowerValueCurrent = new List<PromptValue>();
            LowerValueDefault = new List<PromptValue>();

            UpperValueCurrent = new List<PromptValue>();
            UpperValueDefault = new List<PromptValue>();

            ReportId = 0;

            IsSystemPrompt = false;
            LowerValue = new List<PromptValue>();
            UpperValue = new List<PromptValue>();
            IsFreeForm = false;
            LovSql = String.Empty;
            InititialValueSql = String.Empty;
            LovLimit = 0;
            IsLovSqlStoredProcedure = false;
            IsInitSqlStoredProcedure = false;
            IsTextBox = false;
            IsEnableLoadOnDemand = false;

            initLovDependancy = new List<BOEPromptInfo>();
            lovDependancy = new List<BOEPromptInfo>();
        }

        public BOEPromptInfo(ReportParameter prompt, int promptId, int rptId)
        {
            Id = promptId;
            Guid = System.Guid.NewGuid().ToString();
            Name = prompt.ParameterName;
            AllowCustomValues = !prompt.DisallowEditing;
            AllowDiscreteValues = prompt.SupportsDiscreteValues;
            AllowMultipleValue = prompt.EnableMultipleValues;
            AllowNullValue = prompt.EnableNullValue;

            if (prompt.SupportsRangeValues)
            {
                PromptFormat = BOEPromptFormat.Range;
            }
            else if (AllowMultipleValue)
            {
                PromptFormat = BOEPromptFormat.Multiple;
            }
            else
            {
                PromptFormat = BOEPromptFormat.Single;
            }

            Type = prompt.ValueType.ToString();
            IsProccess = prompt.InUse;
            IsPromptUserForPrompt = prompt.InUse;
            PromptText = (!prompt.Prompt.Equals(string.Empty)
                           ? prompt.Prompt :
                            string.Concat("Enter prompt values(", this.Name, ")"));
            ReportName = "N/A";
            LowerValueCurrent = new List<PromptValue>();
            LowerValueDefault = new List<PromptValue>();

            UpperValueCurrent = new List<PromptValue>();
            UpperValueDefault = new List<PromptValue>();

            if (prompt.DefaultValues.Count > 0)
            {
                loadDefaultValues(prompt.DefaultValues);
            }
            if (prompt.CurrentValues.Count > 0)
            {
                loadCurrentValues(prompt.CurrentValues);
            }

            //ControlList = new List<ReportPromptControl>();
            ReportId = rptId;
            IsEnableDescriptionOnly = prompt.EnableShowDescriptionOnly;

            IsSystemPrompt = false;
            LowerValue = new List<PromptValue>();
            UpperValue = new List<PromptValue>();
            IsFreeForm = false;
            LovSql = String.Empty;
            InititialValueSql = String.Empty;
            LovLimit = 0;
            IsLovSqlStoredProcedure = false;
            IsInitSqlStoredProcedure = false;
            IsTextBox = false;
            IsEnableLoadOnDemand = false;

            initLovDependancy = new List<BOEPromptInfo>();
            lovDependancy = new List<BOEPromptInfo>();
        }

        private void loadDefaultValues(ReportParameterValues vals)
        {
            foreach (ReportParameterValue val in vals)
            {
                if (val.IsSingleValue)
                {
                    LowerValueDefault.Add(new PromptValue(val.SingleValue.Value.ToString(), val.SingleValue.Description));
                }
                else if (val.IsRangeValue)
                {
                    if (!val.RangeValue.HasNoLowerBound)
                        LowerValueDefault.Add(new PromptValue(val.RangeValue.FromValue.Value.ToString(), val.RangeValue.FromValue.Description));
                    if (!val.RangeValue.HasNoUpperBound)
                        UpperValueDefault.Add(new PromptValue(val.RangeValue.ToValue.Value.ToString(), val.RangeValue.ToValue.Description));
                }
            }
        }

        private void loadCurrentValues(ReportParameterValues vals)
        {
            foreach (ReportParameterValue val in vals)
            {
                if (val.IsSingleValue)
                {
                    LowerValueCurrent.Add(new PromptValue(val.SingleValue.Value.ToString(), val.SingleValue.Description));
                }
                else if (val.IsRangeValue)
                {
                    if (!val.RangeValue.HasNoLowerBound)
                        LowerValueCurrent.Add(new PromptValue(val.RangeValue.FromValue.Value.ToString(), val.RangeValue.FromValue.Description));
                    if (!val.RangeValue.HasNoUpperBound)
                        UpperValueCurrent.Add(new PromptValue(val.RangeValue.ToValue.Value.ToString(), val.RangeValue.ToValue.Description));
                }
            }
        }
    }
}