﻿using System;
using System.Collections;

namespace IHPReportingInterface.Classes
{
    public class ReportPrompts : ArrayList
    {

        /// <summary>
        /// Gets the <see cref="ReportPrompt"/> with the specified name.
        /// </summary>
        /// <value></value>
        public virtual ReportPrompt this[string name]
        // Get Parameter by Name             
        {
            get
            {
                foreach (ReportPrompt p in this)
                {
                    //if (p.name.Equals(Name))
                    if (p.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase))
                    {
                        return p;
                    }
                }
                return null;
            }
        }
    }
}
