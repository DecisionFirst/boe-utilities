﻿using CrystalDecisions.Enterprise;

namespace DFT.BOE
{
    /// <summary>
    /// Represents Generic Report that dispalys in first(top) section of wireframe
    /// Moved over from IHPReportingInterface.Classes for BO 4.1 upgrade by Dell Stinnett-Christy 6/18/2014
    ///  </summary>
    public class GenericReport : BoxiReport
    {
        /// <summary>
        /// Jeff Hirschhorn 5/25/2011
        /// </summary>
        public int PersonalVersions { get; set; }

        /// <summary>
        /// Jeff Hirschhorn 5/25/2011
        /// </summary>
        public string PersonalVersionsIcon { get; set; }

        /// <summary>
        /// Jeff Hirschhorn 5/25/2011
        /// </summary>
        public int ScheduledVersions { get; set; }

        /// <summary>
        /// Jeff Hirschhorn 5/25/2011
        /// </summary>
        public string ScheduledVersionsIcon { get; set; }

        public GenericReport()
            : base()
        {
            PersonalVersions = -1;
            PersonalVersionsIcon = string.Empty;
            ScheduledVersions = -1;
            ScheduledVersionsIcon = string.Empty;
        }

        public GenericReport(InfoObject rpt)
            : base(rpt)
        {
            PersonalVersions = -1;
            PersonalVersionsIcon = string.Empty;
            ScheduledVersions = -1;
            ScheduledVersionsIcon = string.Empty;

            try
            {
                LastSuccessInstanceId = BOECommon.GetPropertyInt(rpt.Properties, "SI_LAST_SUCCESSFUL_INSTANCE_ID");
            }
            catch
            {
                //eat the error - there are no instances.
            }
            try
            {
                LastRun = BOECommon.GetPropertyDateTime(rpt.Properties, "SI_LAST_RUN_TIME");
            }
            catch
            {
                //eat the error - the report hasn't been run
            }
        }
    }
}