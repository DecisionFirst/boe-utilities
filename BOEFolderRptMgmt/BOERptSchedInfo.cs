﻿using CrystalDecisions.Enterprise;
using CrystalDecisions.Enterprise.Desktop;
using System;
using System.Collections.Generic;
using System.Web;

namespace DFT.BOE
{
    public class BOERptSchedInfo : BOEObjectInfo, IDisposable
    {
        public BOERptSchedInfo(InfoObjects iobjs, BOECommon common, bool forScheduling)
        {
            ErrMsg = string.Empty;
            if (forScheduling)
                //we need an InfoObjects here so that we can actually schedule the report.
                ReportObject = iobjs;
            else
                ReportObject = null;
            ID = iobjs[1].ID;
            Name = iobjs[1].Title;
            Description = iobjs[1].Description;
            Path = "/" + common.GetObjectPath(iobjs[1].ID, BOECommon.CETables.CI_INFOOBJECTS).Replace("\\", "/");
            AposFrequency = "N/A";

            Prompts = new Dictionary<string, BOEPromptInfo>();
            Prompts = loadPrompts(iobjs[1]);

            if (forScheduling)
            {
                Kind = iobjs[1].Kind;
                ProgId = iobjs[1].ProgID;
                ParentID = iobjs[1].ParentID;
                Properties props = iobjs[1].Properties;
                OutputFormat = BOECommon.GetPropertyString(props, "SI_PROGID_SCHEDULE");
                if (OutputFormat == string.Empty)
                {
                    OutputFormat = "CrystalEnterprise.Excel";
                }
            }
        }

        ~BOERptSchedInfo()
        {
            Dispose(true);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool Managed)
        {
            if (ReportObject != null)
            {
                ReportObject.Dispose();
            }
        }

        public string ErrMsg { get; set; }
        public InfoObjects ReportObject { get; set; }
        public Dictionary<string, BOEPromptInfo> Prompts { get; set; }
        public string OutputFormat { get; set; }
        public string ProgId { get; set; }
        public string AposFrequency { get; set; }
        public CeScheduleOutcome ScheduleOutcome { get; set; }

        /// <summary>
        /// Set the value of a prompt on the ReportObject InfoObject so that we can schedule the report.
        /// </summary>
        /// <param name="promptName">Name of the prompt</param>
        /// <param name="loValues">List of values for all types of prompts.  For Single Value and Range prompts, this will
        ///      have only one value.  For Multi Value prompts, it may have more than one.</param>
        /// <param name="hiValues">The high value for a range prompt.  This list should always contain only one entry.</param>
        /// <returns></returns>
        public bool SetPromptValue(BOEPromptInfo prompt)
        {
            try
            {
                Report rpt = (Report)ReportObject[1];
                foreach (ReportParameter param in rpt.ReportParameters)
                {
                    if (param.ParameterName == prompt.Name)
                    {
                        param.CurrentValues.Clear();
                        switch (prompt.PromptFormat)
                        {
                            case BOEPromptFormat.Single:

                                setSingleValue(param, prompt.LowerValue[0], prompt);
                                break;

                            case BOEPromptFormat.Range:
                                setRangeValue(param, prompt.LowerValue[0], prompt.UpperValue[0], prompt);
                                break;

                            case BOEPromptFormat.Multiple:
                                setMultiValue(param, prompt.LowerValue);
                                break;
                        }
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Schedule a report to run right now.
        /// </summary>
        /// <param name="instanceName">The name to set for the new instance</param>
        /// <param name="common">An BOECommon that controls the connection to the CMS</param>
        /// <returns>SI_ID of the new instance if successful.  0 if the schedule was attempted but errored out.
        ///    -1 if there was an error prior to attempting the schedule.</returns>
        public int ScheduleReportNow(string instanceName, BOECommon common)
        {
            //initialize properties
            ErrMsg = string.Empty;
            ScheduleOutcome = CeScheduleOutcome.ceOutcomePending;
            AposFrequency = string.Empty;

            if (ReportObject != null)
            {
                //Schedule the report
                ReportObject[1].Title = instanceName;
                SchedulingInfo si = ReportObject[1].SchedulingInfo;

                si.RightNow = true;
                if (common.ScheduleObjects(ReportObject))
                {
                    //get the schedule outcome
                    ScheduleOutcome = ReportObject[1].SchedulingInfo.Outcome;
                    if ((ScheduleOutcome == CeScheduleOutcome.ceOutcomeSuccess) || (ScheduleOutcome == CeScheduleOutcome.ceOutcomePending))
                    {
                        //Get the SI_ID of the new instance
                        string newId = BOECommon.GetPropertyString(ReportObject[1].Properties, "SI_NEW_JOB_ID");
                        if (!string.IsNullOrEmpty(newId))
                        {
                            return int.Parse(newId);
                        }
                        else
                        {
                            ErrMsg = "BOERptInfo.ScheduleReportNow: New Instance ID not found.";
                            return 0;
                        }
                    }
                    else
                    {
                        //Set the error message based on the outcome
                        setScheduleError(ScheduleOutcome, ReportObject[1].SchedulingInfo.ErrorMessage);
                        return 0;
                    }
                }
            }
            else
            {
                ErrMsg = "BOERptInfo.ScheduleReportNow: ReportObject not initialized for scheduling.";
            }
            return -1;
        }

        /// <summary>
        /// Schedule a report to run on a specific start date and frequency.
        /// </summary>
        /// <param name="instanceName">The name to set for the new instance</param>
        /// <param name="common">An BOECommon that controls the connection to the CMS</param>
        /// <returns>SI_ID of the new instance if successful.  0 if the schedule was attempted but errored out.
        ///    -1 if there was an error prior to attempting the schedule.</returns>
        public int ScheduleReport(DateTime start, DateTime end, string frequency, string instanceName, BOECommon common)
        {
            //initialize properties
            ErrMsg = string.Empty;
            ScheduleOutcome = CeScheduleOutcome.ceOutcomePending;
            AposFrequency = string.Empty;

            if (ReportObject != null)
            {
                //Set the instance name
                ReportObject[1].Title = instanceName;
                SchedulingInfo si = ReportObject[1].SchedulingInfo;
                si.RightNow = false;

                si.BeginDate = start;
                si.EndDate = end;

                //set the frequency
                if (frequency == null)
                {
                    si.Type = CeScheduleType.ceScheduleTypeOnce;
                }
                else if (frequency.IndexOf("Daily", StringComparison.CurrentCultureIgnoreCase) >= 0)
                {
                    si.Type = CeScheduleType.ceScheduleTypeDaily;
                    AposFrequency = "Daily";
                }
                else if (frequency.IndexOf("Weekly", StringComparison.CurrentCultureIgnoreCase) >= 0)
                {
                    AposFrequency = "Weekly";
                    si.Type = CeScheduleType.ceScheduleTypeWeekly;
                    CeDayOfWeek dow = getDow(start);
                    //Must add the day(s) of week to run a weekly schedule.  This will default to the day
                    //  of the start date.  This functionality was not set up correctly in the original IHP
                    //  code and may not have ever been used.
                    //TODO:  In order to fully implement the ability to select the days of the week that the
                    //       report will run, we need to pass that information in to this method.
                    si.CalendarRunDays.Add(start.Day, start.Month, start.Year, end.Day, end.Month, end.Year, dow);
                }
                else if (frequency.IndexOf("Monthly", StringComparison.CurrentCultureIgnoreCase) >= 0)
                {
                    AposFrequency = "Monthly";
                    si.Type = CeScheduleType.ceScheduleTypeMonthly;
                    si.IntervalMonths = 1;
                }
                //schedule
                if (common.ScheduleObjects(ReportObject))
                {
                    //get the schedule outcome
                    ScheduleOutcome = ReportObject[1].SchedulingInfo.Outcome;
                    if ((ScheduleOutcome == CeScheduleOutcome.ceOutcomeSuccess) || (ScheduleOutcome == CeScheduleOutcome.ceOutcomePending))
                    {
                        //Get the SI_ID of the new instance
                        string newId = BOECommon.GetPropertyString(ReportObject[1].Properties, "SI_NEW_JOB_ID");
                        if (!string.IsNullOrEmpty(newId))
                        {
                            return int.Parse(newId);
                        }
                        else
                        {
                            ErrMsg = "BOERptInfo.ScheduleReport: New Instance ID not found.";
                            return 0;
                        }
                    }
                    else
                    {
                        //Set the error message based on the outcome
                        setScheduleError(ScheduleOutcome, ReportObject[1].SchedulingInfo.ErrorMessage);
                        return 0;
                    }
                }
                else
                {
                    ErrMsg = "BOERptInfo.ScheduleReport: " + BOECommon.ErrMsg;
                }
            }
            else
            {
                ErrMsg = "BOERptInfo.ScheduleReportNow: ReportObject not initialized for scheduling.";
            }
            return -1;
        }

        /// <summary>
        /// Sets the output format of the report that is being scheduled.
        /// </summary>
        /// <param name="destFormat"></param>
        /// <param name="formatExcel"></param>
        /// <param name="customWidth"></param>
        public bool SetFormat(string destFormat, bool formatExcel, int customWidth)
        {
            if (ReportObject != null)
            {
                if (destFormat != null)
                {
                    Report rpt = (Report)ReportObject[1];
                    if (destFormat.Equals("PDF", StringComparison.CurrentCultureIgnoreCase))
                        rpt.ReportFormatOptions.Format = CeReportFormat.ceFormatPDF;
                    else if (destFormat.Equals("Excel", StringComparison.CurrentCultureIgnoreCase))
                    {
                        rpt.ReportFormatOptions.Format = CeReportFormat.ceFormatExcelDataOnly;
                        rpt.ReportFormatOptions.ExcelDataOnlyFormat.UseFormat = formatExcel;
                        if (customWidth > 0)
                        {
                            rpt.ReportFormatOptions.ExcelDataOnlyFormat.UseConstColWidth = true;
                            rpt.ReportFormatOptions.ExcelDataOnlyFormat.ConstColWidth = customWidth;
                        }
                    }
                }
                return true;
            }
            else
            {
                ErrMsg = "BOERptInfo.ScheduleReportNow: ReportObject not initialized for scheduling.";
                return false;
            }
        }

        #region Private

        private Dictionary<string, BOEPromptInfo> loadPrompts(InfoObject iobj)
        {
            int i = 0;
            ReportParameters prompts = ((Report)iobj).ReportParameters;
            foreach (ReportParameter prompt in prompts)
            {
                BOEPromptInfo pinfo = new BOEPromptInfo(prompt, i * 10, iobj.ID);
                Prompts.Add(i.ToString(), pinfo);
                i++;
            }
            return Prompts;
        }

        private void setSingleValue(ReportParameter param, PromptValue value)
        {
            ReportParameterSingleValue psValue = param.CreateSingleValue();

            if (value.Description == string.Empty)
            {
                psValue.SingleValue.Description = HttpUtility.HtmlDecode(value.Value);
            }
            else
            {
                psValue.SingleValue.Description = value.Description;
            }
            psValue.Value = HttpUtility.HtmlDecode(value.Value);
            param.CurrentValues.Add(psValue);
        }

        private void setSingleValue(ReportParameter param, PromptValue value, BOEPromptInfo prompt)
        {
            ReportParameterValue psValue = param.CreateSingleValue();

            if (value.Description == string.Empty)
            {
                psValue.SingleValue.Description = HttpUtility.HtmlDecode(value.Value);
            }
            else
            {
                psValue.SingleValue.Description = value.Description;
            }

            try
            {
                DateTime startdate = DateTime.MinValue;
                bool isdate = DateTime.TryParse(value.Value, out startdate);

                if (isdate)
                {
                    string res = prompt.Type.Equals(CeReportVariableValueType.ceRVDate.ToString(),
                                               StringComparison.CurrentCultureIgnoreCase)
                          ? String.Concat("Date(", startdate.Year.ToString(), ",", startdate.Month.ToString(), ",",
                                          startdate.Day.ToString(), ")")
                          : String.Concat("DateTime(", startdate.Year.ToString(), ",", startdate.Month.ToString(), ",",
                                          startdate.Day.ToString(), ",", startdate.Hour.ToString(), ",", startdate.Minute.ToString(), ",",
                                          startdate.Second.ToString(), ")");
                    psValue.SingleValue.Value = HttpUtility.HtmlDecode(res);
                }
                else
                {
                    psValue.SingleValue.Value = value.Value;
                }
            }
            catch (ArgumentException)
            {
                psValue.SingleValue.Value = value.Value;
            }

            param.CurrentValues.Add(psValue);
        }

        private void setRangeValue(ReportParameter param, PromptValue loValue, PromptValue hiValue, BOEPromptInfo prompt)
        {
            ReportParameterRangeValue prValue = param.CreateRangeValue();

            if (loValue != null)
            {
                try
                {
                    DateTime startdate = DateTime.MinValue;
                    bool isdate = DateTime.TryParse(loValue.Value, out startdate);

                    if (isdate)
                    {
                        string res = prompt.Type.Equals(CeReportVariableValueType.ceRVDate.ToString(),
                                                   StringComparison.CurrentCultureIgnoreCase)
                              ? String.Concat("Date(", startdate.Year.ToString(), ",", startdate.Month.ToString(), ",",
                                              startdate.Day.ToString(), ")")
                              : String.Concat("DateTime(", startdate.Year.ToString(), ",", startdate.Month.ToString(), ",",
                                              startdate.Day.ToString(), ",", startdate.Hour.ToString(), ",", startdate.Minute.ToString(), ",",
                                              startdate.Second.ToString(), ")");
                        prValue.RangeValue.FromValue.Value = HttpUtility.HtmlDecode(res);
                    }
                    else
                    {
                        prValue.RangeValue.FromValue.Value = HttpUtility.HtmlDecode(loValue.Value);
                    }
                }
                catch (ArgumentException)
                {
                    prValue.RangeValue.FromValue.Value = HttpUtility.HtmlDecode(loValue.Value);
                }
            }
            if (hiValue != null)
            {
                try
                {
                    DateTime enddate = DateTime.MinValue;
                    bool isdate = DateTime.TryParse(hiValue.Value, out enddate);

                    if (isdate)
                    {
                        string res = prompt.Type.Equals(CeReportVariableValueType.ceRVDate.ToString(),
                                                   StringComparison.CurrentCultureIgnoreCase)
                              ? String.Concat("Date(", enddate.Year.ToString(), ",", enddate.Month.ToString(), ",",
                                              enddate.Day.ToString(), ")")
                              : String.Concat("DateTime(", enddate.Year.ToString(), ",", enddate.Month.ToString(), ",",
                                              enddate.Day.ToString(), ",", enddate.Hour.ToString(), ",", enddate.Minute.ToString(), ",",
                                              enddate.Second.ToString(), ")");
                        prValue.RangeValue.ToValue.Value = HttpUtility.HtmlDecode(res);
                    }
                    else
                    {
                        prValue.RangeValue.ToValue.Value = HttpUtility.HtmlDecode(hiValue.Value);
                    }
                }
                catch (ArgumentException)
                {
                    prValue.RangeValue.ToValue.Value = HttpUtility.HtmlDecode(hiValue.Value);
                }
            }

            param.CurrentValues.Add(prValue);
        }

        private void setMultiValue(ReportParameter param, List<PromptValue> values)
        {
            foreach (PromptValue pvalue in values)
            {
                setSingleValue(param, pvalue);
            }
        }

        private CeDayOfWeek getDow(DateTime dt)
        {
            DayOfWeek dtDow = dt.DayOfWeek;
            CeDayOfWeek result;
            switch (dtDow)
            {
                case DayOfWeek.Sunday:
                    result = CeDayOfWeek.ceDaySunday;
                    break;

                case DayOfWeek.Monday:
                    result = CeDayOfWeek.ceDayMonday;
                    break;

                case DayOfWeek.Tuesday:
                    result = CeDayOfWeek.ceDayTuesday;
                    break;

                case DayOfWeek.Wednesday:
                    result = CeDayOfWeek.ceDayWednesday;
                    break;

                case DayOfWeek.Thursday:
                    result = CeDayOfWeek.ceDayThursday;
                    break;

                case DayOfWeek.Friday:
                    result = CeDayOfWeek.ceDayFriday;
                    break;

                case DayOfWeek.Saturday:
                    result = CeDayOfWeek.ceDaySaturday;
                    break;

                default:
                    result = CeDayOfWeek.ceDayAll;
                    break;
            }
            return result;
        }

        private void setScheduleError(CeScheduleOutcome outcome, string msg)
        {
            string errorMessage = string.Empty;
            switch (outcome)
            {
                case CeScheduleOutcome.ceOutcomeFailComponentFailed:
                    errorMessage = "FAIL_COMPONENT_FAILED";
                    break;

                case CeScheduleOutcome.ceOutcomeFailEndTime:
                    errorMessage = "FAIL_ENDTIME";
                    break;

                case CeScheduleOutcome.ceOutcomeFailJobServer:
                    errorMessage = "FAIL_JOBSERVER";
                    break;

                case CeScheduleOutcome.ceOutcomeFailJobServerChild:
                    errorMessage = "FAIL_JOBSERVER_CHILD";
                    break;

                case CeScheduleOutcome.ceOutcomeFailJobServerPlugin:
                    errorMessage = "FAIL_JOBSERVER_PLUGIN";
                    break;

                case CeScheduleOutcome.ceOutcomeFailObjectPackageFailed:
                    errorMessage = "FAIL_OBJECT_PACKAGE_FAILED";
                    break;

                case CeScheduleOutcome.ceOutcomeFailSchedule:
                    errorMessage = "FAIL_SCHEDULE";
                    break;

                case CeScheduleOutcome.ceOutcomeFailSecurity:
                    errorMessage = "FAIL_SECURITY";
                    break;

                case CeScheduleOutcome.ceOutcomeFailConstraints:
                    errorMessage = "FAIL_CONSTRAINTS";
                    break;
            }
            if (!string.IsNullOrEmpty(msg))
                ErrMsg = String.Format("Scheduling failed with code: {0}. Err={1}", errorMessage, msg);
            else
                ErrMsg = String.Format("Scheduling failed with code: {0}", errorMessage);
        }

        public string TranslateOutcome()
        {
            string result = string.Empty;
            switch (ScheduleOutcome)
            {
                case CeScheduleOutcome.ceOutcomeSuccess:
                    result = "Success";
                    break;

                case CeScheduleOutcome.ceOutcomePending:
                    result = "Pending";
                    break;

                case CeScheduleOutcome.ceOutcomeFailComponentFailed:
                    result = "FAIL_COMPONENT_FAILED";
                    break;

                case CeScheduleOutcome.ceOutcomeFailEndTime:
                    result = "FAIL_ENDTIME";
                    break;

                case CeScheduleOutcome.ceOutcomeFailJobServer:
                    result = "FAIL_JOBSERVER";
                    break;

                case CeScheduleOutcome.ceOutcomeFailJobServerChild:
                    result = "FAIL_JOBSERVER_CHILD";
                    break;

                case CeScheduleOutcome.ceOutcomeFailJobServerPlugin:
                    result = "FAIL_JOBSERVER_PLUGIN";
                    break;

                case CeScheduleOutcome.ceOutcomeFailObjectPackageFailed:
                    result = "FAIL_OBJECT_PACKAGE_FAILED";
                    break;

                case CeScheduleOutcome.ceOutcomeFailSchedule:
                    result = "FAIL_SCHEDULE";
                    break;

                case CeScheduleOutcome.ceOutcomeFailSecurity:
                    result = "FAIL_SECURITY";
                    break;

                case CeScheduleOutcome.ceOutcomeFailConstraints:
                    result = "FAIL_CONSTRAINTS";
                    break;
            }
            return result;
        }

        #endregion Private
    }
}