﻿using BusinessObjects.Enterprise.Exception;
using CrystalDecisions.Enterprise;
using CrystalDecisions.Enterprise.Desktop;
using System;
using System.Collections.Generic;

namespace DFT.BOE
{
    /// <summary>
    /// Represents generic Boxi Object (folder, cr report, Webi etc)
    /// </summary>
    public class BoxiObject
    {
        public string Name { get; set; }
        public string ProgId { get; set; }
        public int Id { get; set; }
        public int ParentId { get; set; }
        public int Children { get; set; }

        public BoxiObject(InfoObject iobj, BOECommon common)
        {
            Name = iobj.Title;
            ProgId = iobj.ProgID;
            Id = iobj.ID;
            ParentId = iobj.ParentID;
            if (iobj.Kind == "CrystalReport")
            {
                Report rpt = (Report)iobj;
                Children = int.Parse(rpt.ReportParameters.Count.ToString());
            }
            else
            {
                string count = BOECommon.GetPropertyString(iobj.Properties, "SI_CHILDREN");
                if (!string.IsNullOrEmpty(count))
                    Children = int.Parse(count);
                else
                    Children = 0;
            }
        }
    }

    public class FolderObject
    {
        private List<FolderObject> lChildren;
        private List<FolderObject> lParents;

        private bool bHasChildren;
        private bool bIsRootNode;
        private int iID;
        private string sCUID;
        private string sName;
        private string sParentCUID;
        private int iParentID;
        private string sParentName;

        public bool IsRootNode
        {
            get { return bIsRootNode; }
            set { this.IsRootNode = value; }
        }

        public bool HasChildren
        {
            get { return bHasChildren; }
            set { this.bHasChildren = value; }
        }

        public String CUID
        {
            get
            {
                return sCUID;
            }
            set
            {
                this.sCUID = value;
            }
        }

        public List<FolderObject> Parents
        {
            get
            {
                return lParents;
            }
            set
            {
                this.lParents = value;
            }
        }

        public FolderObject()
        {
            iID = 0;
            sCUID = "";
            sName = "";
            sParentCUID = "";
            iParentID = 0;
            sParentName = "";
            bHasChildren = false;
            bIsRootNode = true;
        }

        public FolderObject(int ID, string CUID, string Name, string ParentCUID, bool HasChildren, bool IsRootNode)
        {
            iID = ID;
            sCUID = CUID;
            sName = Name;
            iParentID = 0;
            sParentName = "";
            sParentCUID = ParentCUID;
            bHasChildren = HasChildren;
            bIsRootNode = IsRootNode;
        }

        public FolderObject(int ID, string CUID, string Name, string ParentCUID, String HasChildren = "", bool IsRootNode = false)
        {
            iID = ID;
            sCUID = CUID;
            sName = Name;
            iParentID = 0;
            sParentName = "";
            sParentCUID = ParentCUID;
            if (HasChildren.ToLower() == "true") bHasChildren = true;
            else bHasChildren = false;
            bIsRootNode = IsRootNode;
        }

        public FolderObject(int ID, string CUID, string Name, int ParentID, string ParentCUID, string ParentName, bool IsRootNode, String HasChildren = "")
        {
            iID = ID;
            sCUID = CUID;
            sName = Name;
            sParentCUID = ParentCUID;
            iParentID = ParentID;
            sParentName = ParentName;
            if (HasChildren.ToLower() == "true") bHasChildren = true;
            else bHasChildren = false;
            bIsRootNode = IsRootNode;
        }

        public String Name
        {
            get
            {
                return sName;
            }
            set
            {
                this.sName = value;
            }
        }

        public void setNameCust(InfoObject pObj)
        {
            try
            {
                if (pObj.Kind.ToLower().Equals("favoritesfolder"))
                {
                    this.sName = "My Reports";
                }
                else
                {
                    this.sName = pObj.Title;
                }
            }
            catch (SDKException)
            {
                throw;
            }
        }

        public int Id
        {
            get { return iID; }
            set { this.iID = value; }
        }

        public List<FolderObject> Children
        {
            get { return lChildren; }
            set { this.lChildren = value; }
        }

        public int ParentID
        {
            get { return iParentID; }
            set { this.iParentID = value; }
        }

        public string ParentCUID
        {
            get { return sParentCUID; }
            set { this.sParentCUID = value; }
        }

        public string ParentName
        {
            get { return sParentName; }
            set { this.sParentName = value; }
        }
    }
}