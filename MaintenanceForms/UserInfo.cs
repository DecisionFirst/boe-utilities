﻿using CrystalDecisions.Enterprise;
using CrystalDecisions.Enterprise.Dest;
using DFT.BOE;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace MaintenanceForms
{
    public partial class UserInfo : Form
    {
        private BOECommon CommonInfo;

        public UserInfo()
        {
            InitializeComponent();
            CommonInfo = new BOECommon();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CommonInfo = new BOECommon(txtUserID.Text, txtPassword.Text, txtCMS.Text);
            if (CommonInfo.LoggedOn)
            {
                lblStatus.Text = "User " + txtUserID.Text + " logged on successfully";
            }
            else
            {
                lblStatus.Text = "Error Occurred During Logon: " + CommonInfo.GetLastErrMsg;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
        }

        private void btnLoadUsers_Click(object sender, EventArgs e)
        {
            if (CommonInfo.LoggedOn)
            {
                //SchedulingInfo sched = null;
                DestinationPlugin destPlugin = null;

                SmtpOptions smtpOpts = null;
                Destination destination = null;

                //BOEUserMaintenance BOEUM = new BOEUserMaintenance(CommonInfo);
                InfoObjects iObjs =
                    CommonInfo.ExecuteRawQuery("SELECT * FROM CI_INFOOBJECTS WHERE SI_RECURRING= 1");

                destPlugin = (DestinationPlugin)CommonInfo.ExecuteRawQuery("select * " +
                  "from ci_systemobjects where si_parentId=29 and si_name='CrystalEnterprise.SMTP' ")[1];

                List<AliasInfo> lUsers = new List<AliasInfo>();
                lbUserList.Items.Clear();

                foreach (InfoObject iObj in iObjs)
                {
                    //sched = iObj.SchedulingInfo;
                    destination = iObj.SchedulingInfo.Destination;
                    destination.CopyToPlugin(destPlugin);

                    if (destPlugin.GetType() == typeof(CrystalDecisions.Enterprise.Dest.Smtp))
                    {
                        smtpOpts = (SmtpOptions)destPlugin.ScheduleOptions;

                        String s = smtpOpts.ToAddresses.ToString();
                        s = s.Replace(',', ';');
                        s = s.Replace('[', ' ');
                        s = s.Replace(']', ' ');
                        s = s.Trim();

                        String from = smtpOpts.SenderAddress.ToString();

                        lbUserList.Items.Add(destination.Name + " - " + from + " - " + s);
                    }
                }
                lbUserList.Sorted = true;
            }
        }

        private void btnLoadLDAP_Click(object sender, EventArgs e)
        {
            if (CommonInfo.LoggedOn)
            {
                //BOEUserMaintenance BOEUM = new BOEUserMaintenance(CommonInfo);
                /*
                //List<AliasInfo> lUsers = BOEUM.getAllUserIDs("secLDAP");
                lbUserList.Items.Clear();
                foreach (AliasInfo sUser in lUsers)
                {
                    lbUserList.Items.Add(sUser.ID + " - " + sUser.UserName + " - " + sUser.UserFullName + " - " + sUser.Alias);
                }
                */
                lbUserList.Sorted = true;
            }
        }

        private void lblStatus_Click(object sender, EventArgs e)
        {
        }

        /*
        private void btnMerge_Click(object sender, EventArgs e)
        {
            BOEUserMaintenance BOEUM = new BOEUserMaintenance(CommonInfo);

            List<AliasInfo> lLDAPUsers = BOEUM.getAllUserIDs("secLDAP");
            List<AliasInfo> lADUsers = BOEUM.getAllUserIDs("secWinAD");
            string ldapalias;
            string adalias;
            List<String> lmergeFrom = new List<string>();
            lbUserList.Items.Clear();
            lbUserList.Sorted = true;
            foreach (AliasInfo ainfo in lLDAPUsers)
            {
                ldapalias = ainfo.Alias.Replace("secLDAP:", "");
                    foreach (AliasInfo aADUser in lADUsers)
                    {
                        adalias = aADUser.Alias.Replace("secWinAD:", "");
                        if (ldapalias.Trim().ToLower().Replace(", ou=",",ou=").Replace(", dc=", ",dc=").Equals(adalias.Trim().ToLower()))
                        {
                            lmergeFrom.Clear();
                            lmergeFrom.Add(ainfo.UserName);
                            BOEUM.MergeUsers(aADUser.UserName, lmergeFrom, false);
                            lbUserList.Items.Add("Replace Alias: From: " + ainfo.UserName + " To: " + aADUser.UserName);
                        }
                    }
            }
            lblStatus.Text = "Merge of Users Complete";
        } */
    }

    public class SortedListBox :
    ListBox
    {
        public SortedListBox()
            : base()
        {
        }

        // Overrides the parent class Sort to perform a simple
        // bubble sort on the length of the string contained in each item.
        protected override void Sort()
        {
            if (Items.Count > 1)
            {
                bool swapped;
                do
                {
                    int counter = Items.Count - 1;
                    swapped = false;

                    while (counter > 0)
                    {
                        // Compare the items' length.
                        if (Items[counter].ToString().Length
                            < Items[counter - 1].ToString().Length)
                        {
                            // Swap the items.
                            object temp = Items[counter];
                            Items[counter] = Items[counter - 1];
                            Items[counter - 1] = temp;
                            swapped = true;
                        }
                        // Decrement the counter.
                        counter -= 1;
                    }
                }
                while ((swapped == true));
            }
        }
    }
}