﻿namespace UpdateUsers
{
  partial class ScheduleTestForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScheduleTestForm));
            this.cmsGroup = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtInfo = new System.Windows.Forms.TextBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.userLabel = new System.Windows.Forms.Label();
            this.statusLabel = new System.Windows.Forms.Label();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.loadBtn = new System.Windows.Forms.Button();
            this.cmsStatus = new System.Windows.Forms.Label();
            this.cmsLogon = new System.Windows.Forms.Button();
            this.cmsPassword = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmsUser = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cms = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.iList = new System.Windows.Forms.ImageList(this.components);
            this.tvRptFolders = new System.Windows.Forms.TreeView();
            this.gbParams = new System.Windows.Forms.GroupBox();
            this.lblLOV = new System.Windows.Forms.Label();
            this.lbLOV = new System.Windows.Forms.ListBox();
            this.dgvParams = new System.Windows.Forms.DataGridView();
            this.paramNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paramValueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paramIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dbsParamList = new System.Windows.Forms.BindingSource(this.components);
            this.scheduleTestData = new UpdateUsers.ScheduleTestData();
            this.cmsGroup.SuspendLayout();
            this.panel1.SuspendLayout();
            this.gbParams.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvParams)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbsParamList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scheduleTestData)).BeginInit();
            this.SuspendLayout();
            // 
            // cmsGroup
            // 
            this.cmsGroup.Controls.Add(this.panel1);
            this.cmsGroup.Controls.Add(this.cmsStatus);
            this.cmsGroup.Controls.Add(this.cmsLogon);
            this.cmsGroup.Controls.Add(this.cmsPassword);
            this.cmsGroup.Controls.Add(this.label3);
            this.cmsGroup.Controls.Add(this.cmsUser);
            this.cmsGroup.Controls.Add(this.label2);
            this.cmsGroup.Controls.Add(this.cms);
            this.cmsGroup.Controls.Add(this.label1);
            this.cmsGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmsGroup.Location = new System.Drawing.Point(3, 3);
            this.cmsGroup.Name = "cmsGroup";
            this.cmsGroup.Size = new System.Drawing.Size(345, 351);
            this.cmsGroup.TabIndex = 8;
            this.cmsGroup.TabStop = false;
            this.cmsGroup.Text = "Log On To CMS";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtInfo);
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.userLabel);
            this.panel1.Controls.Add(this.statusLabel);
            this.panel1.Controls.Add(this.cancelBtn);
            this.panel1.Controls.Add(this.loadBtn);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(3, 143);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(339, 205);
            this.panel1.TabIndex = 18;
            // 
            // txtInfo
            // 
            this.txtInfo.Location = new System.Drawing.Point(4, 83);
            this.txtInfo.Multiline = true;
            this.txtInfo.Name = "txtInfo";
            this.txtInfo.Size = new System.Drawing.Size(331, 107);
            this.txtInfo.TabIndex = 25;
            this.txtInfo.TextChanged += new System.EventHandler(this.txtInfo_TextChanged);
            // 
            // btnClose
            // 
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(260, 5);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 24;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // userLabel
            // 
            this.userLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.userLabel.Location = new System.Drawing.Point(4, 56);
            this.userLabel.Name = "userLabel";
            this.userLabel.Size = new System.Drawing.Size(332, 23);
            this.userLabel.TabIndex = 23;
            this.userLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // statusLabel
            // 
            this.statusLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.statusLabel.Location = new System.Drawing.Point(4, 31);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(333, 23);
            this.statusLabel.TabIndex = 22;
            this.statusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cancelBtn
            // 
            this.cancelBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cancelBtn.Enabled = false;
            this.cancelBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelBtn.Location = new System.Drawing.Point(132, 5);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 21;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // loadBtn
            // 
            this.loadBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.loadBtn.Enabled = false;
            this.loadBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadBtn.Location = new System.Drawing.Point(4, 5);
            this.loadBtn.Name = "loadBtn";
            this.loadBtn.Size = new System.Drawing.Size(75, 23);
            this.loadBtn.TabIndex = 20;
            this.loadBtn.Text = "Load Users";
            this.loadBtn.UseVisualStyleBackColor = true;
            this.loadBtn.Click += new System.EventHandler(this.loadBtn_Click);
            // 
            // cmsStatus
            // 
            this.cmsStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmsStatus.Location = new System.Drawing.Point(6, 100);
            this.cmsStatus.Name = "cmsStatus";
            this.cmsStatus.Size = new System.Drawing.Size(326, 50);
            this.cmsStatus.TabIndex = 7;
            this.cmsStatus.Text = "NOT logged on to CMS!";
            this.cmsStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmsLogon
            // 
            this.cmsLogon.Location = new System.Drawing.Point(248, 51);
            this.cmsLogon.Name = "cmsLogon";
            this.cmsLogon.Size = new System.Drawing.Size(75, 23);
            this.cmsLogon.TabIndex = 6;
            this.cmsLogon.Text = "Log On";
            this.cmsLogon.UseVisualStyleBackColor = true;
            this.cmsLogon.Click += new System.EventHandler(this.cmsLogon_Click);
            // 
            // cmsPassword
            // 
            this.cmsPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmsPassword.Location = new System.Drawing.Point(75, 77);
            this.cmsPassword.Name = "cmsPassword";
            this.cmsPassword.PasswordChar = '*';
            this.cmsPassword.Size = new System.Drawing.Size(167, 20);
            this.cmsPassword.TabIndex = 5;
            this.cmsPassword.Text = "Admin123";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Password:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmsUser
            // 
            this.cmsUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmsUser.Location = new System.Drawing.Point(75, 51);
            this.cmsUser.Name = "cmsUser";
            this.cmsUser.Size = new System.Drawing.Size(167, 20);
            this.cmsUser.TabIndex = 3;
            this.cmsUser.Text = "Administrator";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "User Name:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cms
            // 
            this.cms.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cms.Location = new System.Drawing.Point(75, 25);
            this.cms.Name = "cms";
            this.cms.Size = new System.Drawing.Size(167, 20);
            this.cms.TabIndex = 1;
            this.cms.Text = "BOE-DEV01";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(36, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "CMS:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // iList
            // 
            this.iList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("iList.ImageStream")));
            this.iList.TransparentColor = System.Drawing.Color.Transparent;
            this.iList.Images.SetKeyName(0, "Folder_open.gif");
            this.iList.Images.SetKeyName(1, "Folder.gif");
            this.iList.Images.SetKeyName(2, "webi.gif");
            this.iList.Images.SetKeyName(3, "cr_report.gif");
            // 
            // tvRptFolders
            // 
            this.tvRptFolders.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvRptFolders.ImageIndex = 0;
            this.tvRptFolders.ImageList = this.iList;
            this.tvRptFolders.Location = new System.Drawing.Point(363, 12);
            this.tvRptFolders.Name = "tvRptFolders";
            this.tvRptFolders.SelectedImageIndex = 0;
            this.tvRptFolders.Size = new System.Drawing.Size(272, 338);
            this.tvRptFolders.TabIndex = 9;
            this.tvRptFolders.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvRptFolders_AfterSelect);
            this.tvRptFolders.DoubleClick += new System.EventHandler(this.loadBtn_Click);
            // 
            // gbParams
            // 
            this.gbParams.Controls.Add(this.lblLOV);
            this.gbParams.Controls.Add(this.lbLOV);
            this.gbParams.Controls.Add(this.dgvParams);
            this.gbParams.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbParams.Location = new System.Drawing.Point(642, 13);
            this.gbParams.Name = "gbParams";
            this.gbParams.Size = new System.Drawing.Size(431, 338);
            this.gbParams.TabIndex = 10;
            this.gbParams.TabStop = false;
            this.gbParams.Text = "Parameter Values";
            this.gbParams.Visible = false;
            // 
            // lblLOV
            // 
            this.lblLOV.AutoSize = true;
            this.lblLOV.Location = new System.Drawing.Point(12, 206);
            this.lblLOV.Name = "lblLOV";
            this.lblLOV.Size = new System.Drawing.Size(77, 13);
            this.lblLOV.TabIndex = 15;
            this.lblLOV.Text = "Valid Values";
            this.lblLOV.Visible = false;
            // 
            // lbLOV
            // 
            this.lbLOV.DisplayMember = "ParamLOVValue";
            this.lbLOV.FormattingEnabled = true;
            this.lbLOV.Location = new System.Drawing.Point(12, 225);
            this.lbLOV.Name = "lbLOV";
            this.lbLOV.Size = new System.Drawing.Size(240, 95);
            this.lbLOV.TabIndex = 14;
            this.lbLOV.ValueMember = "ParamLOVValue";
            this.lbLOV.Visible = false;
            // 
            // dgvParams
            // 
            this.dgvParams.AutoGenerateColumns = false;
            this.dgvParams.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvParams.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.paramNameDataGridViewTextBoxColumn,
            this.paramValueDataGridViewTextBoxColumn,
            this.paramIDDataGridViewTextBoxColumn});
            this.dgvParams.DataSource = this.dbsParamList;
            this.dgvParams.Location = new System.Drawing.Point(12, 18);
            this.dgvParams.Name = "dgvParams";
            this.dgvParams.Size = new System.Drawing.Size(384, 179);
            this.dgvParams.TabIndex = 13;
            this.dgvParams.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvParams_RowEnter);
            // 
            // paramNameDataGridViewTextBoxColumn
            // 
            this.paramNameDataGridViewTextBoxColumn.DataPropertyName = "ParamName";
            this.paramNameDataGridViewTextBoxColumn.HeaderText = "ParamName";
            this.paramNameDataGridViewTextBoxColumn.Name = "paramNameDataGridViewTextBoxColumn";
            // 
            // paramValueDataGridViewTextBoxColumn
            // 
            this.paramValueDataGridViewTextBoxColumn.DataPropertyName = "ParamValue";
            this.paramValueDataGridViewTextBoxColumn.HeaderText = "ParamValue";
            this.paramValueDataGridViewTextBoxColumn.Name = "paramValueDataGridViewTextBoxColumn";
            // 
            // paramIDDataGridViewTextBoxColumn
            // 
            this.paramIDDataGridViewTextBoxColumn.DataPropertyName = "ParamID";
            this.paramIDDataGridViewTextBoxColumn.HeaderText = "ParamID";
            this.paramIDDataGridViewTextBoxColumn.Name = "paramIDDataGridViewTextBoxColumn";
            // 
            // dbsParamList
            // 
            this.dbsParamList.DataMember = "ParamList";
            this.dbsParamList.DataSource = this.scheduleTestData;
            // 
            // scheduleTestData
            // 
            this.scheduleTestData.DataSetName = "ScheduleTestData";
            this.scheduleTestData.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ScheduleTestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1085, 366);
            this.Controls.Add(this.gbParams);
            this.Controls.Add(this.tvRptFolders);
            this.Controls.Add(this.cmsGroup);
            this.Name = "ScheduleTestForm";
            this.Text = "User Alias Migration";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ScheduleTestForm_FormClosing);
            this.cmsGroup.ResumeLayout(false);
            this.cmsGroup.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.gbParams.ResumeLayout(false);
            this.gbParams.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvParams)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbsParamList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scheduleTestData)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox cmsGroup;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Button btnClose;
    private System.Windows.Forms.Label userLabel;
    private System.Windows.Forms.Label statusLabel;
    private System.Windows.Forms.Button cancelBtn;
    private System.Windows.Forms.Button loadBtn;
    private System.Windows.Forms.Label cmsStatus;
    private System.Windows.Forms.Button cmsLogon;
    private System.Windows.Forms.TextBox cmsPassword;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox cmsUser;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox cms;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.ImageList iList;
    private System.Windows.Forms.TreeView tvRptFolders;
    private System.Windows.Forms.TextBox txtInfo;
    private System.Windows.Forms.BindingSource dbsParamList;
    private ScheduleTestData scheduleTestData;
    private System.Windows.Forms.GroupBox gbParams;
    private System.Windows.Forms.ListBox lbLOV;
    private System.Windows.Forms.DataGridView dgvParams;
    private System.Windows.Forms.DataGridViewTextBoxColumn paramNameDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn paramValueDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn paramIDDataGridViewTextBoxColumn;
    private System.Windows.Forms.Label lblLOV;
  }
}

