﻿using CrystalDecisions.Enterprise;
using CrystalDecisions.Enterprise.Desktop;
using CrystalDecisions.Enterprise.Dest;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;

/// <summary>
/// Summary description for InfoObjUtils
/// </summary>
public partial class BOUtilities
{
    public enum DeliveryType
    {
        Inbox,
        Email
    }

    public enum ScheduleTypes
    {
        FirstMonday,
        Calendar,
        CalendarTemplate,
        Daily,
        Hourly,
        LastDay,
        Monthly,
        Now,
        NthDay,
        Weekly,
        WeekDay,
        RunOnce
    }

    public enum ExportAs
    {
        Webi,
        Excel,
        PDF,
        CSV
        //MHTML
    }

    public class ScheduleSetup
    {
        public ScheduleTypes Type;
        private List<DayOfWeek> dWeekDays;
        public DateTime BeginDate;
        public DateTime EndDate;
        public int DayInterval;
        public int HourInterval;
        public int MinuteInterval;
        public int MonthInterval;

        public ScheduleSetup()
        {
            DayInterval = 0;
            HourInterval = 0;
            MinuteInterval = 0;
            MonthInterval = 0;
        }

        public bool AddWeekDays(DayOfWeek addDay)
        {
            dWeekDays.Add(addDay);
            return true;
        }

        public List<DayOfWeek> WeekDays()
        {
            return dWeekDays;
        }
    }

    public struct ScheduledReportDelivery
    {
        public DeliveryType SendTo;
        public string EmailFrom;
        public string EmailTo;
        public string EmailCC;
        public string EmailSubject;
        public string Message;

        /// <summary>
        /// Create a ScheduledReportDelivery for delivery to Email.
        /// </summary>
        /// <param name="emailFrom"></param>
        /// <param name="emailTo"></param>
        /// <param name="emailCC"></param>
        /// <param name="emailSubject"></param>
        /// <param name="message"></param>
        public ScheduledReportDelivery(
            string emailFrom,
            string emailTo,
            string emailCC,
            string emailSubject,
            string message)
        {
            SendTo = DeliveryType.Email;
            EmailFrom = emailFrom;
            EmailTo = emailTo;
            EmailCC = emailCC;
            EmailSubject = emailSubject;
            Message = message;
        }

        /// <summary>
        /// Create a ScheduledReportDelivery for delivery to the users Inbox.
        /// </summary>
        public ScheduledReportDelivery(DeliveryType deliveryType)
        {
            SendTo = deliveryType;
            EmailFrom = string.Empty;
            EmailTo = string.Empty;
            EmailCC = string.Empty;
            EmailSubject = string.Empty;
            Message = string.Empty;
        }
    }

    private BOUserInfo _boInfoObject = null;

    public string ErrMsg { get; private set; }

    public BOUtilities(BOUserInfo infostore)
    {
        _boInfoObject = infostore;
    }

    public InfoObjects GetMailFolders(String userName, string folderType)
    {
        ErrMsg = string.Empty;
        string queryString;
        string sErr = string.Empty;
        if (_boInfoObject.Logon(out sErr))
        {
          if (String.IsNullOrEmpty(userName))
          {
            queryString = String.Format(BOQUERY_MAILFOLDERS_WITHOUT_USER, folderType);
          }
          else
          {
            queryString = String.Format(BOQUERY_MAILFOLDERS_WITH_USER, folderType, userName);
          }

          try
          {
            InfoObjects info = _boInfoObject.ExecuteQuery(queryString.ToString());
            return info;
          }
          finally
          {
            _boInfoObject.SessionLogOff();
          }
        }
        else
        {
          ErrMsg = sErr;
          return null;
        }
    }

    public InfoObjects GetMailFolders(string folderId)
    {
      //Session set in GetMailItems which then calls GetMailFolderIds which calls this
      //so we don't need Logon and SessionLogOut
      string sErr = string.Empty;
      string queryString = String.Format(BOQUERY_MAILFOLDERS_BY_FOLDER, folderId);
      InfoObjects info = null;
      try
      {
        info = _boInfoObject.ExecuteQuery(queryString.ToString());
      }
      catch (Exception ex)
      {
        ErrMsg = ex.Message;
      }
      return info;
    }

    public InfoObjects GetMailItems(InfoObjects mailFolders)
    {
      return GetMailItems(mailFolders, true);
    }

    public InfoObjects GetMailItems(InfoObjects mailFolders, bool logon)
    {
      ErrMsg = string.Empty;
      string sErr = string.Empty;
      InfoObjects mailItems = null;
      if (!logon || _boInfoObject.Logon(out sErr))
      {
        string idString = GetMailFolderIds(mailFolders);
        if (ErrMsg == string.Empty)
        {
          string queryString = String.Format(BOQUERY_MAILFOLDER_ITEM_QUERY, idString);

          try
          {
            mailItems = _boInfoObject.ExecuteQuery(queryString.ToString());
          }
          catch (Exception ex)
          {
            ErrMsg = ex.Message;
          }
          finally
          {
            if (logon) _boInfoObject.SessionLogOff();
          }
        }
      }
      else
      {
        ErrMsg = sErr;
      }
      return mailItems;
    }

    public List<BOReportInfo> GetMailItems(string folderId)
    {
        List<BOReportInfo> mailitems = new List<BOReportInfo>();
        InfoObjects imail;
        string sErr = string.Empty;
        ErrMsg = string.Empty;
        if (_boInfoObject.Logon(out sErr))
        {
          using (InfoObjects temp = GetMailFolders(folderId))
          {

            imail = GetMailItems(temp, false);
            if (ErrMsg == string.Empty)
            {
              foreach (InfoObject ReportInfo in imail)
              {
                mailitems.Add(new BOReportInfo(ReportInfo.ID, ReportInfo.CUID, ReportInfo.Title, ReportInfo.Description));
              }
            }
            _boInfoObject.SessionLogOff();
          }
        }
        return mailitems;
    }
  /*
    public static Dictionary<String, String> GetActiveMFAQuestions(String UserID)
    {
        //WS example
        MFAWS.MFAWS mfaws = new MFAWS.MFAWS();
        mfaws.Credentials = System.Net.CredentialCache.DefaultCredentials;
        mfaws.Url = System.Configuration.ConfigurationManager.AppSettings["MFAService"];
        Dictionary<String, String> dQuestions = new Dictionary<String, String>();

        try
        {
            DataSet questionsT = mfaws.GetUsersChallengeQuestions(UserID);
            foreach (DataRow question in questionsT.Tables[0].Rows)
            {
                //Add Question, Answer to dQuestions list
                dQuestions.Add(question[2].ToString(), question[0].ToString().Trim().ToLower());
            }
        }
        catch { }
        return dQuestions;
    }
  */
    public string GetMailFolderIds(InfoObjects mailFolders)
    {
      //Session set in GetMailItems so we don't need Logon and SessionLogOut
      StringBuilder idString = new StringBuilder();

        //build a list of inbox id's for the query
        foreach (InfoObject folder in mailFolders)
        {
            if (String.IsNullOrEmpty(idString.ToString()))
            {
                idString.Append(String.Format("'{0}'", folder.ID));
            }
            else
            {
                idString.Append(String.Format(",'{0}'", folder.ID));
            }
        }
        return idString.ToString();
    }

    public List<BOReportInfo> GetAllUsers()
    {
        List<BOReportInfo> _groupList = new List<BOReportInfo>();
        string sErr = string.Empty;
        ErrMsg = string.Empty;
        if (_boInfoObject.Logon(out sErr))
        {
            try
            {
                using (InfoObjects infoObjects = _boInfoObject.ExecuteQuery(BOQUERY_GET_ALL_USER_GROUPS))
                {
                    foreach (InfoObject infoObjectsGroup in infoObjects)
                    {
                        _groupList.Add(new BOReportInfo(infoObjectsGroup.ID, infoObjectsGroup.CUID, infoObjectsGroup.Title.Trim(),
                            infoObjectsGroup.Description));
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = ex.Message;
            }
            finally
            {
                _boInfoObject.SessionLogOff();
            }
        }
        else ErrMsg = sErr;
        return _groupList;
    }

    public List<BOReportInfo> GetAllUserGroups()
    {
      List<BOReportInfo> _groupList = new List<BOReportInfo>();
      string sErr = string.Empty;
      ErrMsg = string.Empty;
      if (_boInfoObject.Logon(out sErr))
      {
        try
        {
          using (InfoObjects infoObjects = _boInfoObject.ExecuteQuery(BOQUERY_GET_ALL_USER_GROUPS))
          {
            foreach (InfoObject infoObjectsGroup in infoObjects)
            {
              _groupList.Add(new BOReportInfo(infoObjectsGroup.ID, infoObjectsGroup.CUID, infoObjectsGroup.Title.Trim(),
                  infoObjectsGroup.Description));
            }
          }
        }
        catch (Exception ex)
        {
          ErrMsg = ex.Message;
        }
        finally
        {
          _boInfoObject.SessionLogOff();
        }
      }
      else ErrMsg = sErr;
      return _groupList;
    }

    public List<BOReportInfo> GetUserGroups(String UserName)
    {
      List<BOReportInfo> _groupList = new List<BOReportInfo>();
      string sErr = string.Empty;
      ErrMsg = string.Empty;
      if (_boInfoObject.Logon(out sErr))
      {
        try
        {
          // first we pull the groups the user is assigned to
          string UserQuery = String.Format(BOQUERY_GET_USER_GROUPS, UserName);

          using (InfoObjects infoObjects = _boInfoObject.ExecuteQuery(UserQuery))
          {
            foreach (InfoObject infoObjectsGroup in infoObjects)
            {
              _groupList.Add(new BOReportInfo(infoObjectsGroup.ID, infoObjectsGroup.CUID, infoObjectsGroup.Title.Trim(),
                  infoObjectsGroup.Description));
            }
          }
        }
        catch (Exception ex)
        {
          ErrMsg = ex.Message;
        }
        finally
        {
          _boInfoObject.SessionLogOff();
        }
      }
      else ErrMsg = sErr;
      return _groupList;
    }

    public List<BOReportInfo> GetUserGroups(String UserName, List<string> groups)
    {
      List<BOReportInfo> groupList = new List<BOReportInfo>();
      ErrMsg = string.Empty;
      string sErr = string.Empty;
      if (_boInfoObject.Logon(out sErr))
      {
        try
        {
          string UserQuery;
          string GroupWhereClause = string.Empty;

          foreach (string GroupName in groups)
          {
            GroupWhereClause += (GroupWhereClause.Length > 0 ? " OR " : string.Empty) + "si_name = '" + GroupName + "'";
          }

          // first we pull the groups the user is assigned to
          UserQuery = String.Format(BOQUERY_GET_UserInGroups, UserName, GroupWhereClause);

          using (InfoObjects infoObjects = _boInfoObject.ExecuteQuery(UserQuery))
          {
            foreach (InfoObject infoObjectsGroup in infoObjects)
            {
              groupList.Add(new BOReportInfo(infoObjectsGroup.ID, infoObjectsGroup.CUID, infoObjectsGroup.Title.Trim(),
                  infoObjectsGroup.Description));
            }
          }
        }
        catch (Exception ex)
        {
          ErrMsg = ex.Message;
        }
        finally
        {
          _boInfoObject.SessionLogOff();
        }
      }
      else ErrMsg = sErr;
      return groupList;
    }

    public static string GenerateRandomPassword(int minChars, int maxChars, int minNumbers, int minExtendedChars, int minLowercaseChars, int minUppercaseChars)
    {
        return GenerateRandomPassword(minChars, maxChars, minNumbers, minExtendedChars, minLowercaseChars, minUppercaseChars, "abcdefghijklmnopqrstuvwxyz", "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "0123456789", "~!#$%^*();:");
    }

    public static string GenerateRandomPassword(int minChars, int maxChars, int minNumbers, int minExtendedChars, int minLowercaseChars, int minUppercaseChars, string lowerCaseLetters, string uppercaseLetters, string numbers, string extendedCharacters)
    {
        Random RandInt = new Random(DateTime.UtcNow.Millisecond);
        StringBuilder NewPassword;
        int Numbers = 0;
        int ExtendedChars = 0;
        int LowercaseChars = 0;
        int UppercaseChars = 0;
        int CharsNeeded;

        CharsNeeded = RandInt.Next(minChars, maxChars);
        NewPassword = new StringBuilder();
        while (NewPassword.Length < CharsNeeded)
        {
            int CurrentCharType = RandInt.Next(0, 4);
            //Debug.WriteLine("CurrentCharType: " + CurrentCharType.ToString());
            switch (CurrentCharType)
            {
                case 1: // Number
                    Numbers++;
                    NewPassword.Append(numbers[RandInt.Next(0, numbers.Length)]);
                    break;

                case 2: // Extended characters
                    ExtendedChars++;
                    NewPassword.Append(extendedCharacters[RandInt.Next(0, extendedCharacters.Length)]);
                    break;

                case 3: // Letter (a-z)
                    LowercaseChars++;
                    NewPassword.Append(lowerCaseLetters[RandInt.Next(0, lowerCaseLetters.Length)]);
                    break;

                default: // Letter (A-Z)
                    UppercaseChars++;
                    NewPassword.Append(uppercaseLetters[RandInt.Next(0, uppercaseLetters.Length)]);
                    break;
            }
            if (NewPassword.Length == CharsNeeded &&
                minNumbers > Numbers &&
                minExtendedChars > ExtendedChars &&
                minLowercaseChars > LowercaseChars &&
                minUppercaseChars > UppercaseChars
                )
            {
                NewPassword = new StringBuilder();
                Numbers = 0;
                ExtendedChars = 0;
                LowercaseChars = 0;
                UppercaseChars = 0;
            }
        }
        return NewPassword.ToString();
    }
/*
    public string ResetUserPassword(String UserName, out string sErr)
    {
        sErr = string.Empty;
        string NewPassword = null;
        CrystalDecisions.Enterprise.Desktop.User user;
        BOUserInfo AdminUser = new BOUserInfo(); ;
        try
        {
            AdminUser.Logon(
                (string)ConfigurationManager.AppSettings["BOAdminUser"],
                (string)ConfigurationManager.AppSettings["BOAdminPwd"],
                (string)ConfigurationManager.AppSettings["BOServer"],
                (string)ConfigurationManager.AppSettings["BOAuthentication"], out sErr);

            //String query = "Select Top 1 * From CI_SYSTEMOBJECTS " + "Where SI_KIND='User' AND SI_NAME='" + SecurityEncoder.HtmlEncode(txtUsername.Text) + "'";
            string query = String.Format(BOQUERY_GET_USER, UserName);
            using (InfoObjects infoObjects = AdminUser.ExecuteQuery(query))
            {
                if (infoObjects != null && infoObjects.Count > 0)
                {
                    using (InfoObject infoObject = infoObjects[1])
                    {
                        user = (CrystalDecisions.Enterprise.Desktop.User)infoObject;
                        NewPassword = GenerateRandomPassword(8, 12, 1, 1, 2, 2, "abcdefghjkmnpqrstuvwxyz", "ABCDEFGHJKMNPQRSTUVWXYZ", "23456789", "~!#$%^*();:");
                        user.NewPassword = NewPassword;
                        user.ChangePasswordAtNextLogon = true;
                        user.AllowChangePassword = true;
                        AdminUser.GetInfoStore().Commit(user.ParentInfoObjects);
                        //ProcessEmail(NewPassword);
                    }
                }
            }
            AdminUser.SessionLogOff();
        }
        catch (Exception ex)
        {
            sErr = ex.Message;
        }
        return NewPassword;
    }

    public bool ChangeUserPassword(string userid, string oldpassword, string newpassword, out string sErr)
    {
        sErr = string.Empty;
        bool bPasswordChange = false;
        try
        {
            string UserQuery = String.Format(BOQUERY_GET_USER, userid);

            using (InfoObjects infoObjects = _boInfoObject.ExecuteQuery(UserQuery))
            {
                if (infoObjects.Count > 0)
                {
                    User BOUser = (User)infoObjects[1];
                    BOUser.ChangePassword(oldpassword, newpassword);
                    _boInfoObject.GetInfoStore().Commit(BOUser.ParentInfoObjects);
                    bPasswordChange = true;
                }
            }
        }
        catch (Exception ex)
        {
            sErr = ex.Message;
            bPasswordChange = false;
        }
        _boInfoObject.SessionLogOff(); //Logoff Session When done with InfoObjects
        return bPasswordChange;
    }
*/
    //Make Wrapper around new GetUserGroups function

    public bool IsUserInGroups(String UserName, List<string> groups)
    {
        List<BOReportInfo> infoObjects = GetUserGroups(UserName, groups);
        return (infoObjects.Count > 0);
    }

    /*
    public bool IsUserInGroups(string UserId, List<string> groups)
    {
        string UserQuery;
        string GroupWhereClause = string.Empty;

        foreach (string GroupName in groups)
        {
            GroupWhereClause += (GroupWhereClause.Length > 0 ? " OR " : string.Empty) + "si_name = '" + GroupName + "'";
        }

        // first we pull the groups the user is assigned to
        UserQuery = String.Format(BOQUERY_GET_UserInGroups, UserId, GroupWhereClause);

        InfoObjects infoObjects = ExecuteQuery(UserQuery);
        if (infoObjects.Count > 0)
        {
            return true;
        }
        return false;
    }
    */

    public Dictionary<string, string> BOGroupsForUser(String UserID)
    {
      Dictionary<string, string> BOGroups;
      string UserQuery;
      ErrMsg = string.Empty;
      string sErr = string.Empty;
      BOGroups = new Dictionary<string, string>();
      if (_boInfoObject.Logon(out sErr))
      {
        // first we pull the groups the user is assigned to
        UserQuery = String.Format(BOQUERY_GET_OBJECT_BY_NAME, UserID);

        try
        {
          using (InfoObjects infoObjects = _boInfoObject.ExecuteQuery(UserQuery))
          {
            using (InfoObject infoObject = infoObjects[1])
            {
              User user = (User)infoObject;

              Groups groups = user.Groups;

              foreach (int group in groups)
              {
                string GroupQuery = String.Format(BOQUERY_GET_OBJECT_NAME_BY_ID, group.ToString());
                using (InfoObjects infoObjectGroups = _boInfoObject.ExecuteQuery(GroupQuery))
                {
                  if (infoObjects.Count >= 1)
                  {
                    if (!BOGroups.ContainsKey(infoObjects[1].Title.Trim().ToLower()))
                      BOGroups.Add(infoObjects[1].Title.Trim().ToLower(), infoObjects[1].Title);
                  }
                }
              }
            }
          }
        }
        catch (Exception ex)
        {
          ErrMsg = ex.Message;
        }
        finally
        {
          _boInfoObject.SessionLogOff();
        }
      }
      else ErrMsg = sErr;
      return BOGroups;
    }

    public System.Data.DataTable GetScheduledReports(String UserID)
    {
        return GetScheduledReports(UserID, -1);
    }

    public System.Data.DataTable GetScheduledReports(String UserID, Int32 reportId)
    {
      string queryString;
      DataTable dt;
      ErrMsg = string.Empty;
      string sErr = string.Empty;

      dt = new DataTable();
      dt.Columns.Add(new DataColumn("ReportID", System.Type.GetType("System.Int32")));
      dt.Columns.Add(new DataColumn("ReportTitle", System.Type.GetType("System.String")));
      dt.Columns.Add(new DataColumn("OutputKind", System.Type.GetType("System.String")));
      dt.Columns.Add(new DataColumn("NextRunTime", System.Type.GetType("System.DateTime")));
      dt.Columns.Add(new DataColumn("Frequency", System.Type.GetType("System.String")));
      dt.Columns.Add(new DataColumn("LastUpdated", System.Type.GetType("System.DateTime")));
      dt.Columns.Add(new DataColumn("Destination", System.Type.GetType("System.String")));
      dt.Columns.Add(new DataColumn("EmailFrom", System.Type.GetType("System.String")));
      dt.Columns.Add(new DataColumn("EmailTo", System.Type.GetType("System.String")));
      dt.Columns.Add(new DataColumn("EmailSubject", System.Type.GetType("System.String")));
      dt.Columns.Add(new DataColumn("EmailMessage", System.Type.GetType("System.String")));
      dt.Columns.Add(new DataColumn("LastRun", System.Type.GetType("System.String")));
      dt.Columns.Add(new DataColumn("EndDate", System.Type.GetType("System.DateTime")));
      dt.Columns.Add(new DataColumn("Status", System.Type.GetType("System.String")));
      dt.Columns.Add(new DataColumn("StatusDescription", System.Type.GetType("System.String")));

      if (reportId != -1)
      {
          queryString = String.Format(BOQUERY_ScheduledReport, reportId);
      }
      else
      {
          queryString = String.Format(BOQUERY_ScheduledReports, UserID, GetFavoritesID(UserID));
      }
      if (_boInfoObject.Logon(out sErr))
      {
        try
        {
          using (InfoObjects ScheduledReports = _boInfoObject.ExecuteQuery(queryString.ToString()))
          {
            foreach (InfoObject ScheduledReport in ScheduledReports)
            {
              string LastUpdatedString;
              int ReportID;
              string ReportTitle;
              string OutputKind;
              DateTime NextRunTime;
              string Frequency;
              DateTime LastUpdated;
              string Destination = "Inbox";
              //DateTime LastRun;
              string LastRun;
              DateTime EndDate;
              string Status;
              string EmailFrom = string.Empty;
              string EmailTo = string.Empty;
              string EmailSubject = string.Empty;
              string EmailMessage = string.Empty;
              string StatusDescription = string.Empty;

              EndDate = ScheduledReport.SchedulingInfo.EndDate;
              ReportID = ScheduledReport.ID;
              ReportTitle = ScheduledReport.Parent.Title;
              OutputKind = ScheduledReport.Kind;
              NextRunTime = GetBOPropertyDate(ScheduledReport, "SI_NEXTRUNTIME");
              Frequency = GetScheduleTypeString(ScheduledReport.SchedulingInfo.Type);
              if (Frequency == "Weekly")
              {
                Frequency += " (" + GetDaysOfWeek(ScheduledReport.SchedulingInfo.CalendarRunDays).Replace(",", ", ") + ")";
              }
              LastUpdated = GetBOPropertyDate(ScheduledReport, "SI_UPDATE_TS");
              LastUpdatedString = FormatTS(GetBOPropertyDate(ScheduledReport, "SI_UPDATE_TS"));
              queryString = String.Format(BOQUERY_ScheduledReportLastRun, LastUpdatedString, UserID, ScheduledReport.Title);
              using (InfoObjects ScheduledReportLastRun = _boInfoObject.ExecuteQuery(queryString.ToString()))
              {
                if (ScheduledReportLastRun.Count >= 1)
                {
                  Int32 SI_STATUSINFO_ID;
                  Int32 SI_SUBST_STRINGS_ID;
                  Int32 FIRST_ERROR_ID;

                  //J.Ratledge 10.22.13 - changed LastRun assignment from .SchedulingInfo.EndDate to Get EndTime from Properties of InfoObjects
                  try
                  {
                    LastRun = (ScheduledReportLastRun[1].Properties["SI_ENDTIME"].ToString());
                  }
                  catch
                  {
                    LastRun = string.Empty;
                  }

                  Status = GetScheduleStatusString(ScheduledReportLastRun[1].SchedulingInfo.Status);

                  SI_STATUSINFO_ID = GetBOPropertyId(ScheduledReportLastRun[1].Properties, "SI_STATUSINFO");
                  if (SI_STATUSINFO_ID > 0)
                  {
                    SI_SUBST_STRINGS_ID = GetBOPropertyId(
                        ScheduledReportLastRun[1].Properties[SI_STATUSINFO_ID].Properties, "SI_SUBST_STRINGS");
                    if (SI_SUBST_STRINGS_ID > 0)
                    {
                      FIRST_ERROR_ID =
                          GetBOPropertyId(
                              ScheduledReportLastRun[1].Properties[SI_STATUSINFO_ID].Properties[SI_SUBST_STRINGS_ID]
                                  .Properties, "1");
                      if (FIRST_ERROR_ID > 0)
                      {
                        StatusDescription =
                            ScheduledReportLastRun[1].Properties[SI_STATUSINFO_ID].Properties[SI_SUBST_STRINGS_ID]
                                .Properties[FIRST_ERROR_ID].Value.ToString();
                      }
                    }
                  }
                }
                else
                {
                  LastRun = DateTime.MinValue.ToString();
                  Status = "Unknown";
                }
              } //end using
              if (ScheduledReport.SchedulingInfo.Destinations.Count > 0)
              {
                for (int x = 1; x <= ScheduledReport.SchedulingInfo.Destinations.Count; x++)
                {
                  if (ScheduledReport.SchedulingInfo.Destinations[x].Name.ToLower() == "crystalenterprise.smtp")
                  {
                    InfoObject SmtpObject = null;
                    InfoObjects SmtpObjects = null;
                    try
                    {
                      Destination = "Email";

                      //SmtpObjects = _InfoStore.Query("Select SI_DEST_SCHEDULEOPTIONS, SI_PROGID From CI_SYSTEMOBJECTS Where SI_PARENTID=29 and SI_NAME='CrystalEnterprise.SMTP'");
                      SmtpObjects = _boInfoObject.ExecuteQuery(
                              "Select SI_DEST_SCHEDULEOPTIONS, SI_PROGID From CI_SYSTEMOBJECTS Where SI_PARENTID=29 and SI_NAME='CrystalEnterprise.SMTP'");

                      SmtpObject = SmtpObjects[1];
                      ScheduledReport.SchedulingInfo.Destinations[x].CopyToPlugin(
                          (DestinationPlugin)SmtpObject.PluginInterface);
                      Smtp ceSMTP = new Smtp(SmtpObject.PluginInterface);
                      SmtpOptions ceSMTPOpts = new SmtpOptions(ceSMTP.ScheduleOptions);
                      if (ceSMTPOpts != null)
                      {
                        if (ceSMTPOpts.SenderAddress != null)
                        {
                          EmailFrom = ceSMTPOpts.SenderAddress;
                        }
                        foreach (string ToAddress in ceSMTPOpts.ToAddresses)
                        {
                          EmailTo += (EmailTo.Length > 0 ? "," : string.Empty) + ToAddress;
                        }
                        EmailSubject = ceSMTPOpts.Subject;
                        EmailMessage = ceSMTPOpts.Message;
                      }
                    }
                    catch
                    {
                      //todo: Need to log this error
                      EmailFrom = string.Empty;
                      EmailTo = string.Empty;
                      EmailSubject = string.Empty;
                      EmailMessage = string.Empty;
                    }
                    finally
                    {
                      if (SmtpObject != null) SmtpObject.Dispose();
                      if (SmtpObjects != null) SmtpObjects.Dispose();
                    }
                  }
                }
              }

              dt.Rows.Add(new object[] { ReportID, ReportTitle, OutputKind, NextRunTime, Frequency, LastUpdated, Destination, EmailFrom, EmailTo, EmailSubject, EmailMessage, LastRun, EndDate, Status, StatusDescription });
            }
          }
        }
        catch (Exception ex)
        {
          ErrMsg = ex.Message;
        }
        finally
        {
          _boInfoObject.SessionLogOff(); //Logoff Session When done with InfoObjects
        }
      }
      else ErrMsg = sErr;
      return dt;
    }

    public string GetScheduleStatusString(CeScheduleStatus scheduleStatus)
    {
        switch (scheduleStatus)
        {
            case CeScheduleStatus.ceStatusFailure:
                return "Failure";

            case CeScheduleStatus.ceStatusPaused:
                return "Paused";

            case CeScheduleStatus.ceStatusPending:
                return "Pending";

            case CeScheduleStatus.ceStatusRunning:
                return "Running";

            case CeScheduleStatus.ceStatusSuccess:
                return "Success";
        }
        return "Unknown";
    }

    public string GetScheduleTypeString(CeScheduleType scheduleType)
    {
        switch (scheduleType)
        {
            case CeScheduleType.ceScheduleType1stMonday:
                return "First Monday of Month";

            case CeScheduleType.ceScheduleTypeCalendar:
                return "Weekly";

            case CeScheduleType.ceScheduleTypeCalendarTemplate:
                return "Calendar Template";

            case CeScheduleType.ceScheduleTypeDaily:
                return "Daily";

            case CeScheduleType.ceScheduleTypeHourly:
                return "Hourly";

            case CeScheduleType.ceScheduleTypeLastDay:
                return "Last Day of Month";

            case CeScheduleType.ceScheduleTypeMonthly:
                return "Monthly";

            case CeScheduleType.ceScheduleTypeNthDay:
                return "Nth Day of Month";

            case CeScheduleType.ceScheduleTypeOnce:
                return "Run Once";

            case CeScheduleType.ceScheduleTypeWeekly:
                return "Weekly";
        }
        return "Unknown";
    }

    private InfoObject GetInbox(String UserName, bool logon)
    {
      string queryString = String.Format(BOQUERY_USERS_INBOX, UserName);
      InfoObject retObject = null;
      ErrMsg = string.Empty;
      string sErr = string.Empty;
      if (!logon || _boInfoObject.Logon(out sErr))
      {
        try
        {
          using (InfoObjects InboxFolderRecord = _boInfoObject.ExecuteQuery(queryString.ToString()))
          {
            if (InboxFolderRecord != null && InboxFolderRecord.Count >= 1)
            {
              retObject = InboxFolderRecord[1];
            }
          }
        }
        catch (Exception ex)
        {
          ErrMsg = ex.Message;
        }
        finally
        {
          if (logon) 
            _boInfoObject.SessionLogOff();
        }
      }
      else
      {
        if (logon)
          ErrMsg = sErr;
      }
      return retObject;
    }

    public Int32 GetInboxID(String UserName, bool logon)
    {
        InfoObject InboxFolder;

        InboxFolder = GetInbox(UserName, logon);
        if (InboxFolder != null)
        {
            return InboxFolder.ID;
        }
        return 0;
    }

    private InfoObject GetFavorites(String UserID)
    {
      string queryString = String.Format(BOQUERY_USERS_FAVORITES, UserID);
      InfoObject tInfoObject = null;
      ErrMsg = string.Empty;
      string sErr = string.Empty;
      try
      {
        using (InfoObjects FavoritesFolderRecord = _boInfoObject.ExecuteQuery(queryString))
        {
          if (FavoritesFolderRecord != null && FavoritesFolderRecord.Count >= 1)
          {
            tInfoObject = FavoritesFolderRecord[1];
          }
        }
      }
      catch (Exception ex)
      {
        ErrMsg = ex.Message;
      }
      finally
      {
        _boInfoObject.SessionLogOff(); //Logoff Session When done with InfoObjects
      }
      return tInfoObject;
    }

    public Dictionary<string, string> GetFavoriteReportList(String UserID)
    {
      string queryString;
      Dictionary<string, string> favList = new Dictionary<string, string>();
      ErrMsg = string.Empty;
      string sErr = string.Empty;
      if (_boInfoObject.Logon(out sErr))
      {
        try
        {
          InfoObject FavFolder = GetFavorites(UserID);
          if (ErrMsg == string.Empty)
          {
            queryString = String.Format(BOQUERY_REPORTS_IN_FOLDER, FavFolder.ID);
            using (InfoObjects ScheduledReports = _boInfoObject.ExecuteQuery(queryString.ToString()))
            {
              foreach (InfoObject ScheduledReport in ScheduledReports)
              {
                if (!favList.ContainsKey(ScheduledReport.Title.Trim().ToLower()))
                {
                  favList.Add(ScheduledReport.Title.Trim().ToLower(), ScheduledReport.ID.ToString());
                }
              }
            }
          }
        }
        catch (Exception ex)
        {
          ErrMsg = ex.Message;
        }
        finally
        {
          _boInfoObject.SessionLogOff(); //Logoff Session When done with InfoObjects
        }
      }
      else ErrMsg = sErr;
      return favList;
    }

    public Int32 GetFavoritesID(String UserID)
    {
      Int32 tInt = 0; //Default Return
      ErrMsg = string.Empty;
      string sErr = string.Empty;
      if (_boInfoObject.Logon(out sErr))
      {
        using (InfoObject FavoritesFolder = GetFavorites(UserID))
        {
          if (ErrMsg == string.Empty)
          {
            if (FavoritesFolder != null)
            {
              tInt = FavoritesFolder.ID;
            }
          }
        }
      }
      else ErrMsg = sErr;
        return tInt;
    }

    private string GetBOPropertyString(CrystalDecisions.Enterprise.Properties properties, string bOPropertyName)
    {
        foreach (CrystalDecisions.Enterprise.Property p in properties)
        {
            if (p.Name.Trim().ToLower() == bOPropertyName.Trim().ToLower())
            {
                return p.Value.ToString();
            }
        }
        return null;
    }
/*
    public static byte[] GetNsightSetupImage(String ClientID, string ImageName)
    {
        return BODataAccess.NsightClientSetupImage(ClientID, ImageName);
    }
*/
    private Int32 GetBOPropertyId(CrystalDecisions.Enterprise.Properties properties, string bOPropertyName)
    {
        for (Int32 PropertyId = 1; PropertyId <= properties.Count; PropertyId++)
        {
            if (properties[PropertyId].Name.Trim().ToLower() == bOPropertyName.Trim().ToLower())
            {
                return PropertyId;
            }
        }
        return -1;
    }

    private string FormatTS(DateTime dateTime)
    {
        return dateTime.ToString("yyyy.MM.dd.HH.mm.ss");
    }

    public DateTime GetBOPropertyDate(InfoObject infoObject, string bOPropertyName)
    {
        DateTime PropertyReturn = DateTime.MinValue;

        foreach (CrystalDecisions.Enterprise.Property p in infoObject.Properties)
        {
            if (p.Name.Trim().ToLower() == bOPropertyName.Trim().ToLower())
            {
                if (!DateTime.TryParse(p.Value.ToString(), out PropertyReturn))
                {
                    PropertyReturn = DateTime.MinValue;
                }
            }
        }
        return PropertyReturn;
    }

    public bool DeleteReport(Int32 ReportId, string userName)
    {
      InfoObject Report = null;
      InfoObjects ReportRecord = null;
      bool retval = false;
      ErrMsg = string.Empty;
      string sErr = string.Empty;
      if (_boInfoObject.Logon(out sErr))
      {
        try
        {
          string queryString = String.Format(BOQUERY_REPORT_BY_ID_TYPE_OWNER, ReportId, userName);

          using (ReportRecord = _boInfoObject.ExecuteQuery(queryString))
          {
            if (ReportRecord != null && ReportRecord.Count >= 1)
            {
              using (Report = ReportRecord[1]) Report.DeleteNow();
            }
          }
          retval = true;
        }
        catch (Exception ex)
        {
          ErrMsg = ex.Message;
        }
        finally
        {
          _boInfoObject.SessionLogOff(); //Logoff Session When done with InfoObjects
        }
      }
      else ErrMsg = sErr;
      return retval;
    }

    public bool DeleteSchedule(Int32 ReportID, Int32 ScheduleID)
    {
        bool bRet = false;
        try
        {
            _boInfoObject.RESTfulLogon();
            _boInfoObject.DeleteRESTfulXML("/raylight/v1/documents/" + ReportID.ToString() + "/schedules/" + ScheduleID.ToString());
            _boInfoObject.SessionLogOff();
        }
        catch { }
        return bRet;
    }

    public Int32 GetUserId(string userName)
    {
      return GetUserId(userName, true);
    }

    public Int32 GetUserId(string userName, bool logon)
    {
      int iID = 0;
      ErrMsg = string.Empty;
      string sErr = string.Empty;
      if (!logon || _boInfoObject.Logon(out sErr))
      {
        string queryFolder = string.Format(BOQUERY_GET_OBJECT_BY_NAME, userName);
        try
        {
          using (InfoObjects infoObjects = _boInfoObject.ExecuteQuery(queryFolder))
          {
            // Verify that the object was found
            if (infoObjects.Count > 0)
              iID = infoObjects[1].ID;
          }
        }
        catch (Exception ex)
        {
          ErrMsg = ex.Message;
        }
        finally
        {
          if (logon)
            _boInfoObject.SessionLogOff();
        }
      }
      else ErrMsg = sErr;
      return iID;
    }

    public DateTime GetUserPasswordChangeDate(string userName)
    {
      string queryFolder = string.Format(BOQUERY_GET_USER_LAST_PASSWORD_TIME, userName);
      DateTime lastPasswordChangeTime = new DateTime();
      ErrMsg = string.Empty;
      string sErr = string.Empty;
      if (_boInfoObject.Logon(out sErr))
      {
        try
        {
          using (InfoObjects infoObjects = _boInfoObject.ExecuteQuery(queryFolder.ToString()))
          {
            // Verify that the object was found
            if (infoObjects.Count > 0)
              lastPasswordChangeTime = Convert.ToDateTime(infoObjects[1].Properties["SI_LAST_PASSWORD_CHANGE_TIME"].ToString());
          }
        }
        catch (Exception ex)
        {
          ErrMsg = ex.Message;
        }
        finally
        {
          _boInfoObject.SessionLogOff();
        }
      }
      else ErrMsg = sErr;
      return lastPasswordChangeTime;
    }

    public Int32 CopyReportToInbox(Int32 ReportId, string userName)
    {
      string queryFolder = string.Format("Select * From CI_INFOOBJECTS Where SI_ID = {0}", ReportId);
      Int32 tInt = -1;
      ErrMsg = string.Empty;
      string sErr = string.Empty;
      if (_boInfoObject.Logon(out sErr))
      {
        try
        {
          using (InfoObjects infoObjects = _boInfoObject.ExecuteQuery(queryFolder))
          {
            // Verify that the object was found
            if (infoObjects.Count > 0)
            {
              InfoObject infoObject = infoObjects[1];
              InfoObject infoObjectCopy = infoObjects.Copy(infoObject, CeCopyObject.ceCopyNewObjectNewFiles);
              infoObjectCopy.ParentID = Convert.ToInt32(GetFavoritesID(_boInfoObject.UserName));
              infoObjectCopy.Properties.Add("SI_OWNERID", GetUserId(userName));
              infoObjectCopy.Description = string.Empty;
              infoObjectCopy.Title = infoObjectCopy.Title + " " + infoObjectCopy.ID.ToString();
              try
              {
                infoObjectCopy.Save();
              }
              catch (Exception ex)
              {
                tInt = -1;
                ErrMsg = ex.Message;
              }
              tInt = infoObjectCopy.ID;
            }
          }
        }
        catch (Exception ex)
        {
          ErrMsg = ex.Message;
        }
        finally
        {
          _boInfoObject.SessionLogOff();
        }
      }
      else ErrMsg = sErr;
      return tInt;
    }

    private string SetDestination(ScheduledReportDelivery ScheduledReportDelivery, ExportAs ExportAs)
    {
        string sFormat = string.Empty;
        string sDestination = string.Empty;

        if (ScheduledReportDelivery.SendTo == DeliveryType.Inbox && ExportAs != BOUtilities.ExportAs.CSV)
        {
            sFormat = "<format type=\"webi\" />";
        }
        else
        {
            switch (ExportAs)
            {
                case BOUtilities.ExportAs.Excel:
                    sFormat = "<format type=\"xls\" />";
                    break;

                case BOUtilities.ExportAs.Webi:
                    sFormat = "<format type=\"webi\" />";
                    break;

                case BOUtilities.ExportAs.CSV:
                    sFormat = "<format type=\"csv\">" +
                        "<properties><property key=\"textQualifier\">\"</property>" +
                        "<property key=\"columnDelimiter\">,</property>" +
                        "<property key=\"charset\">UTF-8</property>" +
                        "<property key=\"onePerDataProvider\">false</property></properties></format>";
                    break;

                default:
                    sFormat = "<format type=\"pdf\" />";
                    break;
            }
        }

        if (ScheduledReportDelivery.SendTo == DeliveryType.Inbox)
        {
            //int InboxId = GetInboxID(_boInfoObject.UserName, false);
            //sDestination = "<inbox><to>" + InboxId.ToString() + "</to><sendAs type=\"shortcut\" /></inbox>";
            sDestination = "<inbox />";
        }
        else
        {
            sDestination = "<mail><from>" + ScheduledReportDelivery.EmailFrom + "</from>" +
                "<to>" + ScheduledReportDelivery.EmailTo + "</to>" +
                "<cc>" + ScheduledReportDelivery.EmailCC + "</cc>" +
                "<subject>" + ScheduledReportDelivery.EmailSubject + "</subject>" +
                "<message>" + ScheduledReportDelivery.Message + "</message>" +
                "<addAttachment>true</addAttachent></mail>";
        }

        string schInfo = sFormat + "<destination>" + sDestination + "</destination>";
        return schInfo;
    }

    public void SetSchedule(int ReportId, int OrigReportId, ExportAs ExportFormat, ScheduledReportDelivery ScheduledReportDelivery, ScheduleSetup scheduleSetup, Dictionary<string, List<string>> promptList)
    {
      string sReportName = string.Empty;
      StringBuilder queryFolder = new StringBuilder();
      queryFolder.AppendFormat(BOQUERY_REPORT_BY_ID, ReportId);
      ErrMsg = string.Empty;
      string sErr = string.Empty;
      if (_boInfoObject.Logon(out sErr))
      {
        try
        {
          using (InfoObjects infoObjects = _boInfoObject.ExecuteQuery(queryFolder.ToString()))
          {
            // Verify that the object was found
            if (infoObjects.Count > 0)
              using (InfoObject io = infoObjects[1]) sReportName = io.Title;
          }
        }
        catch (Exception ex)
        {
          ErrMsg = ex.Message;
        }
        string sStartDate = scheduleSetup.BeginDate.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffK"); //Set Date in YYYY-MM-DDTHH:MM:SS format + offset

        string sEndDate = scheduleSetup.EndDate.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffK");//Set Date in YYYY-MM-DDTHH:MM:SS format + offset

        string sFreq = string.Empty;
        switch (scheduleSetup.Type)
        {
          case ScheduleTypes.NthDay:
            sFreq = "<nthDayOfMonth  retriesAllowed=\"0\" retryIntervalInSeconds=\"60\">" +
                "<startdate>" + sStartDate + "</startdate><enddate>" + sEndDate + "</enddate>" +
                "<day>" + scheduleSetup.DayInterval.ToString() + "</day></nthDayOfMonth >";
            break;

          case ScheduleTypes.Daily:
            sFreq = "<daily retriesAllowed=\"0\" retryIntervalInSeconds=\"60\">" +
                "<startdate>" + sStartDate + "</startdate><enddate>" + sEndDate + "</enddate>" +
                "<dayinterval>" + scheduleSetup.DayInterval.ToString() + "</dayinterval></daily>";
            break;

          case ScheduleTypes.Monthly:
            sFreq = "<monthly retriesAllowed=\"0\" retryIntervalInSeconds=\"60\">" +
            "<startdate>" + sStartDate + "</startdate><enddate>" + sEndDate + "</enddate>" +
            "<month>" + scheduleSetup.MonthInterval.ToString() + "</month></monthly>";
            break;

          case ScheduleTypes.Hourly:
            sFreq = "<hourly retriesAllowed=\"0\" retryIntervalInSeconds=\"60\">" +
            "<startdate>" + sStartDate + "</startdate><enddate>" + sEndDate + "</enddate>" +
            "<hour>" + scheduleSetup.HourInterval.ToString() + "</hour><minute>" + scheduleSetup.MinuteInterval.ToString() + "</minute></hourly>";
            break;

          case ScheduleTypes.LastDay:
            sFreq = "<lastDayOfMonth retriesAllowed=\"0\" retryIntervalInSeconds=\"60\">" +
            "<startdate>" + sStartDate + "</startdate><enddate>" + sEndDate + "</enddate></lastDayOfMonth >";
            break;

          case ScheduleTypes.FirstMonday:
            sFreq = "<firstMondayOfMonth retriesAllowed=\"0\" retryIntervalInSeconds=\"60\">" +
           "<startdate>" + sStartDate + "</startdate><enddate>" + sEndDate + "</enddate></firstMondayOfMonth  >";
            break;

          case ScheduleTypes.Weekly:
            sFreq = "<weekly retriesAllowed=\"0\" retryIntervalInSeconds=\"60\">" +
            "<startdate>" + sStartDate + "</startdate><enddate>" + sEndDate + "</enddate>";
            foreach (DayOfWeek dTest in scheduleSetup.WeekDays())
            {
              if (dTest == DayOfWeek.Monday)
              {
                sFreq = sFreq + "<monday>monday</monday>";
              }
              if (dTest == DayOfWeek.Tuesday)
              {
                sFreq = sFreq + "<tuesday>tuesday</tuesday>";
              }
              if (dTest == DayOfWeek.Wednesday)
              {
                sFreq = sFreq + "<wednesday>wednesday</wednesday>";
              }
              if (dTest == DayOfWeek.Thursday)
              {
                sFreq = sFreq + "<thursday>thursday</thursday>";
              }
              if (dTest == DayOfWeek.Friday)
              {
                sFreq = sFreq + "<friday>friday</friday>";
              }
              if (dTest == DayOfWeek.Saturday)
              {
                sFreq = sFreq + "<saturday>saturday</saturday>";
              }
              if (dTest == DayOfWeek.Sunday)
              {
                sFreq = sFreq + "<sunday>sunday</sunday>";
              }
            }
            sFreq = sFreq + "</weekly>";
            break;

          case ScheduleTypes.RunOnce:
            sFreq = "<once retriesAllowed=\"0\" retryIntervalInSeconds=\"60\">" +
           "<startdate>" + sStartDate + "</startdate><enddate>" + sEndDate + "</enddate></once>";
            break;
          case ScheduleTypes.Now:
            sFreq = string.Empty;
            break;
        }
        try
        {
          _boInfoObject.RESTfulLogon();
          XmlDocument xPrompts = GetReportPrompts(ReportId.ToString(), false);
          XmlNodeList paramNodes = xPrompts.SelectNodes("//parameter");

          // Verify that the object was found
          string sPrompts = string.Empty;
          if (paramNodes.Count > 0)
          {
            //Fill in the prompt values
            if (promptList.Count > 0)
            {  //For each <parameter>
              for (int i = 0; i < paramNodes.Count; i++)
              {
                XmlNode xPrompt = paramNodes[i];

                //Remove <value> tags from the <values> node
                XmlNodeList xValues = xPrompt.SelectNodes("//values");
                for (int j = xValues.Count - 1; j >= 0; j--)
                  xValues[j].ParentNode.RemoveChild(xValues[j]);

                BuildParmValues(OrigReportId, promptList, ref xPrompt);
              }
            }
            sPrompts = "<parameters>";
            foreach (XmlNode node in paramNodes)
            {
              sPrompts += "<parameter ";
              foreach (XmlAttribute attr in node.Attributes)
              {
                sPrompts += attr.Name + "=\"" + attr.Value + "\" ";
              }
              sPrompts += ">" + node.InnerXml + "</parameter>";
            }
            sPrompts += "</parameters>";
          }

          string schInfo = "<schedule><name>" + sReportName + "</name>" +
          SetDestination(ScheduledReportDelivery, ExportFormat) + sFreq + sPrompts + "</schedule>";
          _boInfoObject.RESTfulLogon();
          sErr = string.Empty;
          _boInfoObject.PutRESTfulXML("/raylight/v1/documents/" + ReportId.ToString() + "/schedules/", schInfo, out sErr);
          if (sErr != string.Empty)
            ErrMsg = sErr;
        }
        finally
        {
          _boInfoObject.SessionLogOff();
        }
      }
      else ErrMsg = sErr;
    }

    //TODO: Check on usage of PopulateWebiPrompts
    /*
    public static void PopulateWebiPrompts(IPrompts promptFields, Webi report)
    {
        Properties properties1 = report.ProcessingInfo.Properties;

        try
        {
            properties1.Delete("SI_WEBI_PROMPTS");
        }
        catch (COMException e)
        {
            if (e.ErrorCode != -2147210697)
            {
                throw;
            }
        }

        Properties properties2 = properties1.Add("SI_WEBI_PROMPTS", string.Empty, CePropFlags.cePropFlagBag).Properties;
        properties2.Add("SI_TOTAL", promptFields.Count);

        for (int i1 = 0; i1 < promptFields.Count; i1++)
        {
            IPrompt iPrompt = promptFields[i1];
            int j = i1 + 1;
            Properties properties3 = properties2.Add(j.ToString(), string.Empty, CePropFlags.cePropFlagBag).Properties;
            properties3.Add("SI_NAME", iPrompt.Name);
            Properties properties4 = properties3.Add("SI_VALUES", string.Empty, CePropFlags.cePropFlagBag).Properties;
            Array array = iPrompt.GetCurrentValues();
            properties4.Add("SI_TOTAL", array.Length);

            for (int k = 0; k < array.Length; k++)
            {
                j = k + 1;
                properties4.Add(j.ToString(), array.GetValue(k));
            }

            Properties properties5 = properties3.Add("SI_DATAPROVIDERS", string.Empty, CePropFlags.cePropFlagBag).Properties;
            IDataProviders iDataProviders = iPrompt.GetDataProviders();
            properties5.Add("SI_TOTAL", iDataProviders.Count);

            for (int i2 = 0; i2 < iDataProviders.Count; i2++)
            {
                IDataProvider iDataProvider = iDataProviders[i2];
                j = i2 + 1;
                Properties properties6 = properties5.Add(j.ToString(), string.Empty, CePropFlags.cePropFlagBag).Properties;
                properties6.Add("SI_NAME", iDataProvider.Name);
                properties6.Add("SI_ID", iDataProvider.ID);
            }
        }
    } */

    public Dictionary<string, Int32> GetReportsInFolder(Int32 FolderId)
    {
      Dictionary<string, Int32> Reports;
      Reports = new Dictionary<string, int>();
      string queryFolder = string.Format(BOQUERY_REPORTS_IN_FOLDER, FolderId);

      ErrMsg = string.Empty;
      string sErr = string.Empty;
      if (_boInfoObject.Logon(out sErr))
      {
        try
        {
          using (InfoObjects infoObjects = _boInfoObject.ExecuteQuery(queryFolder.ToString()))
          {
            // Verify that the object was found
            if (infoObjects.Count > 0)
            {
              foreach (InfoObject io in infoObjects)
              {
                if (!Reports.ContainsKey(io.Title))
                  Reports.Add(io.Title, io.ID);
              }
            }
          }
        }
        catch (Exception ex)
        {
          ErrMsg = ex.Message;
        }
        finally
        {
          _boInfoObject.SessionLogOff();
        }
      }
      else ErrMsg = sErr;
      return Reports;
    }

    public Dictionary<string, string> GetReport(Int32 ReportId)
    {
      Dictionary<string, string> ReportParams;
      ReportParams = new Dictionary<string, string>();
      string queryFolder = string.Format(BOQUERY_REPORT_BY_ID, ReportId);

      ErrMsg = string.Empty;
      string sErr = string.Empty;
      if (_boInfoObject.Logon(out sErr))
      {
        try
        {
          using (InfoObjects infoObjects = _boInfoObject.ExecuteQuery(queryFolder.ToString()))
          {
            // Verify that the object was found
            if (infoObjects.Count > 0)
            {
              using (InfoObject io = infoObjects[1])
              {
                ReportParams.Add("ID", io.ID.ToString());
                ReportParams.Add("Title", io.Title);
                using (SchedulingInfo si = io.SchedulingInfo)
                {
                  ReportParams.Add("Schedule-BeginDate", si.BeginDate.ToString());
                  ReportParams.Add("Schedule-EndDate", si.EndDate.ToString());
                  ReportParams.Add("Schedule-ScheduleType", si.Type.ToString());
                  ReportParams.Add("Schedule-IntervalHours", si.IntervalHours.ToString());
                  ReportParams.Add("Schedule-IntervalMinutes", si.IntervalMinutes.ToString());
                  ReportParams.Add("Schedule-IntervalMonths", si.IntervalMonths.ToString());
                  ReportParams.Add("Schedule-IntervalNthDay", si.IntervalNthDay.ToString());
                  ReportParams.Add("Schedule-IntervalDays", si.IntervalDays.ToString());
                }
              }
            }
          }
        }
        catch (Exception ex)
        {
          ErrMsg = ex.Message;
        }
        finally
        {
          _boInfoObject.SessionLogOff();
        }
      }
      else ErrMsg = sErr;
      return ReportParams;
    }

    /// <summary>
    /// Gets a report with the promts (input parameters)
    /// </summary>
    /// <param name="ReportID"></param>
    /// <param name="defaultValues"></param>
    /// <param name="LOVValues"></param>
    /// <returns></returns>
    /// GetReportWithPrompts
    public void GetReportLOVValues(Int32 ReportID, ref Dictionary<string, string> defaultValues, ref Dictionary<string, List<string>> LOVValues)
    {
        XmlDocument xPrompts = GetReportPrompts(ReportID.ToString(), true);
        XmlNodeList paramNodes = xPrompts.SelectNodes("//parameter");

        for (int i = 0; i < paramNodes.Count; i++)
        {
            XmlNode param = paramNodes[i];

            XmlNodeList optList = param.SelectNodes("answer/values");
           List<string> LOVList = new List<string>();

            string sName = string.Empty;
            try { sName = param.SelectSingleNode("name").Value; }
            catch { }

            //for default values - just getting the first one for now... there are currently none with multi
            if (optList.Count > 0)
                defaultValues.Add(sName, optList[0].Value);

            optList = param.SelectNodes("answer/info/lov/values");
            //list of values
            if (optList.Count > 0)
            {
                for (int j = 0; j < optList.Count; j++)
                {
                    LOVList.Add(optList[j].Value);
                }
                LOVValues.Add(sName, LOVList);
            }
        }
    }

  //7/1 - Dell - Changes here because we don't always want the LOV info
    public XmlDocument GetReportPrompts(String ReportID, bool getLOVInfo)
    {
      ErrMsg = string.Empty;
      XmlDocument xParams = null;
      _boInfoObject.RESTfulLogon();
      xParams = _boInfoObject.GetRESTfulXML("/raylight/v1/documents/" + ReportID + "/parameters?lovInfo=" + getLOVInfo.ToString().ToLower());
      return xParams;
    }

    public XmlDocument GetReport(String ReportID)
    {
      XmlDocument xReport = null;
      ErrMsg = string.Empty;
      string sErr = string.Empty;
      if (_boInfoObject.Logon(out sErr))
      {
        _boInfoObject.RESTfulLogon();
        xReport = _boInfoObject.GetRESTfulXML("/raylight/v1/documents/" + ReportID);
        _boInfoObject.SessionLogOff();
      }
      else ErrMsg = sErr;
      return xReport;
    }

    public List<PromptOptions> GetReportPrompts(String SchoolUserID, Int32 ReportId, ref Dictionary<string, string> defaultValueList, ref Dictionary<string, List<string>> LOVList)
    {
        XmlDocument xPrompts = GetReportPrompts(ReportId.ToString(), true);
        XmlNodeList paramNodes = xPrompts.SelectNodes("//parameter");

        List<PromptOptions> promptList = new List<PromptOptions>();
        PromptOptions promptInfo;
        //NsightUtilities NU = new NsightUtilities();

      ErrMsg = string.Empty;
      string sErr = string.Empty;
      if (_boInfoObject.Logon(out sErr))
      {
        try
        {
          for (int iPrompt = 0; iPrompt < paramNodes.Count; iPrompt++)
          {
            promptInfo = new PromptOptions();
            XmlNode param = paramNodes[iPrompt];
            XmlNode opt;
            if (param.Attributes["optional"].Value.Equals("true")) promptInfo.IsOptional = true;
            else promptInfo.IsOptional = false;

            try
            {
              opt = param.SelectSingleNode("id");
              promptInfo.Order = Convert.ToInt32(opt.InnerText) + 1;
            }
            catch { promptInfo.Order = 0; } //Set 0 for default if <id> is invalid

            opt = param.SelectSingleNode("name");
            promptInfo.Question = opt.InnerText;

            opt = param.SelectSingleNode("answer");
            promptInfo.PromptType = opt.Attributes["type"].Value;

            //Set Initial LOV
            XmlNodeList optLOV = param.SelectNodes("answer/info/lov/values");
            foreach (XmlNode nLOV in optLOV)
            {
              promptInfo.LOV.Add(nLOV.InnerText);
            }

            if (defaultValueList.ContainsKey(promptInfo.Question))
              promptInfo.DefaultValue = (promptInfo.PromptType == "0" ? DateTime.Parse(defaultValueList[promptInfo.Question]).ToShortDateString() : defaultValueList[promptInfo.Question]);
            if (LOVList.ContainsKey(promptInfo.Question))
              promptInfo.LOV = LOVList[promptInfo.Question];

            //hard code how the school codes are done by the question name - if the list of schools are not populating check here
            /*            if (promptInfo.Question.Substring(0, 11) == "Enter OPEID")
                        {
                            List<string> schoolList = new List<string>();

                            foreach (DataRow row in NU.GetSchoolIDList(SchoolUserID).Rows)
                            {
                                schoolList.Add(row[0].ToString());
                            }

                            //Handle visibility in Calling code
                            //if (schoolList.Count > 50)
                            //{
                            //    btnSubmitSchedule.Enabled = false;
                            //    lblSchoolsAvailable.Visible = true;
                            //}
                            //make sure that if an empty one of the same name is there you kill it
                            if (LOVList.ContainsKey(promptInfo.Question))
                                LOVList.Remove(promptInfo.Question);

                            LOVList.Add(promptInfo.Question, schoolList);
                            promptInfo.LOV = LOVList[promptInfo.Question];
                        }*/

            promptList.Add(promptInfo);
          }
        }
        catch (Exception ex)
        {
          ErrMsg = ex.Message;
        }
        finally
        {
          _boInfoObject.SessionLogOff();
        }
      }
      else ErrMsg = sErr;
      return promptList;
    }

    public Dictionary<string, string> GetScheduledReport(Int32 ReportId)
    {
        Dictionary<string, string> ReportParams = new Dictionary<string, string>();
        XmlDocument xParams = null;
      ErrMsg = string.Empty;
      string sErr = string.Empty;
      if (_boInfoObject.Logon(out sErr))
      {
        try
        {
          _boInfoObject.RESTfulLogon();
          xParams = _boInfoObject.GetRESTfulXML("/raylight/v1/documents/" + ReportId.ToString() + "/schedules/");

          XmlNodeList xSchedules = xParams.SelectNodes("schedule");

          List<String> sID = new List<string>();
          for (int i = 0; i < xSchedules.Count; i++)
          {
            sID.Add(xSchedules[i].SelectSingleNode("id").Value);
          }


          foreach (String ID in sID)
          {
            xParams = _boInfoObject.GetRESTfulXML("/raylight/v1/documents/" + ReportId.ToString() + "/schedules/" + ID);
            xSchedules = xParams.SelectNodes("schedule");
            //using (InfoObjects infoObjects = _boInfoObject.infoStore.Query(queryFolder.ToString()))
            // {
            // Verify that the object was found
            if (xSchedules.Count > 0)
            {
              XmlNode xSchedule = xSchedules[0];

              ReportParams.Add("ID", ID);
              ReportParams.Add("Title", xSchedule.SelectSingleNode("name").Value);
              ReportParams.Add("Schedule-BeginDate", xSchedule.SelectSingleNode("startdate").Value);
              ReportParams.Add("Schedule-EndDate", xSchedule.SelectSingleNode("enddate").Value);

              XmlNode xScheduleNode = null;
              xScheduleNode = xSchedule.SelectSingleNode("nthDayOfMonth");
              if (xScheduleNode != null)
              {
                ReportParams.Add("Schedule-ScheduleType", "NthDay");
                ReportParams.Add("Schedule-IntervalNthDay", xScheduleNode.SelectSingleNode("day").Value);
              }

              xScheduleNode = xSchedule.SelectSingleNode("daily");
              if (xScheduleNode != null)
              {
                ReportParams.Add("Schedule-ScheduleType", "Daily");
                ReportParams.Add("Schedule-IntervalDays", xScheduleNode.SelectSingleNode("dayinterval").Value);
              }

              xScheduleNode = xSchedule.SelectSingleNode("monthly");
              if (xScheduleNode != null)
              {
                ReportParams.Add("Schedule-ScheduleType", "Monthly");
                ReportParams.Add("Schedule-IntervalMonths", xScheduleNode.SelectSingleNode("month").Value);
              }

              xScheduleNode = xSchedule.SelectSingleNode("hourly");
              if (xScheduleNode != null)
              {
                ReportParams.Add("Schedule-ScheduleType", "Hourly");
                ReportParams.Add("Schedule-IntervalHours", xScheduleNode.SelectSingleNode("hour").Value);
                ReportParams.Add("Schedule-IntervalMinutes", xScheduleNode.SelectSingleNode("minute").Value);
              }

              xScheduleNode = xSchedule.SelectSingleNode("lastDayOfMonth");
              if (xScheduleNode != null)
              {
                ReportParams.Add("Schedule-ScheduleType", "LastDay");
              }

              xScheduleNode = xSchedule.SelectSingleNode("firstMondayOfMonth");
              if (xScheduleNode != null)
              {
                ReportParams.Add("Schedule-ScheduleType", "FirstMonday");
              }

              xScheduleNode = xSchedule.SelectSingleNode("weekly");
              if (xScheduleNode != null)
              {
                ReportParams.Add("Schedule-ScheduleType", "Weekly");

                XmlNode xDay;
                string sDays = String.Empty;
                foreach (DayOfWeek day in Enum.GetValues(typeof(DayOfWeek)))
                {
                  xDay = xScheduleNode.SelectSingleNode(day.ToString().ToLower());
                  if (xDay != null)
                  {
                    if (sDays == String.Empty) sDays = day.ToString();
                    else sDays = sDays + "," + day.ToString();
                  }
                }
                ReportParams.Add("Schedule-DaysOfWeek", sDays);
              }
            }
          }
        }
        catch (Exception ex)
        {
          ErrMsg = ex.Message;
        }
        finally
        {
          _boInfoObject.SessionLogOff();
        }
      }
      else ErrMsg = sErr;
      return ReportParams;
    }

    private string GetDaysOfWeek(CrystalDecisions.Enterprise.CalendarDays calendarDays)
    {
        string DaysOfWeek = string.Empty;

        foreach (CrystalDecisions.Enterprise.CalendarDay cd in calendarDays)
        {
            switch (cd.DayOfWeek)
            {
                case CeDayOfWeek.ceDayMonday:
                    DaysOfWeek += (DaysOfWeek.Length > 0 ? "," : string.Empty) + "Monday";
                    break;

                case CeDayOfWeek.ceDayTuesday:
                    DaysOfWeek += (DaysOfWeek.Length > 0 ? "," : string.Empty) + "Tuesday";
                    break;

                case CeDayOfWeek.ceDayWednesday:
                    DaysOfWeek += (DaysOfWeek.Length > 0 ? "," : string.Empty) + "Wednesday";
                    break;

                case CeDayOfWeek.ceDayThursday:
                    DaysOfWeek += (DaysOfWeek.Length > 0 ? "," : string.Empty) + "Thursday";
                    break;

                case CeDayOfWeek.ceDayFriday:
                    DaysOfWeek += (DaysOfWeek.Length > 0 ? "," : string.Empty) + "Friday";
                    break;

                case CeDayOfWeek.ceDaySaturday:
                    DaysOfWeek += (DaysOfWeek.Length > 0 ? "," : string.Empty) + "Saturday";
                    break;

                case CeDayOfWeek.ceDaySunday:
                    DaysOfWeek += (DaysOfWeek.Length > 0 ? "," : string.Empty) + "Sunday";
                    break;
            }
        }
        return DaysOfWeek;
    }

    private DateTime NextWeekday(DateTime Date, DayOfWeek dayOfWeek)
    {
        if (Date.DayOfWeek == dayOfWeek)
        {
            return Date;
        }
        return DateTime.Now;
    }

    private static void BuildParmValues(int OrigReportId, Dictionary<string, List<string>> promptList, ref XmlNode xParameter)
    {
      /*string FilteredReports;
      string[] SplitFilteredReports;
      DataTable FilteredReportsTable;
      List<string> FilteredReportsList = new List<string>();

      FilteredReportsTable = NsightUtilities.GetNsightSetting("SchoolPortal-ReportWithFilter", "ReportWithFilter");
      if (FilteredReportsTable != null && FilteredReportsTable.Rows.Count >= 1)
      {
        FilteredReports = FilteredReportsTable.Rows[0]["setting"].ToString();
        SplitFilteredReports = FilteredReports.Split(new char[] { ',' });
        foreach (string SplitFilteredReport in SplitFilteredReports)
        {
          if (!string.IsNullOrEmpty(SplitFilteredReport) && !FilteredReportsList.Contains(SplitFilteredReport.Trim()))
          {
            FilteredReportsList.Add(SplitFilteredReport.Trim());
          }
        }
      }*/

      XmlNodeList xName = xParameter.SelectNodes("name");
      if (xName.Count == 0) xName = xParameter.SelectNodes("technicalName");

      XmlNode xNameNode = null;
      if (xName.Count == 1) xNameNode = xName.Item(0);
      if (xNameNode != null)
      {
        XmlNode xAnswer = xParameter.SelectSingleNode("//answer");
        XmlNode xInfo = xAnswer.SelectSingleNode("//info");
        if (xInfo != null)
        {
          xAnswer.RemoveChild(xInfo);
        }
        XmlNode xValues = xAnswer.SelectSingleNode("//values");
        if (xValues == null)
        {
          xInfo.InnerXml = "<values></values>"; 
          xAnswer.AppendChild(xInfo.SelectSingleNode("//values"));
          xValues = xAnswer.SelectSingleNode("//values");
        }
        /*if (FilteredReportsList.Contains(OrigReportId.ToString().Trim()) && xNameNode.Value.Substring(0, 11) == "Enter OPEID")
        {
          string Params = String.Empty;
          foreach (string pValue in promptList[xNameNode.Value])
          {
            if (Params == string.Empty)
              Params = "<Value>" + pValue;
            else
              Params = Params + "," + pValue;
          }
          Params = Params + "</Value>";
          tDoc = new XmlDocument();
          tDoc.LoadXml(Params);
          xValues.AppendChild(tDoc.DocumentElement);
        }
        else
        {*/
        if (promptList.ContainsKey(xNameNode.InnerText))
        {
          foreach (string pValue in promptList[xNameNode.InnerText])
          {
            xInfo.InnerXml = ("<value>" + pValue + "</value>");
            xValues.AppendChild(xInfo.SelectSingleNode("//value"));
          }
        }
        //}
      }
    }

  /*
    public bool DetermineTwoFactorAuthBySQL(String UserID)
    {
        return BODataAccess.GetTwoFactorNSITokenRequired(UserID);
    }

    public bool DetermineTwoFactorAuthByBO(String UserID)
    {
        if (ConfigurationManager.AppSettings["EnableTokens"] == null || ConfigurationManager.AppSettings["EnableTokens"].ToLower() == "true")
        {
            Dictionary<int, string> TwoFactorAuthGroupListing;
            List<string> GroupsToMatch = BODataAccess.GetTwoFactorNSITokenRequiredGroups(UserID);

            TwoFactorAuthGroupListing = new Dictionary<int, string>();
            List<BOReportInfo> Groups = GetUserGroups(UserID);

            foreach (BOReportInfo group in Groups)
            {
                if (GroupsToMatch.Contains(group.Title.ToLower()))
                {
                    TwoFactorAuthGroupListing.Add(group.ID, group.Title);
                }
            }

            Groups.Clear();
            Groups = GetUserGroups(UserID);
            foreach (BOReportInfo group in Groups)
            {
                if (TwoFactorAuthGroupListing.ContainsKey(group.ID)) return true;
            }
        }
        return false;
    }

    public static string GetEulaAgreeDate(String userID)
    {
        return BODataAccess.GetEulaAgreeDate(userID);
    }

    public static bool InsertEula(String userID)
    {
        return BODataAccess.InsertEula(userID);
    }

    public static bool UpdateEula(String userID, bool Agree)
    {
        return BODataAccess.UpdateEula(userID, Agree);
    }
*/

    public DataTable LoadReports(string reportFolderId)
    {
      DataTable ReportListing;

      ReportListing = new DataTable();
      ReportListing.Columns.Add("ReportName");
      ReportListing.Columns.Add("Description");
      ReportListing.Columns.Add("ReportId");
      ReportListing.Columns.Add("ReportCUID");
      ReportListing.Columns.Add("LastUpdated");

      ErrMsg = string.Empty;
      string sErr = string.Empty;
      if (_boInfoObject.Logon(out sErr))
      {
        try
        {
          using (InfoObjects mailFolders = GetMailFolders(reportFolderId))
          using (InfoObjects infoObjects = GetMailItems(mailFolders))
          {
            foreach (InfoObject ReportInfo in infoObjects)
            {
              ReportListing.Rows.Add(new object[]
						{
							ReportInfo.Title, ReportInfo.Description, ReportInfo.ID.ToString(), ReportInfo.CUID,
							GetBOPropertyString(ReportInfo.Properties, "SI_UPDATE_TS")
						});
            }
          }
        }
        catch (Exception ex)
        {
          ErrMsg = ex.Message;
        }
      }
      else ErrMsg = sErr;
      return ReportListing;
    }
}

public class BOReportInfo
{
    private int _ID;
    private string _CUID;
    private string _Title;
    private string _Description;

    public BOReportInfo()
    {
        _ID = -1;
        _CUID = string.Empty;
        _Title = string.Empty;
        _Description = string.Empty;
    }

    public BOReportInfo(int id, string cuid, string title, string description)
    {
        _ID = id;
        _CUID = cuid;
        _Title = title;
        _Description = description;
    }

    public int ID
    {
        get { return _ID; }
        set { _ID = value; }
    }

    public string Cuid
    {
        get { return _CUID; }
        set { _CUID = value; }
    }

    public string Title
    {
        get { return _Title; }
        set { _Title = value; }
    }

    public string Description
    {
        get { return _Description; }
        set { _Description = value; }
    }

    //End Here
}

public class BOUserInfo : IDisposable
{
    private string _sUserName;

    private EnterpriseSession _session = null;
    private EnterpriseLogonInformation _logoninfo;
    public string _sToken;

    private string _sRESTfulToken;
    private string _sPassword;
    private string _boAdminUser;
    private string _boAdminPwd;
    public string _boServer = null;
    public string ErrMsg { get; private set; }

    public BOUserInfo()
    {
      _sUserName = string.Empty;
        _sPassword = string.Empty;
        _sToken = string.Empty;
        _sRESTfulToken = string.Empty;
        _session = null;
        _logoninfo = null;
        ErrMsg = string.Empty;
    }

    public BOUserInfo(string UserName, string Password, string Server, out string Error)
    {
      string sError = string.Empty;
      _sToken = string.Empty;
      _sRESTfulToken = string.Empty;
      _boServer = Server;
      try
      {
        _sUserName = UserName;
        _sPassword = Password;

        bool bRet = Logon(out sError);
        if (sError != string.Empty) ErrMsg = sError;
        SessionLogOff();
      }
      catch (Exception ex)
      {
        sError = ex.Message;
      }
      Error = sError;
    }

    public BOUserInfo(string UserName, string Password, out string Error)
    {
        string sError = string.Empty;
        _sToken = string.Empty;
        _sRESTfulToken = string.Empty;
        try
        {
            _sUserName = UserName;
            _sPassword = Password;

            bool bRet = Logon(out sError);
            SessionLogOff();
        }
        catch (Exception ex)
        {
          sError = ex.Message;
        }
        Error = sError;
    }

    public bool Logon(out string Error)
    {
        bool bLogon = false;
        string sError = string.Empty;
        try
        {
            if (_sToken.Equals(string.Empty))
            {
//                bLogon = Logon(_sUserName, _sPassword, (string)ConfigurationManager.AppSettings["BOServer"],
//                    (string)ConfigurationManager.AppSettings["BOAuthentication"], out sError);
              bLogon = Logon(_sUserName, _sPassword, _boServer, "secEnterprise", out sError);
            }
            else
            {
                //Login with the generated token
                SessionMgr sessionMgr = new SessionMgr();
                _session = sessionMgr.LogonWithToken(_sToken);

                bLogon = true;
            }
        }
        catch (Exception ex)
        { 
          bLogon = false;
          sError = ex.Message;
        }
        Error = sError;
        return bLogon;
    }

    public InfoStore GetInfoStore()
    {
      InfoStore iStore = null;
      string sError = string.Empty;
      ErrMsg = string.Empty;
      if (_session != null || Logon(out sError))
      {
        if (_session != null)
          iStore = (InfoStore)_session.GetService("InfoStore");
      }
      return iStore;
    }

    public bool Logon(string username, string password, out string Error)
    {
//        return Logon(username, password, (string)ConfigurationManager.AppSettings["BOServer"],
//            (string)ConfigurationManager.AppSettings["BOAuthentication"], out Error);
        return Logon(username, password, _boServer, "secEnterprise", out Error);
    }

    public bool Logon(string username, string password, string boserver, string authenticationtype, out string Error)
    {
        bool bLogon = false;
        Error = string.Empty;
        try
        {
            SessionMgr sessionMgr = new SessionMgr();
            _session = sessionMgr.Logon(username, password, boserver, authenticationtype);
            _logoninfo = sessionMgr.CreateLogonInfo();

            int iTimeout = 120; //Convert.ToInt32((string)ConfigurationManager.AppSettings["BOTokenTimeout"]);
            int iMaxUse = 100; // Convert.ToInt32((string)ConfigurationManager.AppSettings["BOTokenMaxLogins"]);
            _sToken = _session.LogonTokenMgr.CreateLogonTokenEx(_logoninfo.ReportedHostname, iTimeout, iMaxUse); //Removed Server to eliminate token reuse error
            //_sToken = _session.LogonTokenMgr.CreateLogonTokenEx(string.Empty, iTimeout, iMaxUse); //Removed Server to eliminate token reuse error
            //_sToken = _session.LogonTokenMgr.CreateLogonTokenEx(boserver, iTimeout, iMaxUse);

            _sUserName = username;
            _sPassword = password;
            bLogon = true;
        }
        catch (Exception ex)
        {
            Error = ex.Message;
            bLogon = false;
        }
        return bLogon;
    }

    public string SerializedTokenToXML(String sToken)
    {
        string loginXML = "<attrs xmlns='http://www.sap.com/rws/bip'><attr name='tokenType' type='string' possibilities='token, serializedSession'>serializedSession</attr>" +
                        "<attr name='logonToken' type='string'></attr></attrs>";

        XmlDocument xAttrDoc = new XmlDocument();
        xAttrDoc.LoadXml(loginXML);

        foreach (XmlNode node in xAttrDoc.GetElementsByTagName("attr"))
            if (node.Attributes["name"].Value == "logonToken")
            {
                node.InnerText = sToken;
                node.Attributes.RemoveNamedItem("null");
            }

        return xAttrDoc.OuterXml;
    }

    public bool RESTfulLogon()
    {
        bool bLogon = false;
        string sErr = string.Empty;
        if (_sRESTfulToken != string.Empty) bLogon = true; //If Logged on, No need to continue
        else
        {
        try
        {
            if (_sToken.Equals(string.Empty))
            {
//                bLogon = Logon(_sUserName, _sPassword, (string)ConfigurationManager.AppSettings["BOServer"],
//                    (string)ConfigurationManager.AppSettings["BOAuthentication"], out sErr);
                bLogon = Logon(_sUserName, _sPassword, _boServer,"secEnterprise", out sErr);

            }
            else bLogon = true;
            if (bLogon)
            {
                if (_session == null)
                { bLogon = Logon(out sErr); }
 
                if (bLogon)
                {
                    string loginXML = SerializedTokenToXML(_session.SerializedSession);
                    _sRESTfulToken = string.Empty;
                    HttpWebResponse POSTResponse = PutRESTfulXML("/logon/token", loginXML, out sErr);
                    if (sErr != string.Empty)
                    {
                      ErrMsg = sErr;
                      bLogon = false;
                    }
                    else
                    {
                      _sRESTfulToken = POSTResponse.Headers["X-SAP-LogonToken"].ToString();
                    }
                }
            }
        }
        catch (Exception ex) 
        { 
          bLogon = false;
          ErrMsg = ex.Message;
        }
      }
      return bLogon;
    }

    public bool DeleteRESTfulXML(String URLFunction)
    {
        bool bRet = false;
        try
        {
//            string RESTfulURL = (String)ConfigurationManager.AppSettings["BORESTfulService"];
            string RESTfulURL = "http://" + _boServer + ":6405/biprws";
            if (!URLFunction.StartsWith("/")) URLFunction = "/" + URLFunction;
            HttpWebRequest GetRequest = (HttpWebRequest)WebRequest.Create(RESTfulURL + URLFunction);
            GetRequest.Method = "DELETE";
            GetRequest.Accept = "application/xml";
            GetRequest.KeepAlive = false;
            if (!_sRESTfulToken.Equals(string.Empty)) GetRequest.Headers.Set("X-SAP-LogonToken", _sRESTfulToken);
            HttpWebResponse GETResponse = (HttpWebResponse)GetRequest.GetResponse();

            bRet = (GETResponse.StatusCode == HttpStatusCode.OK || GETResponse.StatusCode == HttpStatusCode.NoContent);
        }
        catch (Exception ex)
        {
          ErrMsg = ex.Message;
          bRet = false;
        }

        return bRet;
    }

    public XmlDocument GetRESTfulXML(String URLFunction)
    {
        XmlDocument xRet = new XmlDocument();
        try
        {
//            string RESTfulURL = (String)ConfigurationManager.AppSettings["BORESTfulService"];
          string RESTfulURL = "http://" + _boServer + ":6405/biprws";
            if (!URLFunction.StartsWith("/")) URLFunction = "/" + URLFunction;
            HttpWebRequest GetRequest = (HttpWebRequest)WebRequest.Create(RESTfulURL + URLFunction);
            GetRequest.Method = "GET";
            GetRequest.Accept = "application/xml";
            if (!_sRESTfulToken.Equals(string.Empty)) GetRequest.Headers.Set("X-SAP-LogonToken", _sRESTfulToken);
            HttpWebResponse GETResponse = (HttpWebResponse)GetRequest.GetResponse();
            Stream GETResponseStream = GETResponse.GetResponseStream();
            StreamReader sr = new StreamReader(GETResponseStream);
            xRet.LoadXml(sr.ReadToEnd());
        }
        catch (Exception ex)
        {
          ErrMsg = ex.Message;
        }
        return xRet;
    }

    public HttpWebResponse PutRESTfulXML(String URLFunction, string XML, out string errMsg)
    {
        HttpWebResponse POSTResponse = null;
        try
        {
//            string RESTfulURL = (String)ConfigurationManager.AppSettings["BORESTfulService"];
          string RESTfulURL = "http://" + _boServer + ":6405/biprws";
            XmlDocument login = new XmlDocument();
            login.LoadXml(XML);
            byte[] dataByte = Encoding.Default.GetBytes(login.OuterXml);
            if (!URLFunction.StartsWith("/")) URLFunction = "/" + URLFunction;
            HttpWebRequest POSTRequest = (HttpWebRequest)WebRequest.Create(RESTfulURL + URLFunction);
            POSTRequest.Method = "POST";
            POSTRequest.ContentType = "application/xml";
            POSTRequest.Accept = "application/xml";
            //POSTRequest.KeepAlive = false;
            if (!_sRESTfulToken.Equals(string.Empty)) POSTRequest.Headers.Set("X-SAP-LogonToken", _sRESTfulToken);
            POSTRequest.ContentLength = dataByte.Length;

            Stream POSTstream = POSTRequest.GetRequestStream();
            POSTstream.Write(dataByte, 0, dataByte.Length);

            POSTResponse = (HttpWebResponse)POSTRequest.GetResponse();
            errMsg = XML;//string.Empty;
        }
        catch (Exception ex)
        {
            errMsg = ex.Message; 
        }
        return POSTResponse;
    }

    public bool SessionLogOff()
    {
        bool bRet = false;
        ErrMsg = string.Empty;
        try
        {
            if (_session != null) _session.Logoff();
            _session.Dispose();
            _session = null;
            _sRESTfulToken = string.Empty; 
            bRet = true;
        }
        catch (Exception ex)
        {
          ErrMsg = ex.Message;
        }
        return bRet;
    }

    public bool LogOff()
    {
        bool bRet = false;
        try
        {
            _session.Logoff();
            _session.Dispose();
            _session = null;
            _sRESTfulToken = string.Empty;
            _sToken = string.Empty;
            bRet = true;
        }
        catch (Exception ex)
        {
          ErrMsg = ex.Message;
        }
        return bRet;
    }

    public string Password
    {
        get { return _sPassword; }
        set { _sPassword = value; }
    }

    public string Token
    {
        get
        {
            return _sToken;
        }
        set { _sToken = value; }
    }

    public string RESTfulToken
    {
        get { return _sRESTfulToken; }
        set { _sRESTfulToken = value; }
    }

    public string UserName
    {
        get
        {
            return _sUserName;
        }
        set { _sUserName = value; }
    }

    ~BOUserInfo()
    {
        Dispose();
    }

    public void Dispose()
    {
        _sUserName = null;

        if (_session != null)
        {
            try
            {
                _session.Logoff();
                _session.Dispose();
            }
            catch { }
        }
        System.GC.SuppressFinalize(this);
    }

    public InfoObjects ExecuteQuery(String queryString, ref BOUserInfo userStore)
    {
        if (userStore != null) return userStore.GetInfoStore().Query(queryString);
        else return null;
    }

    public InfoObjects ExecuteQuery(String queryString)
    {
        //Query the iStore
        return GetInfoStore().Query(queryString);
    }
}

public struct PromptOptions
{
    private string _question;
    private bool _isOptional;
    private int _orderBy;
    private int _response;
    private string _promptType;
    private string _defaultValue;
    private List<string> _LOV;

    public List<string> LOV
    {
        get
        {
            if (_LOV == null) _LOV = new List<string>();
            return _LOV;
        }
        set { _LOV = value; }
    }

    public string DefaultValue
    {
        get { return _defaultValue; }
        set { _defaultValue = value; }
    }

    public string PromptType
    {
        get { return _promptType.Trim(); }
        set { _promptType = value; }
    }

    /*
    public int Response
    {
        get { return _response; }
        set { _response = value; }
    }
    */

    public int Order
    {
        get { return _orderBy; }
        set { _orderBy = value; }
    }

    public bool IsOptional
    {
        get { return _isOptional; }
        set { _isOptional = value; }
    }

    public string Question
    {
        get { return _question; }
        set { _question = value; }
    }
}