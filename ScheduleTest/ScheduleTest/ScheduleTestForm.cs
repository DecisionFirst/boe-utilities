﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using BOE.TreeFolderNode;
using CrystalDecisions.Enterprise;

namespace UpdateUsers
{

  public partial class ScheduleTestForm : Form
  {
    private const string C_FLDRSQL = "Select Top 10000 SI_ID, SI_NAME, SI_PARENTID from CI_INFOOBJECTS where SI_KIND = 'Folder' and SI_PARENTID = {0}";

    private BOUserInfo _bui = null;
    private InfoStore _istore = null;
    private Boolean bCancel = false;
    private ScheduleTestData _data;
    private string prevParamName = string.Empty;

    public ScheduleTestForm()
    {
      InitializeComponent();
      _data = new ScheduleTestData();
      dbsParamList.DataSource = _data;
      dbsParamList.DataMember = "ParamList";
    }

    private void cmsLogon_Click(object sender, EventArgs e)
    {
      try
      {
        Cursor = Cursors.WaitCursor;
        cmsStatus.Text = "Logging on to CMS...";
        cmsStatus.Refresh();

        //log on to the CMS
        try
        {
          string err = "";
          _bui = new BOUserInfo(cmsUser.Text, cmsPassword.Text, cms.Text, out err);
          if (err != "")
          {
            cmsStatus.Text = "Unable to log on to CMS: " + err;
          }
          else
          {
            cmsStatus.Text = "Logged on to CMS";
            cmsStatus.Refresh();
            txtInfo.Text = _bui._sToken;
            userLabel.Text = "Loading Folder Tree";
            userLabel.Refresh();
            tvRptFolders.BeginUpdate();
            tvRptFolders.Nodes.Clear();
            try
            {
              cancelBtn.Enabled = true;
              loadFolderTree();
              userLabel.Text = string.Empty;
              userLabel.Refresh();
            }
            finally
            {
              tvRptFolders.EndUpdate();
              cancelBtn.Enabled = false;
            }
            loadBtn.Enabled = true;
            cmsLogon.Enabled = false;
          }
        }
        catch (Exception ex)
        {
          MessageBox.Show("Unable to log on to CMS: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

      }
      finally
      {
        Cursor = Cursors.Default;
      }
    }

    private void ScheduleTestForm_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (_bui != null)
      {
        _bui.LogOff();
        _bui.Dispose();
      }
    }

    private void btnClose_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void loadFolderTree()
    {
      TreeFolderNode newNode;
      _istore = _bui.GetInfoStore();
      string query = "Select SI_ID, SI_NAME from CI_INFOOBJECTS where SI_KIND = 'Folder' and SI_PARENTID = 0 and SI_NAME != 'User Folders' order by SI_NAME";
      using (InfoObjects iobjs = _istore.Query(query))
      {
        //generic for User Folders
        newNode = new TreeFolderNode("User Folders", -1);
        newNode.ImageIndex = 1;
        newNode.SelectedImageIndex = 0;
        tvRptFolders.Nodes.Add(newNode);
        //load the tree for the public folders
        foreach (InfoObject obj in iobjs)
        {
          newNode = new TreeFolderNode(obj.Title, obj.ID);
          newNode.ImageIndex = 1;
          newNode.SelectedImageIndex = 0;
          loadFolderNodes(newNode, obj.ID, false);
          loadReports(newNode, obj.ID);
          tvRptFolders.Nodes.Add(newNode);
          Application.DoEvents();
          if (bCancel)
            return;
        }
      }
    }

    private void loadFolderNodes(TreeFolderNode parentNode, int parentID, bool check)
    {
      TreeFolderNode newNode;
      using (InfoObjects iobjs = _istore.Query(string.Format(C_FLDRSQL, parentID) + " order by SI_NAME"))
      {
        foreach (InfoObject obj in iobjs)
        {
          newNode = new TreeFolderNode(obj.Title, obj.ID);
          newNode.ImageIndex = 1;
          newNode.SelectedImageIndex = 0;
          newNode.Checked = check;
          loadFolderNodes(newNode, obj.ID, check);
          loadReports(newNode, obj.ID);
          parentNode.Nodes.Add(newNode);
        }
      }
    }

    private void loadReports(TreeFolderNode parentNode, int parentID)
    {
      TreeFolderNode newNode;
      using (InfoObjects iobjs = _istore.Query(string.Format("Select SI_ID, SI_NAME, SI_KIND, SI_CUID from CI_INFOOBJECTS where SI_KIND in ('Webi', 'CrystalReport') and SI_PARENTID = {0} order by SI_NAME", parentID)))
      {
        foreach (InfoObject obj in iobjs)
        {
          newNode = new TreeFolderNode(obj.Title, obj.ID);
          newNode.Tag = obj.CUID;
          if (obj.Kind == "Webi")
          {
            newNode.ImageIndex = 2;
            newNode.SelectedImageIndex = 2;
          }
          else 
          {
            newNode.ImageIndex = 3;
            newNode.SelectedImageIndex = 3;
          }
          parentNode.Nodes.Add(newNode);
        }
      }
    }

    private void loadBtn_Click(object sender, EventArgs e)
    {
      Cursor = Cursors.WaitCursor;
      //We're only concerned about Webi reports here....
      if (tvRptFolders.SelectedNode.ImageIndex == 2)
      {
        BOUtilities _BOU = new BOUtilities(_bui);

        BOUtilities.ExportAs ExportAs = BOUtilities.ExportAs.PDF;
        DateTime BeginDate = DateTime.Now;
        DateTime EndDate = DateTime.Now;
        BOUtilities.ScheduledReportDelivery ScheduledReportDelivery;

        BOUtilities.ScheduleSetup Schedule = new BOUtilities.ScheduleSetup();
        Schedule.Type = BOUtilities.ScheduleTypes.RunOnce;
        Schedule.BeginDate = DateTime.Now.AddMonths(6);
        Schedule.EndDate = DateTime.Now.AddMonths(7);
        TreeFolderNode node = (TreeFolderNode) tvRptFolders.SelectedNode;
        int ReportId = node.FolderID;

       // ReportId = _BOU.CopyReportToInbox(ReportId, _bui.UserName);

        Dictionary<string, List<string>> SetPromptValues = new Dictionary<string,List<string>>();
        List<string> vals = new List<string>();
        string pName = string.Empty;
        foreach (DataRow row in _data.ParamList.Rows)
        {
          if (pName == string.Empty)
          {
            pName = row["ParamName"].ToString();
          }
          else if (pName != row["ParamName"].ToString())
          {
            SetPromptValues.Add(pName, vals);
            pName = row["ParamName"].ToString();
            vals.Clear();
          }
          vals.Add(row["ParamValue"].ToString());
        }
        //add the last parameter processed
        if (vals.Count > 0)
        {
          SetPromptValues.Add(pName, vals);
        }
        ScheduledReportDelivery = new BOUtilities.ScheduledReportDelivery(BOUtilities.DeliveryType.Inbox);

        _BOU.SetSchedule(ReportId, node.FolderID, ExportAs, ScheduledReportDelivery, Schedule, SetPromptValues);
        if (_BOU.ErrMsg != string.Empty)
        {
          txtInfo.Text = _BOU.ErrMsg;
        }

      }
      else
      {
        MessageBox.Show("Only Webi Reports can be scheduled here", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
      }
      Cursor = Cursors.Default;
    }

    private void tvRptFolders_AfterSelect(object sender, TreeViewEventArgs e)
    {
      try
      {
        if (tvRptFolders.SelectedNode.ImageIndex == 2) //this is a Webi
        {
          Cursor = Cursors.WaitCursor;
          BOUtilities _BOU = new BOUtilities(_bui);

          gbParams.Visible = false;
          _data.ParamLOV.Clear();
          _data.ParamList.Clear();

          XmlDocument xPrompts = _BOU.GetReportPrompts(((TreeFolderNode)tvRptFolders.SelectedNode).FolderID.ToString(), true);
          foreach (XmlNode rootNode in xPrompts.ChildNodes)
          {
            //find the parameters node
            if (rootNode.Name == "parameters")
            {
              loadParamInfo(rootNode);
            }
          }
        }
        gbParams.Visible = (_data.ParamList.Count > 0);
        if (gbParams.Visible)
        {
          dgvParams_RowEnter(this, null);
        }
        else
        {
          lbLOV.Items.Clear();
          lbLOV.Visible = false;
          lblLOV.Visible = false;
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      finally
      {
        Cursor = Cursors.Default;
      }
    }

    private void loadParamInfo(XmlNode paramsNode)
    {
      string pName = string.Empty;
      string pValue = string.Empty;
      string pID = string.Empty;
      foreach (XmlNode paramNode in paramsNode.ChildNodes)
      {
        foreach (XmlNode node in paramNode.ChildNodes)
        {
          if (node.Name == "name")
          {
            pName = node.InnerText;
          }
          else if (node.Name == "id")
          {
            pID = node.InnerText;
          }
          else if (node.Name == "answer")
          {
            foreach (XmlNode answerNode in node.ChildNodes)
            {
              if (answerNode.Name == "info")
              {
                foreach (XmlNode infoNode in answerNode.ChildNodes)
                {
                  if (infoNode.Name == "lov")
                  {
                    loadLOVInfo(infoNode, pID);
                  }
                }
              }
              else if (answerNode.Name == "values")
              {
                foreach (XmlNode valNode in answerNode.ChildNodes)
                {
                  _data.ParamList.AddParamListRow(pName, valNode.InnerText, pID);
                }
              }
            }
          }
        }
      }
    }

    private void loadLOVInfo(XmlNode lovNode, string pID)
    {
      foreach (XmlNode node in lovNode.ChildNodes)
      {

        if (node.Name == "values")
        {
          foreach (XmlNode valNode in node.ChildNodes)
          {
            _data.ParamLOV.AddParamLOVRow(pID, valNode.InnerText);
          }
        }
      }
    }

    private void dgvParams_RowEnter(object sender, DataGridViewCellEventArgs e)
    {
      if ((dgvParams.CurrentRow != null) && 
          (dgvParams.CurrentRow.Cells[0].Value != null) &&
          (dgvParams.CurrentRow.Cells[0].Value.ToString() != prevParamName))
      {
        lbLOV.Items.Clear();
        string filter = string.Format("paramID='{0}'", dgvParams.CurrentRow.Cells[2].Value.ToString());
        DataRow[] rows = (_data.ParamLOV.Select(filter, "ParamLOVValue ASC", DataViewRowState.CurrentRows));
        foreach (DataRow row in rows)
        {
          lbLOV.Items.Add(row["ParamLOVValue"].ToString());
        }
        lbLOV.Visible = (lbLOV.Items.Count > 0);
        lblLOV.Visible = lbLOV.Visible;
        prevParamName = dgvParams.CurrentRow.Cells[0].Value.ToString();
      }
    }


  }
}
